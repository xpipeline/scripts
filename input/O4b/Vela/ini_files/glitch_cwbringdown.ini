; ---- X-Pipeline parameter file for the offline O4b Vela pulsar glitch search.
;      See https://trac.ligo.caltech.edu/xpipeline/wiki/Documentation/Searches/grb/grb.iniDoc


[tags] 
; ---- DO NOT EDIT - SVN Id tag for this file.
version = $Id$


[background]
; ---- Information on data to be used for background estimation.
; ---- Duration of background period in seconds. This length of time, by default centered on 
;      the trigger, is used for background event generation. 
backgroundPeriod = 10800
; ---- This is needed by the online search when using asymmetric background estimation.
;      If doAsymmetricBackground = 0 then the background period is symmetric about the trigger time.
;      Otherwise backgroundAsymmetryFactor is the fraction of the background period before the trigger time.
;      Specifically, the background period is [start_time,end_time] where
;        start_time = int(trigger_time - background_period*backgroundAsymmetryFactor)
;          end_time = int(trigger_time + background_period*(1.0 - backgroundAsymmetryFactor))
doAsymmetricBackground = 0
backgroundAsymmetryFactor = 0.9
; ---- Full path of files listing time lags to be used for background estimation.
;      Specify one lag file for each network type. (Note that lag files
;      are ignored for single-detector networks.) 
;      The format is one set of time lags per row, with each column being the time shift applied 
;      to the corresponding detector in the [input] detectorList below. With variable names of 
;      the form "lags_2det1site" etc., grb.py will automatically select the right file based on 
;      what detectors are being used. 
;      TIP: It's good practice to point to a file in a repository if running an analysis to be reviewed.
;      All coincidence segments for all listed time lags will be used to generate
;      background. The actual number of background trials you get isn't determined 
;      advance, as it depends on the duty cycle of each detector, and also on the value of 
;      circtimeslidestep in the [parameters] section.
; ---- Use the same lag files as in O3a:
lags_2det2site = /home/patrick.sutton/Runs/O4b/Vela/lags2det_200.txt
lags_3det3site = /home/patrick.sutton/Runs/O4b/Vela/lags3det_200.txt


[parameters]
; ---- xdetection parameters. See 
;      https://trac.ligo.caltech.edu/xpipeline/wiki/Documentation/xdetection
;      Particularly important parameters that are not obvious are:
;        onSourceBeginOffset,onSourceEndOffset - these define the on source region to be analyzed as 
;          [grbTriggerTime+onSourceBeginOffset,grbTriggerTime+onSourceEndOffset], where grbTriggerTime 
;          is the GPS time supplied by the -g option of grb.py 
;        circtimeslidestep - this is the step size of circular time shifts applied within each 
;        background job. Smaller values give more circular time shifts and more background trials.
analysisTimes = 1.0,0.5,0.25,0.125,0.0625,0.03125,0.015625,0.0078125
blockTime = 256
onSourceBeginOffset = -362
onSourceEndOffset = 362
likelihoodType_1det1site = loghbayesian,energyitf1,skypositiontheta,skypositionphi
likelihoodType_2det2site = loghbayesiancirc,standard,circenergy,circinc,circnullenergy,circnullinc,powerlaw,energyitf1,energyitf2,skypositiontheta,skypositionphi
likelihoodType_3det3site = loghbayesiancirc,standard,circenergy,circinc,circnullenergy,circnullinc,nullenergy,nullinc,powerlaw,energyitf1,energyitf2,energyitf3,skypositiontheta,skypositionphi
minimumFrequency = 200
maximumFrequency = 4000
offsetFraction = 0.5
outputType = clusters
sampleFrequency = 8192
verboseFlag = 1
whiteningTime = 4
seed = 1235
applyCalibCorrection = 1
circtimeslidestep = 2
; ---- Optionally apply generalised clustering. We do not use this in production GRB/FRB searches.
; genclusteringthresh = 2
; ---- Optionally apply autogating. This is independent of the applyGates parameter which
;      controls application of pre-defined gates. The parameters are defined as follows:
;        filter_duration   Positive scalar. Length of the moving average filter in seconds.
;        sigma_threshold   Positive scalar. Threshold on normalised energy to create a gate.
;        window_duration   Positive scalar. Length of the gate window in seconds.
;        veto_duration     Optional positive scalar. Veto (ignore) a gate if there is a
;                          gate in any other detector within +/-veto_duration seconds.
;        veto_threshold    Optional positive scalar. Threshold on normalised energy in
;                          any other detector at which to veto a coincident gate in a
;                          given detector. Defaults to sigma_threshold.
; autogateparameters = 1,50,2,1/32,10


[output]
; ---- These variables set how many injections or off-source blocks are processed by each condor job.
;      Injections are processed in groups of up to maxInjNum/(number_of_sky_positions) injections per condor job. 
;      Off-source jobs are also grouped together in batches of this number if maxOffNum variable is not set. 
;      Results from separate jobs are later merged using xmergegrbresults.m. 
;      This option is useful to control how long condor jobs take. 
;      A larger number means fewer but longer condor jobs.
;      Jobs that run for 1-2 hours are the most efficient for large-scale analyses.
maxInjNum = 1200
maxOffNum = 500


[input]
; ---- This section specifies the information on the raw data frames to be analysed. 
;      See https://wiki.ligo.org/LSC/JRPComm/ObsRun4#LIGO_Offline_data
; ---- Detectors in the O4a LIGO-Virgo-KAGRA network.
detectorList = H1,L1,V1
; ---- List of channels. 
;      The order of the channels must match the order in which the detectors were specified in 
;      detectorList. The list of channels found in a particular frame (.gwf) file can be found 
;      using FrDump -i <name of frame file>  
channelList = GDS-CALIB_STRAIN_CLEAN_AR,GDS-CALIB_STRAIN_CLEAN_AR,Hrec_hoft_16384Hz
; ---- List of frametypes. 
;      The type of frame holding the specified channel. The order of the frame types must match the 
;      order in which the detectors were specified in detectorList. The list of available frame types 
;      can be found using your datafind_exec exectuable (see [datafind]), e.g gw_data_find --show-types. 
frameTypeList = H1_HOFT_C00_AR,L1_HOFT_C00_AR,HoftOnline
; ---- Optionally specify a pre-generated frame cache file in the X-Pipeline format. For instructions 
;      see https://trac.ligo.caltech.edu/xpipeline/wiki/Documentation/Searches/FrameCacheFile 
;      If framecacheFile is not specified, X-Pipeline will generate its own. 
; frameCacheFile = <FULL/PATH/TOFILE>


[skygrid]
; ---- Parameters used to construct the sky grid searched over. 
;        numSigmaSkyPos - Amount of the trigger error box to cover in a multiple sky-position search, 
;          as a multiple of the estimated 1-sigma uncertainty radius. This should be considered as a 
;          one-dimensional Gaussian; for example, numSigmaSkyPos=1 means that the sky grid will cover 
;          the 68% containment region of the GRB error box. 
;        delayTol - maximum allowed time delay error [sec] in the grid. Smaller values give a denser 
;          sky grid. Note that for a maximum allowed SNR loss characterised by r=rho/rho_max for a 
;          signal at frequency fmax we have 
;            delayTol = acos(r^2)/(2*pi*fmax)
;      The settings below assume the "1-sigma" error region in fact covers the full possible sky 
;      position uncertainty. A time delay error of 24.9e-6s gives a maximum amplitude SNR loss of 10% 
;      (r=0.9) at 4000 Hz. An error of 17.7e-6s (12.5e-6s) gives r=0.95 (r=0.975). 
;      which gives a maximum amplitude SNR loss 10% (r=0.9).
numSigmaSkyPos = 1.0
delayTol = 12.5e-6


[datafind]
; ---- X-Pipeline can generate its own frame cache file and find segment lists of good and bad data 
;      times. This requires a server to query and the names of the executables that return the frame 
;      and segment infomration.
; ---- Data find server used for calls to datafind_exec.
datafind_server      = datafind.ldas.cit:80
; ---- Executable for finding data frames.
datafind_exec        = gw_data_find
; ---- Executable for obtaining science segments ("good" data times). 
segfind_exec         = ligolw_segment_query_dqsegdb
; ---- Executable for obtaining category 2-5 veto segment files ("bad" data times). 
segs_from_cats_exec  = ligolw_segments_from_cats_dqsegdb
; ---- Executable to convert xml segment files into plain text format. 
ligolw_print_exec    = ligolw_print


[segfind]
; ---- Options to be used with segfind_exec (segment finding tool) and 
;      segs_from_cats_exec (veto segment finding tool)
; ---- If you are going to generate segment lists on the fly then you need to 
;      specify segment-url and veto-file.
; ---- Segment database URL.
segment-url = https://segments.ligo.org
; ---- Category definer used by ligolw_segments_from_cats in order to generate
;      category 2-4 veto segment files.
; ---- This is the burst veto definer for O4b.
;      See https://git.ligo.org/detchar/veto-definitions/-/tree/CAT1-HLV-20240716-R1/burst/O4
veto-file = /home/patrick.sutton/git/scripts/input/O4b/segments/H1L1V1-HOFT_C01_O4_BURST.xml
; ---- Optional flag. Only specify to use DMT files if running online, otherwise 
;      using segdb is significantly faster.
useDMTFile = 0
; ---- If 1, apply gates if listed for any detector. If 0 then do not apply gates.
applyGates = 0


; ---- Segment information.  There should be one section per detector in 
;      detectorList. Give one of the following:
;        i)  a comma separated-list of segment types (data quality flags)
;            for each detector; or
;        ii) pre-made segment-list AND veto-list files in segwizard format, with an 
;            optional gating-list file. If either of these files are missing then 
;            xpipeline will generate them by querying the segment database. Either of 
;            the veto or gating files may be empty. Gating is only applied if applyGates=1 
;            in the [segfind] section.
[H1]
include-segments = H1:DMT-ANALYSIS_READY:1
;segment-list = /home/patrick.sutton/xpipeline/branches/scripts/input/O4a/segments/H1_science_cat1.txt
;veto-list    = /home/patrick.sutton/xpipeline/branches/scripts/input/O4a/segments/H1_cat24veto.txt

[L1]
include-segments = L1:DMT-ANALYSIS_READY:1  
;segment-list = /home/patrick.sutton/xpipeline/branches/scripts/input/O4a/segments/L1_science_cat1.txt
;veto-list    = /home/patrick.sutton/xpipeline/branches/scripts/input/O4a/segments/L1_cat24veto.txt

[V1]
include-segments = V1:ITF_SCIENCE:1


[condor]
; ---- Condor parameters. You should only need to update the 'Era' and 'SearchType' 
;      parameters for your search. They don't affect the analysis but are used by 
;      sysadmins for counting CPU time used by the various searches. 
universe = vanilla
ProdDevSim = prod
Era = o4
Group = burst
SearchType = grb.xoffline
; ---- Failed condor jobs are rerun this number of times (or until they succeed).
;      retryNumber = 5 is usually good enough for job failures that are "random" 
;      (eg temporary file system problems). Jobs which fail persistently indicate 
;      a probable bug in the parameters, missing data, or some other problem that 
;      you will have to fix yourself. 
retryNumber = 1000
; ---- These are cluster (scratch disk) specific. /local/$ENV(USER)/ works on CIT.
;      Warning: Log file problems are a common cause of failed jobs. You must specify a directory 
;      which already exists and to which you have write permission. These log files need to be 
;      local for the head node from which you submit jobs, not NFS mounted. 
dagman_log_on_source   = /local/$ENV(USER)/grb_jobs_on_source.logs
dagman_log_off_source  = /local/$ENV(USER)/grb_jobs_off_source.logs
dagman_log_simulations = /local/$ENV(USER)/grb_jobs_simulations.logs


[injection]
; ---- This section specifies properties common to all injection waveform sets, 
;      such as amplitude scales and whether to account for calibration uncertainties.
; ---- Amplitude scales applied to all injections in the [waveforms] section.
injectionScales = 0.00316,0.0100,0.0147,0.0215,0.0316,0.0464,0.0681,0.1000,0.1468,0.2154,0.3162,0.4642,0.6813,1.0000,1.4678,2.1544,3.1623,4.6416,6.8129,10.0000
; ---- This parameter controls the number or spacing of injections.
;      Positive value: time interval between injections. The number of injections at each 
;        injection scale is approximately blockTime/injectionInterval.
;      Negative value: on-the-fly injections are spaced so that exactly 
;        abs(injectionInterval) injections are performed at each injection scale.
injectionInterval = -1200
; ---- Apply estimated calibration uncertainties to injections. This should normally 
;      be 1 for production analyses.
;      Set to 0 in this case because we are using pre-made injection files that 
;      have already had calibration uncertainties applied; see the [waveforms] section.
miscalibrateInjections = 0


[waveforms]
; ---- Sets of on-the-fly injections to be performed. Fixed amplitudes.
;      Format: set_name = waveform_type!waveform_parameters
;          or: set_name = waveform_type1!waveform_parameters1,waveform_type2!waveform_parameters2,...
;        set_name - an arbitrary string of letters and numbers only (no underscores, 
;          punctuation marks, etc.).  Make sure that each name is unique and not a sub-string of 
;          another name. This is because some book-keeping codes use "ls" to find files associated 
;          with a given injection set. So, for example, having sets dfm1, dfm2, dfm3 is fine, but 
;          sets dfm, dfm2, dfm3 will cause failures because a command like "ls dfm*" for the first 
;          injection set will also return files associated with dfm2, dfm3. And be careful if you 
;          have many sets: dfm1, ..., dfm10! 
;        waveform_type - can be any of the types recognized by xmakewaveform; type
;          "help xmakewaveform" in matlab.
;        waveform_parameters - To understand the parameters of a given type, e.g. 'chirplet', 
;          do "xmakewaveform('chirplet')" in matlab. A parameter of the form 'number;number;word' 
;          (e.g. -1;1;linear) will cause the value of the parameter to be generated randomly for 
;          each injection. For allowed syntax see the 'assignparameter' helper function of 
;          https://trac.ligo.caltech.edu/xpipeline/browser/xpipeline/trunk/matlab/share/xmakegwbinjectionfile.m
;      If there is more than one waveform in the comma-separated list, then for each injection one 
;      waveform is chosen randomly from the list.
;      Note that some waveforms are loaded from catalogs; in these cases you 
;      must use the -c option with grb.py to specify a location. All valid
;      catalogs are stored in branches/waveforms/ .
; ---- Pre-made injection files: We use pre-made injection parameters copied from the run15 analysis, 
;      which used a more advanced version of x-pipleine (r6460M) to fix the injection polarisation to 
;      match the orientation of Vela. The parameter strings used to generate those files are left as 
;      comments for ease of interpretation. 
;      The injection type has been updated to use ringdowns as defined by coherent WaveBurst (cWB), 
;      wth the injection waveforms stored in the git repo as 
;      git/waveforms/usercreated-[a...j].mat 
;      The conversion of the injection log files was done by the script 
;      git/scripts/input/O4b/Vela/injections/convert_injection_files.sh 
; ---- ringdowns: short-duration, fixed inclination = 63.3 deg or 116.7 deg.
; ringdown2is = ds2p!1.0e-21~0.01~650~0;6.2832;linear~0.4493 
; ringdown4is = ds2p!1.0e-21~0.01~2100~0;6.2832;linear~-0.4493 
cwbringdown2is = injection_cwbringdown2is.txt
cwbringdown4is = injection_cwbringdown4is.txt
; ---- ringdowns: medium-duration, fixed inclination = 63.3 deg or 116.7 deg.
;ringdown1i = ds2p!1.0e-21~0.1~290~0;6.2832;linear~0.4493
;ringdown2i = ds2p!1.0e-21~0.1~650~0;6.2832;linear~-0.4493
;ringdown3i = ds2p!1.0e-21~0.1~1100~0;6.2832;linear~0.4493
;ringdown4i = ds2p!1.0e-21~0.1~2100~0;6.2832;linear~-0.4493
;ringdown5i = ds2p!1.0e-21~0.1~3900~0;6.2832;linear~0.4493
cwbringdown1i = injection_cwbringdown1i.txt
cwbringdown2i = injection_cwbringdown2i.txt
cwbringdown3i = injection_cwbringdown3i.txt
cwbringdown4i = injection_cwbringdown4i.txt
cwbringdown5i = injection_cwbringdown5i.txt
; ---- ringdowns: long-duration, fixed inclination = 63.3 deg or 116.7 deg.
; ringdown2il = ds2p!1.0e-21~0.5~650~0;6.2832;linear~-0.4493
; ringdown4il = ds2p!1.0e-21~0.5~2100~0;6.2832;linear~0.4493
cwbringdown2il = injection_cwbringdown2il.txt
cwbringdown4il = injection_cwbringdown4il.txt



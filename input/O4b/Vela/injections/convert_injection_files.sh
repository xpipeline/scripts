
# Split strings of letter IDs and waveform set names into arrays.
# Store default input field separator.
bkpIFS="$IFS" 
# Set field separators to commas and spaces, then read strings into arrays.
IFS=', ' read -r -a array_letter <<<"a b c d e f g h i"
IFS=', ' read -r -a array_wfset  <<<"ringdown2is, ringdown4is, ringdown1i, ringdown2i, ringdown3i, ringdown4i, ringdown5i, ringdown2il, ringdown4il"
## Sanity check: dump arrays to screen to verify the elements are paired up correctly.
#for index in "${!array_letter[@]}"
#do
#    echo "$index ${array_letter[index]} ${array_wfset[index]}"
#done
# Reset default input field separator.
IFS="$bkpIFS"

# Loop over the waveform sets and process each injection file.
for index in "${!array_letter[@]}"
do
    echo "Processing ${array_wfset[index]} ..."
    echo "cat injection_${array_wfset[index]}.txt | awk -F~ '{print \$1, \$5, \$9}' | awk '{print \$1, \$2, \$3, \$4, \$5, \"usercreated-${array_letter[index]}\", \$7 \"~\" \$8, \$9, \$10, \$11, \$12, \$13, \"usercreated-${array_letter[index]}\", \$15 \"~\" \$16}' > injection_cwb${array_wfset[index]}.txt" > .command
    source .command
    #head -n 3 injection_ringdown2is.txt | awk -F~ '{print $1, $5, $9}' | awk '{print $1, $2, $3, $4, $5, "usercreated-a", $7 "~" $8, $9, $10, $11, $12, $13, "usercreated-a", $15 "~" $16}'
done
rm .command




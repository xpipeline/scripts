%--------------------------------------------------------------------------
%
% run_xmakefrbrunfiles - script to generate search and injection grids for the
% O3A/B FRB search.
%
% This script can be run in interactive matlab using matlab 2017a or later.
%
% See /home/eric/Documents/PYTHON_LINUX_BOX/2021_PROJECTS/FRB_Work/XPipeline_Skygrid_Work/make_chime_skygrid_production_2.m
% for the main code that produced the function 'xmakefrbrunfiles.m'
%
% eric.howell@ligo.org
% patrick.sutton@ligo.org
%
% $Id$
%
%--------------------------------------------------------------------------

%--------------------------------------------------------------------------
% Set paths to xpipeline program files - these are the paths at CIT.
%--------------------------------------------------------------------------

addpath /home/xpipeline/xpipeline/trunk/matlab/share/
addpath /home/xpipeline/xpipeline/trunk/matlab/searches/grb
addpath /home/xpipeline/xpipeline/trunk/matlab/utilities/  
addpath /home/xpipeline/opt/xpipeline/dev/share/fastclusterprop/
addpath /home/edaw/libframe/matlab

%--------------------------------------------------------------------------
% Set paths to input and output
%--------------------------------------------------------------------------

% ---- Input .csv file.
input_file_name =  '/home/xpipeline/xpipeline/branches/scripts/input/O3-frb/sky_grids/O3A/reorganized_chime_data_May1corrected.csv';

% ---- Run of interest. One of O3A or O3B.
run = 'O3A'

% ---- Maximum luminosity distance [Mpc] of interest for selecting FRBs.
dL_limit = 500

% ---- Flag to make plots of grids.
plots = 1

% ---- Select repeaters or non-repeaters and output directory (directory must end with '/').
repeater_type = 'R' ; output_dir = '/home/xpipeline/xpipeline/branches/scripts/input/O3-frb/sky_grids/O3A/repeater/'
% repeater_type = 'NR' ; output_dir = '/home/xpipeline/xpipeline/branches/scripts/input/O3-frb/sky_grids/O3A/non-repeater/'

% ---- Run the grid-generation function.
xmakefrbrunfiles(input_file_name, output_dir, dL_limit, run, repeater_type, plots)

function xmakefrbrunfiles(input_file_name, output_dir, dL_limit, run, repeater_type, plots)
% XMAKEFRBRUNFILES - Make GRB-search-like TRIGGER source list and IPN-like grid
% and injection files for the CHIME FRB search.
%
% usage:
%
%   xmakefrbrunfiles(input_file_name, output_dir, dL_limit, run, repeater_type, plots)
%
%  input_file_name     String. Name of .csv file containing FRB data.
%  output_dir          String. Directory in which to write output files.
%  dL_limit            Scalar. Maximum luminosity distance [Mpc] of FRBs to be
%                      analysed. 
%  run                 String. LIGO/Virgo run period of FRBs of interest. One of
%                      'O3A' or 'O3B'.  
%  repeater_type       String. 'R' for repeater or 'NR' for non-repeater FRBs.
%  plots               Scalar. If 1 then plots are made, otherwise no plots are
%                      made.
%
% $Id$

% NOTE: skygrid files are in the x-pipeline SVN at
% https://trac.ligo.caltech.edu/xpipeline/browser/xpipeline/branches/scripts/input/O3-frb/sky_grids

%--------------------------------------------------------------------------
%  Usage:
%--------------------------------------------------------------------------
%  dL_limit = 377
% %  input_file_name =  'reorganized_chime_data_with_corrected_dms.csv'; -
%  not used now
%
%  input_file_name =  'reorganized_chime_data_May1corrected.csv';
%  output_file_name = 'out_test1.txt';
%  run = 'O3A'
%  plots = 1 -produce plots of grids , 0 - no plots
%  repeater_type = 'R' for repeater or 'NR' for non-repeater
%
% Call via terminal
%  matlab -nodisplay ; xmakefrbrunfiles(input_file_name,output_file_name, dL_limit, run, plots);exit
%
% Stand alone function
% xmakefrbrunfiles(input_file_name,output_file_name, dL_limit, 'NR', plots)

% test_dir = '/home/eric/Documents/PYTHON_LINUX_BOX/2021_PROJECTS/FRB_Work/frb-trigger-database-o3/GRID_FILES_TEST/'
%--------------------------------------------------------------------------
% Inputs
%--------------------------------------------------------------------------
% input_file_name   String. Name of input csv file containing FRB data.
% output_file_name  String.  Name of output file of the FRB list
% dm_limit          Float. The maximim dL value for the FRB sample from the
%                   input_file
% LVC run           [string]: 'O3A' or 'O3B'
% plots: [optional] 1 - plots required;
%                   0 = no plots required;
%--------------------------------------------------------------------------
% $Id$
% Matlab function to produce the injection and grid files based on the code
% written by patrick.sutton@ligo.org

%
%--------------------------------------------------------------------------
% Outputs
%--------------------------------------------------------------------------
% [1] grid file in format:   'FRBNAME_grid_search.txt'
% [2] injection file in format:  'FRBNAME_grid_sim.txt'
% [3] optional: plot of 'injections','search grid','island centres','1-sigma radius','search radius','coverage'
% [4] FRB_list.txt : a list of FRBs for analysis
%--------------------------------------------------------------------------

%--------------------------------------------------------------------------
% External functions
%--------------------------------------------------------------------------
%
% xmakeskygrid.m - makes the grid and injection files for xpipeline : found
% in https://trac.ligo.caltech.edu/xpipeline/browser/xpipeline/branches/o3b-production#matlab/utilities
%
% The following 2 files are found in:
% https://trac.ligo.caltech.edu/xpipeline/browser/xpipeline/branches/o3b-production#matlab/share
%
% numorcell2tildedelimstr - from cell or numeric to tilde separated string
% tildedelimstr2numorcell - converts data to format for xmakeskygrid.m

%--------------------------------------------------------------------------
% Internal functions
%--------------------------------------------------------------------------
% FRB_data_format2.m - converts from cell format to numerical and tilde
% separated

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%---------------------------------------------------------------------------
% % TEST RUN
% % input .csv file
% input_file_name =  '/home/eric/Documents/PYTHON_LINUX_BOX/2021_PROJECTS/FRB_Work/frb-trigger-database-o3/reorganized_chime_data_May1corrected.csv';
% % test directory
% test_dir = '/home/eric/Documents/PYTHON_LINUX_BOX/2021_PROJECTS/FRB_Work/frb-trigger-database-o3/GRID_FILES_TEST/'
% % output Xpipeline run file list
% output_file_name = [test_dir,'out_test2.txt'];
% run = 'O3A'
% dL_limit = 160
% plots = 0
% repeater_type = 'NR'
%---------------------------------------------------------------------------


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% [ 1 of 4] Read and extract data from csv
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

T = readtable(input_file_name);
table_length=size(T,1)
VarNames = T.Properties.VariableNames;

%--------------------------------------------------------------------------
% Make sure reads to required precision
% format long g

%--------------------------------------------------------------------------
% Extract all data in cell format
%--------------------------------------------------------------------------
FRB_ID = T{1:table_length,find(strcmp(VarNames, 'ID'))};
FRB_RA_LIST2 = T{1:table_length,find(strcmp(VarNames, 'RA_list'))};
FRB_RA_ERROR_LIST2 = T{1:table_length,find(strcmp(VarNames, 'RA_error_list'))};
FRB_DEC_LIST2 = T{1:table_length,find(strcmp(VarNames, 'DEC_list'))};
FRB_DEC_ERROR_LIST2 = T{1:table_length,find(strcmp(VarNames, 'DEC_error_list'))};
FRB_T_INF = T{1:table_length,find(strcmp(VarNames, 't_inf'))};
%FRB_TIME_SOURCE_RUN_GPS(1)
GW_NETWORK = T{1:table_length,find(strcmp(VarNames, 'network'))};
DL_MAX = T{1:table_length,find(strcmp(VarNames, 'DL_max'))};
DM = T{1:table_length,find(strcmp(VarNames, 'DM'))};
DM_CORR = T{1:table_length,find(strcmp(VarNames, 'DM_corrected'))};

% Values for grb_file
RA_center_approx = T{1:table_length,find(strcmp(VarNames, 'RA_center_approx'))};
DEC_center_approx= T{1:table_length,find(strcmp(VarNames, 'DEC_center_approx'))};
sky_error_approx= T{1:table_length,find(strcmp(VarNames, 'sky_error_approx'))};
Z_max= T{1:table_length,find(strcmp(VarNames, 'Z_max'))};
REPEATER= T{1:table_length,find(strcmp(VarNames, 'repeater'))};


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  [ 2 of 4] Make FRB selection based on Lum distance limit and observation
%  run (O3A or O3B)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%--------------------------------------------------------------------------
% Look for bursts within dL limit with available IFOs
% Additionally, only include data with more than 1 IFO for Xpipeline
%--------------------------------------------------------------------------

isites =0;
p=0;
for i = 1:length(GW_NETWORK)
    %    if (~isempty(GW_NETWORK{i,1})  &  DL_MAX(i) < dL_limit)  % 377
    NET_FRB_LEN = length(GW_NETWORK{i});
    if (~isempty(GW_NETWORK{i,1})  & NET_FRB_LEN > 2 & DL_MAX(i) < dL_limit & strcmp(REPEATER(i), repeater_type))  % 377
        p=p+1;
        isites(p) = i;
    end
end

%--------------------------------------------------------------------------
% Work out whether FRBs within  O3A OR O3B based on T_inf and observation
% run start and finish GPS times
%--------------------------------------------------------------------------
O3a_start_GPS = 1238166018;
O3a_end_GPS = 1253977218;

O3b_start_GPS = 1256655618;
%O3b_end_GPS = 1253977218


clear O3b_ind O3a_ind
b=0;c=0;
for i = 1:length(isites)
    if(FRB_T_INF(isites(i)) > O3a_start_GPS & FRB_T_INF(isites(i)) < O3a_end_GPS);
        %        if(FRB_TIME_SOURCE_RUN_GPS(isites(i)) < O3a_end_GPS);
        b=b+1;
        O3a_ind(b)  = isites(i);
    end
    if(FRB_T_INF(isites(i)) > O3b_start_GPS);
        c=c+1;
        O3b_ind(c)  = isites(i);
    end
end


%--------------------------------------------------------------------------
% Select index for required data run - in order of increasing d_L
%--------------------------------------------------------------------------
if (run == 'O3A')
    [sort_value sort_index ]=sort(DL_MAX(O3a_ind)) ;
    DL_MAX(O3a_ind);
    O3a_ind = O3a_ind(sort_index);
    DL_MAX(O3a_ind);
    run_index = O3a_ind;
else
    [sort_value sort_index ]=sort(DL_MAX(O3b_ind)) ;
    O3b_ind = O3b_ind(sort_index);
    run_index = O3b_ind;
end
%--------------------------------------------------------------------------

disp('-----------------------------------------------------------------------');
disp(['Total FRB sample is ' num2str(length(run_index)) ' bursts']);


%--------------------------------------------------------------------------
% Can print out data for cross checking
%--------------------------------------------------------------------------
for i = 1:length(run_index)
    ind=run_index(i);
    if (i==1)
        disp( [	'Designation    '   'Rep'   ' 	       Trigger (GPS) '	  '    RA (deg)'	'    Dec (deg)'	 '    Pos Err (deg)' 'DM'  ' DM_ERR' 	'    z'  ' Dist (Mpc)'  'NET ']);
    end
    RA_center_approx_FRB = num2str(RA_center_approx(ind),'%9.2f');
    DEC_center_approx_FRB= num2str(DEC_center_approx(ind),'%9.2f');
    sky_error_approx_FRB= num2str(sky_error_approx(ind),'%7.2f');
    Z_max_FRB= num2str((Z_max(ind)),'%7.2f');
    GPS_FRB = num2str(FRB_T_INF(ind),'%7.2f');
    NAME_FRB = strcat('FRB',num2str(FRB_ID(ind)));
    DL_MAX_FRB = num2str(DL_MAX(ind),'%7.2f');
    NET_FRB = GW_NETWORK{ind};
    %[NET_FRB] = cell_string_clean( NET_FRB, '{};',1 );
    DM_FRB = num2str(DM(ind),'%7.2f');
    DM_err = num2str(DM(ind),'%7.2f');
    REP = REPEATER{ind};
    disp( [ num2str(i,'%7.2d') ' '	NAME_FRB ' '	 	REP '  ' GPS_FRB	 '  '...
        '   '	 RA_center_approx_FRB	'  '    DEC_center_approx_FRB	 ' '  sky_error_approx_FRB	' ' DM_FRB  '    ' DM_err  '   ' Z_max_FRB  ' '  DL_MAX_FRB '  ' NET_FRB]);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   [ 3 of 4]  Print out values for parameter FRB_list.txt file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for i = 1:length(run_index) % EDIT TO O3A OR O3B
    ind=run_index(i);       % EDIT TO O3A OR O3B
    RA_center_approx_FRB2(i) = RA_center_approx(ind);
    DEC_center_approx_FRB2(i)= DEC_center_approx(ind);
    sky_error_approx_FRB2(i)= sky_error_approx(ind);
    Z_max_FRB2(i)= Z_max(ind);
    GPS_FRB2(i) = FRB_T_INF(ind);
    NAME_FRB2(i) = FRB_ID(ind);
end

fileID = fopen(strcat(output_dir,'FRB_list.txt'),'w');

% Format for output data file
formatSpec= ' nan            CHIME       CHIME       FRB%s          FRB              UTC           %8s     -       -        %12s     %12s     %12s    %8f\n';

% [OPTIONAL] With names of all parameters - for checking
% fprintf(fileID, ' Grace ID    Satellite   Instrument      Designation      Classification  Trigger (UTC)        Trigger (GPS)  T90 (s)  T90 error (s)    RA (deg)       Dec (deg)       Pos Err (deg)      z\n'  );

for i = 1:length(run_index)
    fprintf(fileID,formatSpec,num2str(NAME_FRB2(i)),num2str(GPS_FRB2(i)), num2str(RA_center_approx_FRB2(i)),...
        num2str(DEC_center_approx_FRB2(i)), num2str(sky_error_approx_FRB2(i)), Z_max_FRB2(i));
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%--------------------------------------------------------------------------
% Can display all data for inspection by un-commenting this loop
%--------------------------------------------------------------------------
% for i = 1:length(run_index) % EDIT TO O3A OR O3B
%     ind=run_index(i);       % EDIT TO O3A OR O3B
%     if (i==1)
%         disp( ['Grace ID    '	'Satellite    '	'Instrument    '	'Designation    '	...
%             '    Classification'	'    Trigger (UTC)' 	'   Trigger (GPS)'	 '   T90 (s)'...
%             '   T90 error (s)'	 '    RA (deg)'	'    Dec (deg)'	 '    Pos Err (deg)' 	'    z'   ]);
%     end
%     RA_center_approx_FRB = num2str(RA_center_approx(ind));
%     DEC_center_approx_FRB= num2str(DEC_center_approx(ind));
%     sky_error_approx_FRB= num2str(sky_error_approx(ind));
%     Z_max_FRB= num2str((Z_max(ind)));
%     GPS_FRB = num2str(FRB_T_INF(ind));
%     NAME_FRB = strcat('FRB',num2str(FRB_ID(ind)));
%
%     disp( ['  nan  '	'        CHIME      '  '   CHIME       ' 	NAME_FRB	...
%         '           FRB '	'     UTC     '	GPS_FRB	 '   -'...
%         '  -    '	 RA_center_approx_FRB	'  '    DEC_center_approx_FRB	 ' '  sky_error_approx_FRB	' ' Z_max_FRB  ]);
% end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   [ 4 of 4] PRODUCE GRID AND INJECTION FILES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Loop over desired FRBs and construct grid and injection files.
for k = 1:length(run_index)

    i = run_index(k);
    % ---- Convert data from cell form to tilde separated format.
    [name, gps, ra, dec, sigma_deg, sites, dec_er, ra_er, dlmax, dm ] = FRB_data_format2(FRB_ID(i),FRB_T_INF(i),...
        FRB_RA_LIST2(i), FRB_DEC_LIST2(i), FRB_RA_ERROR_LIST2(i), FRB_DEC_ERROR_LIST2(i), GW_NETWORK(i),DL_MAX(i), DM(i) );
    netval = GW_NETWORK(i);
    disp(['Calculating grid files for ' name '. FRB number ' num2str(k) ' of ' num2str(length(run_index))]);
    
    % ---- Convert from tilde separated format to float.
    RA          = tildedelimstr2numorcell(ra);
    DEC         = tildedelimstr2numorcell(dec);
    RA_err      = tildedelimstr2numorcell(ra_er);
    DEC_err     = tildedelimstr2numorcell(dec_er);
    SIGMA_deg   = tildedelimstr2numorcell(sigma_deg);
    
    % ---- The data file contains dummy "-1" for some angle values if there are
    %      less than 5 islands. Delete these.
    bad_idx = find(RA==-1 & RA_err==-1 & DEC==-1 & DEC_err==-1);
    RA(bad_idx)      = [];
    RA_err(bad_idx)  = [];
    DEC(bad_idx)     = [];
    DEC_err(bad_idx) = [];
    
    % ---- Derive sigma_deg.
    SIGMA_deg = max(RA_err .* cosd(DEC),DEC_err);
    
    % ---- Convert data to strings for input to xmakeskygrid.
    ra        = numorcell2tildedelimstr(RA);
    ra_err    = numorcell2tildedelimstr(RA_err);
    dec       = numorcell2tildedelimstr(DEC);
    dec_err   = numorcell2tildedelimstr(DEC_err);
    sigma_deg = numorcell2tildedelimstr(SIGMA_deg);
    
    % ---- Parameters common to search and injection grids.
    gridtype = 'circular';
    verbose = '0';
    % ---- Parameters for search grid.
    nSigma_search = '2';
    delay_tol_search = '125e-6';
    outputfile_search = [output_dir,name '_grid_search.txt'];
    % ---- Parameters for injection "grid".
    N_inj = 1e4;               %-- number of injection locations
    nSigma_inj = '3.0';        %-- higher coverage
    delay_tol_inj = '125e-8';  %-- higher resolution
    outputfile_inj = [output_dir,name '_grid_sim.txt'];
    
    % ---- Make search grid.
    [searchgrid.ra,searchgrid.dec,searchgrid.prob,searchgrid.area,coverage] = xmakeskygrid( ...
        ra,dec,gps,sigma_deg,nSigma_search,sites,delay_tol_search,'None',gridtype,verbose,'2');
    % ---- Write search grid file for X-Pipeline. Columns are ra [deg], dec [deg], prob, area.
    dlmwrite(outputfile_search,[searchgrid.ra,searchgrid.dec,searchgrid.prob,searchgrid.area], ...
        'delimiter',' ','precision','%11.7f');
    
    % ---- Make injection file by random sampling of very high resolution grid.
    %      First generate the grid. 
    [injgrid.ra,injgrid.dec,injgrid.prob] = xmakeskygrid(ra,dec,gps,sigma_deg, ...
        nSigma_inj,sites,delay_tol_inj,'None',gridtype,verbose);
    % ---- Grid points with very low probability (~eps) can cause sampler to
    %      fail. Remove all grid points with negligibly small probability.
    deleteIdx = find(injgrid.prob<1e-10);
    injgrid.ra(deleteIdx)   = [];
    injgrid.dec(deleteIdx)  = [];
    injgrid.prob(deleteIdx) = [];
    % ---- Now perform random sampling of grid.
    idx = randsamp(N_inj,injgrid.prob);    
    injgrid.ra  = injgrid.ra(idx);
    injgrid.dec = injgrid.dec(idx);
    clear injgrid.prob
    % ---- Write injection location file for X-Pipeline. Columns are ra [deg], dec [deg].
    dlmwrite(outputfile_inj,[injgrid.ra injgrid.dec],'delimiter',' ','precision','%16.14f');
        
    if (plots == 1)
        % ---- Make plot.
        figure;
        a = plot(injgrid.ra,injgrid.dec,'r.');
        grid on; hold on;
        b = plot(searchgrid.ra,searchgrid.dec,'bo','MarkerSize',10,'MarkerFaceColor','b');
        grid on; hold on;
        c = plot(RA,DEC,'kx','MarkerSize',14,'LineWidth',3);
        d = circles(RA,DEC,SIGMA_deg,'k-');
        e = circles(RA,DEC,str2num(nSigma_search)*SIGMA_deg,'b-');
        f = circles(RA,DEC,coverage,'g--');
        h1 = [a b c d e f];
        legend(h1,'injections','search grid','island centres','1-sigma radius','search radius','coverage');
        set(gca,'xdir','reverse')
        xlabel('RA [deg]')
        ylabel('Dec [deg]')
        title([name ' (' num2str(length(searchgrid.ra)) ' search grid points)']);
        saveas(gcf,[output_dir,name,'_inj_search_islands.png'],'png');            
    end
    
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- External function for cell-to-number and tilde-separated formatting.
function [name, gps, ra, dec, sigma_deg, sites , dec_er, ra_er, dlmax, dm] = FRB_data_format2(FRB_ID,FRB_T_INF,FRB_RA_LIST2, FRB_DEC_LIST2, FRB_RA_ERROR_LIST2, FRB_DEC_ERROR_LIST2, GW_NETWORK, DL_MAX, DM)

format long g

% ---- Example formatting:
% name = 'FRB210115';  %-- I made up the name based on the GPS time. It has to match column 4 of the "GRB" list file
% gps = '1294754024';
% ra = '206.2~208.1~209.8~210.4~211.1';
% dec = '48.24~48.24~48.24~48.24~48.24';
% sigma_deg = '0.22~0.22~0.22~0.22~0.22';
% sites = 'H~L~V';

for i = 1:1 %table_length
    FRB_RA_LIST(i,:) = str2num(FRB_RA_LIST2{i});
    FRB_RA_ERROR_LIST(i,:) = str2num(FRB_RA_ERROR_LIST2{i});
    FRB_DEC_LIST(i,:) = str2num(FRB_DEC_LIST2{i});
    FRB_DEC_ERROR_LIST(i,:) = str2num(FRB_DEC_ERROR_LIST2{i});
end

% ---- Convert to tilde separated format for Patrick's code.
name  = strcat('FRB',num2str(FRB_ID(1)));
gps   = num2str(FRB_T_INF(1));
dlmax = num2str(DL_MAX(1));
dm    = num2str(DM(1));
%
for i = 1:1
    island_index = find(FRB_RA_LIST(i,1:5) ~= -1);
    ra_str='';dec_str='';err_str='';dec_str_er='';ra_str_er='';
    for j = 1:length(island_index)
        if (j == length(island_index) )
            % ---- If final value or only one value, end the loop (no tilde).
            ra1 = strcat(ra_str,num2str(FRB_RA_LIST(i,j)));
            dec1 = strcat(dec_str,num2str(FRB_DEC_LIST(i,j)));
            ra1_er = strcat(ra_str_er,num2str(FRB_RA_ERROR_LIST(i,j)));
            dec1_er = strcat(dec_str_er,num2str(FRB_DEC_ERROR_LIST(i,j)));
            err_max = max(FRB_RA_ERROR_LIST(i,j), FRB_DEC_ERROR_LIST(i,j))   ;
            err1 = strcat(err_str,num2str(err_max));
        else
            % ---- If not final values, add a tilde.
            ra1 = strcat(ra_str,num2str(FRB_RA_LIST(i,j)),'~');
            dec1 = strcat(dec_str,num2str(FRB_DEC_LIST(i,j)),'~');
            ra1_er = strcat(ra_str_er,num2str(FRB_RA_ERROR_LIST(i,j)),'~');
            dec1_er = strcat(dec_str_er,num2str(FRB_DEC_ERROR_LIST(i,j)),'~');
            err_max = max(FRB_RA_ERROR_LIST(i,j), FRB_DEC_ERROR_LIST(i,j));
            err1 = strcat(err_str,num2str(err_max),'~');
        end
        ra_str = ra1;
        dec_str = dec1;
        ra_str_er = ra1_er;
        dec_str_er = dec1_er;
        err_str = err1;
    end
    ra = ra_str;
    dec = dec_str;
    ra_er = ra_str_er;
    dec_er = dec_str_er;
    sigma_deg = err_str;
end

% ---- Construct 'sites' string.
if (isempty(GW_NETWORK{1,1}))
    sites = '';
end

if (ismember(cellstr(GW_NETWORK{1,1}),'H1'))
    sites = 'H';
end
if (ismember(cellstr(GW_NETWORK{1,1}),'L1'))
    sites = 'L';
end
if (ismember(cellstr(GW_NETWORK{1,1}),'V1'))
    sites = 'V';
end
if (ismember(cellstr(GW_NETWORK{1,1}),'L1V1'))
    sites = 'L~V';
end
if (ismember(cellstr(GW_NETWORK{1,1}),'H1V1'))
    sites = 'H~V';
end
if (ismember(cellstr(GW_NETWORK{1,1}),'H1L1'))
    sites = 'L~H';
end
if (ismember(cellstr(GW_NETWORK{1,1}),'H1L1V1'))
    sites = 'L~H~V';
end


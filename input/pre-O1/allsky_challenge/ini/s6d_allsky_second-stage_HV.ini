; -----------------------------------------------------------------------------
; X-Pipeline sn.py configuration script for S6D first-stage analysis (few lags, 
; all simulations) with the spherical radiometer.  
; -----------------------------------------------------------------------------


[tags] 
; ---- Tags for this file/run.
version = $Id:$
user-tag =


[background]
; ---- Parameters used for background estimation.  (See also circtimeslidestep 
;      in [parameters].)
; ---- Duration of background period in seconds (data outside on-source window).
;      This should always be set to zero for the optically triggered supernova 
;      search.
backgroundPeriod = 0
; ---- Specify a lag file for each network type.
lags_2det2site = /home/psutton/xpipeline/branches/sphrad/input/allsky_challenge/lags/lags_2det2site_60.txt
lags_3det3site = /home/psutton/xpipeline/branches/sphrad/input/allsky_challenge/lags/lags_3det3site_60.txt


[parameters]
; ---- Parameters fed to xdetection trigger generation jobs via the \
;      parameters.txt file.
analysisTimes = 1.0
blockTime = 512
onSourceBeginOffset = -5038200
onSourceEndOffset = 5038200
likelihoodType_3det3site = clusterenergy,standardenergy,standardinc,plusenergy,plusinc,crossenergy,crossinc,nullenergy,nullinc,circenergy,circinc,skypositiontheta,skypositionphi
likelihoodType_2det2site = clusterenergy,standardenergy,standardinc,plusenergy,plusinc,crossenergy,crossinc,nullenergy,nullinc,circenergy,circinc,skypositiontheta,skypositionphi
minimumFrequency = 64
maximumFrequency = 1000
offsetFraction = 0.5
circtimeslidestep = 6
outputType = sphrad
sampleFrequency = 2048
verboseFlag = 1
whiteningTime = 1
seed = 1235
applyCalibCorrection = 1
sphradparameterfile = sphradparameters.txt

[output]
; ---- This variable sets how many injections are processed by each
;      simulation job, and how many off-source segments are analysed
;      by each off-source job.  A larger number means fewer but longer
;      condor jobs.
maxInjNum = 1
maxOffNum = 1
; ---- Parameters controlling whether job output files are sprayed across
;      the cluster instead of returned to the local output/ directory.  
;      DO NOT TURN ON FOR PRODUCTION ANALYSES - the merging and post-processing
;      codes will not be able to find them.  
distributeOnSource = 0
distributeOffSource = 0
distributeSimulation = 0


[input]
; ----- Detector data for S6/VSR3 networks.
detectorList = H1,L1,V1
channelList = LDAS-STRAIN,LDAS-STRAIN,h_4096Hz
frameTypeList = H1_LDAS_C02_L2,L1_LDAS_C02_L2,HrecV2


; ---- Segment information.  There should be one section per detector.
;      We use the reviewed official segments from the S5/VSR1 GRB analysis.
[H1]
segment-list = /home/psutton/xpipeline/branches/sphrad/input/allsky_challenge/segments/S6D_SCIENCE_H1notL1-cat1.txt
veto-list    = /home/psutton/xpipeline/branches/sphrad/input/allsky_challenge/segments/S6D_VETO_H1_cat24.txt
[L1]
segment-list = /home/psutton/xpipeline/branches/sphrad/input/allsky_challenge/segments/S6D_SCIENCE_L1-cat1.txt
veto-list    = /home/psutton/xpipeline/branches/sphrad/input/allsky_challenge/segments/S6D_VETO_L1_cat24.txt
[V1]
segment-list = /home/psutton/xpipeline/branches/sphrad/input/allsky_challenge/segments/S6D_SCIENCE_V1notL1-cat1.txt
veto-list    = /home/psutton/xpipeline/branches/sphrad/input/allsky_challenge/segments/S6D_VETO_V1_cat24.txt


[datafind]
; ---- Data find server used for calls to ligo_data_find.
datafind_server     = ldr.ligo.caltech.edu
;datafind_server     = ldr.atlas.local:80
; ---- Specify executables for data, segment finding.
datafind_exec       = ligo_data_find
segfind_exec        = ligolw_segment_query
segs_from_cats_exec = ligolw_segments_from_cats
ligolw_print_exec   = ligolw_print


[segfind]
; ---- Options to be used with ligolw_segment_query (segment finding tool)
; ---- Set generateSegs = 0 to turn off segment finding (when using pre-made
;      segment lists).
generateSegs = 0


; [segs_from_cats]
; ---- Options to be used with ligolw_segments_from_cats (veto segment 
;      finding tool).  Specify nothing when veto segment lists are supplied
;      pre-made.


[condor]
; ---- Condor parameters.
universe = vanilla
dagman_log_on_source   = /usr1/psutton/allsky_jobs_on_source.logs
dagman_log_off_source  = /usr1/psutton/allsky_jobs_off_source.logs
dagman_log_simulations = /usr1/psutton/allsky_jobs_simulations.logs
dagman_log_mdcs        = /usr1/psutton/allsky_jobs_mdcs.logs
retryNumber = 5


[injection]
; ---- Amplitude scales applied to all injections (MDC and on-the-fly).
injectionScales = 0.001,0.00781,0.0156,0.0186,0.0221,0.0263,0.0312,0.0372,0.0442,0.0526,0.0625,0.0743,0.0884,0.1051,0.1250,0.1487,0.1768,0.2102,0.2500,0.2973,0.3536,0.4204,0.5000,0.5946,0.7071,0.8409,1.0000

; ---- Spacing of on-the-fly injections (ignored for MDCs). 
injectionInterval = -5000
; ---- Injection "jittering" options.  Since we will use pre-generated 
;      injection lists from BurstMDC, turn off all these options.
miscalibrateInjections = 0
jitterInjections = 0
jitterInclination = 0


[waveforms]
; ---- Sets of on-the-fly injections to be performed.  
mva    = adi-a!1~-1;1;linear,adi-b!1~-1;1;linear,adi-c!1~-1;1;linear,adi-d!1~-1;1;linear,adi-e!1~-1;1;linear
adi-a  = adi-a!1~-1;1;linear
adi-b  = adi-b!1~-1;1;linear
adi-c  = adi-c!1~-1;1;linear
adi-d  = adi-d!1~-1;1;linear
adi-e  = adi-e!1~-1;1;linear
ebbh-a = ebbh-a!1~-1;1;linear
ebbh-d = ebbh-d!1~-1;1;linear
ebbh-e = ebbh-e!1~-1;1;linear


[mdc]
; ---- Sets of MDC injections to be performed.
mdc_sets = brst-s6,elptc-s6

[brst-s6]
injectionScales = 0.0375, 0.075, 0.15, 0.3, 0.6, 1.2, 2.4, 4.8, 9.6, 19.2, 38.4, 76.8
logFileName = injection_brst-s6d.txt
channelList = GW-H,GW-H,GW-16K
frameTypeList = BRST_S6,BRST_S6,BRST_S6
frameObsList = GHLV
;frameCacheFile = empty.txt

[elptc-s6]
injectionScales = 0.0375, 0.075, 0.15, 0.3, 0.6, 1.2, 2.4, 4.8, 9.6, 19.2, 38.4, 76.8
logFileName = injection_elptc-s6d.txt
channelList = GW-H,GW-H,GW-16K
frameTypeList = ELPTC_S6,ELPTC_S6,ELPTC_S6
frameObsList = GHLV
;frameCacheFile = empty.txt


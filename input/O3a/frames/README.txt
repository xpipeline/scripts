
O3a/b: see https://wiki.ligo.org/LSC/JRPComm/ObsRun3

frames: [HL]1_HOFT_C01
channels: [HL]1:DCS-CALIB_STRAIN_CLEAN_C01

gw_data_find --server ldr.ligo.caltech.edu -o H --type H1_HOFT_C01 -s 1238166018 -e 1253977218 > O3a_H1_frames.txt
gw_data_find --server ldr.ligo.caltech.edu -o L --type L1_HOFT_C01 -s 1238166018 -e 1253977218 > O3a_L1_frames.txt
gw_data_find --server ldr.ligo.caltech.edu -o H --type H1_HOFT_C01 -s 1256655618 -e 1269363618 > O3b_H1_frames.txt
gw_data_find --server ldr.ligo.caltech.edu -o L --type L1_HOFT_C01 -s 1256655618 -e 1269363618 > O3b_L1_frames.txt



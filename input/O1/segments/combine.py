#!/usr/bin/env python

from ligo.segments import utils as segmentsUtils

#ifo = "H1"
ifo = "L1"

# ---- Read in science and veto segments.
sciseg   = segmentsUtils.fromsegwizard(open(ifo+"-seg.txt")); sciseg.coalesce()
cat1veto = segmentsUtils.fromsegwizard(open(ifo+"-veto-cat1.txt")); cat1veto.coalesce()
cat2veto = segmentsUtils.fromsegwizard(open(ifo+"-veto-cat2.txt")); cat2veto.coalesce()
cat4veto = segmentsUtils.fromsegwizard(open(ifo+"-veto-cat4.txt")); cat4veto.coalesce()

# ---- Subtract cat1veto flags from the science segments.
sciseg_cat1 = sciseg - cat1veto; sciseg_cat1.coalesce()

# ---- Write out newly constructed segment list.
filename = ''.join([ifo,"_science_cat1.txt"])
print('Writing file: ', filename)
f = open(filename,"w"); segmentsUtils.tosegwizard(f,sciseg_cat1); f.flush(); f.close()

# ---- Add cat2veto and cat4veto segments for each ifo.
cat24veto = cat2veto + cat4veto; cat24veto.coalesce()
# ---- Write out newly constructed veto list.
filename = ''.join([ifo,"_cat24veto.txt"])
print('Writing file: ', filename)
f = open(filename,"w"); segmentsUtils.tosegwizard(f,cat24veto); f.flush(); f.close()


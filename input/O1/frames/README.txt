O1: See https://wiki.ligo.org/LSC/JRPComm/ObsRun1

frames: [HL]1_HOFT_C02
channels: [HL]1:DCS-CALIB_STRAIN_C02 

gw_data_find --server ldr.ligo.caltech.edu -o H --type H1_HOFT_C02 -s 1126051217 -e 1136649617 > O1_H1_frames.txt
gw_data_find --server ldr.ligo.caltech.edu -o H --type L1_HOFT_C02 -s 1126051217 -e 1136649617 > O1_L1_frames.txt



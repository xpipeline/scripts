
O2: See https://wiki.ligo.org/LSC/JRPComm/ObsRun2#LIGO_offline_data

frames: [HL]1_CLEANED_HOFT_C02 
channels: [HL]1:DCH-CLEAN_STRAIN_C02 

gw_data_find --server ldr.ligo.caltech.edu -o H --type H1_CLEANED_HOFT_C02 -s 1164556817 -e 1187733618 > O2_H1_frames.txt
gw_data_find --server ldr.ligo.caltech.edu -o L --type L1_CLEANED_HOFT_C02 -s 1164556817 -e 1187733618 > O2_L1_frames.txt


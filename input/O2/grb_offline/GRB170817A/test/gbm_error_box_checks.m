% -------------------------------------------------------------------------

% ---- Load injection location data.
inj = load('inj_phi_theta.txt');
phi = inj(:,1);
idx = find(phi>pi);
phi(idx) = phi(idx) - 2*pi; %-- wrap to -pi:pi for nicer plots
theta = inj(:,2);
V = CartesianPointingVector(phi,theta);

% ---- GRB time and GBM centre.
gbm.gps = 1187008885; 
gbm.ra = 176.79; 
gbm.dec = -39.8399;
[gbm.phi, gbm.theta] = radectoearth(gbm.ra,gbm.dec,gbm.gps);
gbm.V = CartesianPointingVector(gbm.phi,gbm.theta);

% ---- Load sky grid.
tmp = load('grid.txt');
skygrid.theta = tmp(:,1);
skygrid.phi = tmp(:,2);

% -------------------------------------------------------------------------

% ---- Injection angle from GBM centre.
coslambda = V*gbm.V';
lambda = acos(coslambda);

% ---- Check statistical distribution of injection locations.
figure; set(gca,'fontsize',16); set(gcf,'Color',[1 1 1]);
dx = 0.01;
x = [0:dx:pi]'; %-- angles from 0 to pi
sigma_stat = 11.58*pi/180;
sigma_core = 3.7*pi/180;
sigma_tail = 14*pi/180;
prob_core = fisherpdf((0.66*(sigma_core^2+sigma_stat^2)^0.5)^(-2),x);
prob_tail = fisherpdf((0.66*(sigma_tail^2+sigma_stat^2)^0.5)^(-2),x);
prob_comb = 0.9*prob_core + 0.1*prob_tail;
cum_prob = cumsum(prob_comb)*dx;
plot(x,prob_comb,'k-','linewidth',2)
grid on; hold on
%
n = hist(lambda,x+0.5*dx);
n = n/sum(n)/dx;
stairs(x,n,'b-','linewidth',2)
axis([0 1 0 6])
ylabel('probability')
xlabel('angle [rad]')
set(gca,'fontsize',16)
legend('core/tail fisher','injections')

% -------------------------------------------------------------------------

% ---- Effective statistical error for generating X-Pipeline grids. This is
%      defined as the angle "sigma" such that 1.65*sigma has 95%
%      containment of the error box when that statistical uncertainty is
%      zero (i.e., using systematic error only).
prob_core_grid = fisherpdf((0.66*sigma_core)^(-2),x);
prob_tail_grid = fisherpdf((0.66*sigma_tail)^(-2),x);
prob_comb_grid = 0.9*prob_core_grid + 0.1*prob_tail_grid;
cum_prob_grid = cumsum(prob_comb_grid)*dx;
x95 = robustinterp1(cum_prob_grid,x,0.95) / 1.65;
disp(['Effective one-sigma systematic error radius: ' num2str(x95*180/pi) ' deg.']);
disp(['Stated one-sigma statistical error radius:   ' num2str(sigma_stat*180/pi) ' deg.']);
disp(['Quadrature sum:                              ' num2str(norm([sigma_stat,x95])*180/pi) ' deg.']);

CL = robustinterp1(x,cum_prob,1.65*norm([sigma_stat,x95]));
disp(['Estimated containment of grid:             ' num2str(CL*100) ' %.']);

% -------------------------------------------------------------------------

% ---- Plot injection and grid locations.
figure; set(gca,'fontsize',16); set(gcf,'Color',[1 1 1]);
plot(phi,theta,'.')
hold on; grid on
plot(skygrid.phi,skygrid.theta,'r+')
plot(gbm.phi,gbm.theta,'m*')
xlabel('phi [rad]')
ylabel('theta [rad]')
set(gca,'fontsize',16)
legend('injections',['grid (' num2str(CL*100) '%)'],'GBM centre')

% -------------------------------------------------------------------------


;
;X-Pipeline parameter file for O2 GRB offline searches.


[tags] 
; ---- Tags for this file/run.
version = $Id$
user-tag = grb170817a_gate_ext


[background]
; ---- Information on data to be used for background estimation.
; ---- Duration of background period in seconds.
backgroundPeriod = 86400
; ---- Files listing time lags to be used for background estimation.
;      Specify one lag file for each network type. (Note that lag files
;      are ignored for single-detector networks.)
; ---- Use the O1 Online lag files ----
lags_3det3site = /home/psutton/xpipeline/branches/scripts/input/O2/grb_offline/GRB170817A/lags_3det3site_400.txt


[parameters]
; ---- xdetection parameters.
analysisTimes = 2.0,1.0,0.5,0.25,0.125,0.0625,0.03125,0.015625,0.0078125
blockTime = 256
onSourceBeginOffset = -600
onSourceEndOffset = 60
likelihoodType_1det1site = loghbayesian,energyitf1,skypositiontheta,skypositionphi
likelihoodType_2det2site = loghbayesiancirc,standard,circenergy,circinc,circnullenergy,circnullinc,powerlaw,energyitf1,energyitf2,skypositiontheta,skypositionphi
likelihoodType_3det3site = loghbayesiancirc,standard,circenergy,circinc,circnullenergy,circnullinc,nullenergy,nullinc,powerlaw,energyitf1,energyitf2,energyitf3,skypositiontheta,skypositionphi
minimumFrequency = 20
maximumFrequency = 1000
offsetFraction = 0.5
outputType = clusters
sampleFrequency = 2048
verboseFlag = 1
whiteningTime = 4
seed = 1235
applyCalibCorrection = 1
circtimeslidestep = 1
genclusteringthresh = 1,48

[output]
; ---- This variable sets how many injections are processed by each
;      simulation job, and how many off-source segments are analysed
;      by each off-source job.  A larger number means fewer but longer
;      condor jobs.
maxInjNum = 1200
maxOffNum = 35
; ---- This variable specifies the maximum number of off-source segments 
;      each xdetection job should process when doing MDC injections in 
;      the off-source times.  Typically 5 is a good choice.
maxMDCSegNum = 5
; ---- Parameters controlling whether job output files are sprayed across
;      the cluster instead of returned to the local output/ directory.  
;      OBSOLETE: DO NOT TURN ON - merging and post-processing 
;      codes will not be able to find output files.  
distributeOnSource = 0
distributeOffSource = 0
distributeSimulation = 0


[input]
; ---- Detectors in O2 LIGO network.
detectorList = H1,L1,V1
; ---- List of channels for O2 analysis.
channelList = GDS-CALIB_STRAIN,GDS-CALIB_STRAIN,Hrec_hoft_16384Hz
; ---- List of frametypes for O2 analysis.
frameTypeList = H1_HOFT_C00,L1_HOFT_C00,V1Online
#frameCacheFile = /home/psutton/xpipeline/branches/scripts/input/O2/grb_offline/E298046/gated.cache
; ---- Parameters used to construct the sky grid searched over.
numSigmaSkyPos = 1.65
delayTol = 5e-4
usexchooseskylocations = 1


[datafind]
; ---- Data find server used for calls to datafind_exec.
datafind_server     = ldr.ldas.cit:80
; ---- Specify executables for finding frames and segments.
datafind_exec        = gw_data_find
segfind_exec         = ligolw_segment_query_dqsegdb
segs_from_cats_exec  = ligolw_segments_from_cats_dqsegdb
ligolw_print_exec    = ligolw_print


[segfind]
; ---- Options to be used with segfind_exec (segment finding tool) and 
;      segs_from_cats_exec (veto segment finding tool)
; ---- Required flag. Generate new segment lists (1) or use premade files (0).
generateSegs = 1
; ---- If you are going to generate segment lists on the fly then you need to 
;      specify segment-url and veto-file.
; ---- Segment database URL.
segment-url = https://segments.ligo.org
; ---- Category definer used by ligolw_segments_from_cats in order to generate
;      category 2-4 veto segment files.
;veto-file = /home/jordan.palamos/GRB_O2/offline_runs_2det_r5478/H1L1-HOFT_C01_O2_BURST.xml
;veto-file = /home/grb.exttrig/Online/O2/processor/params/X/H1L1V1-HOFT_C00_O2_BURST.xml
veto-file = /home/jordan.palamos/svn/scripts/input/O2/grb_offline/GRB170817A/H1L1V1-HOFT_C00_O2_BURSTGRB.xml
; ---- Optional flag. Only specify to use DMT files if running online, otherwise 
;      using segdb is significantly faster.
useDMTFile = 0


; ---- Segment information.  There should be one section per detector in 
;      detectorList. Give one of the following:
;        i)  a comma separated-list of segment types (data quality flags)
;            for each detector; or
;        ii) a pre-made segment list file in segwizard format, with an optional 
;            veto-list file.
[H1]
include-segments = H1:DMT-ANALYSIS_READY:1
;veto-list = /home/jordan.palamos/svn/scripts/input/O2/grb_offline/GRB170817A/grb170817a_H1_cat24veto.txt
;segment-list = /home/jordan.palamos/svn/scripts/input/O2/grb_offline/GRB170817A/grb170817a_H1_science_cat1.txt
[L1]
include-segments = L1:DMT-ANALYSIS_READY:1
;veto-file = /home/jordan.palamos/svn/scripts/input/O2/grb_offline/GRB170817A/grb170817a_L1_cat24veto.txt
;segment-list = /home/jordan.palamos/svn/scripts/input/O2/grb_offline/GRB170817A/grb170817a_L1_science_cat1.txt

; **** -- Get the right segment flag and version for Virgo! -- ****
[V1]
include-segments = V1:ITF_SCIENCE:2
;veto-list = /home/jordan.palamos/svn/scripts/input/O2/grb_offline/GRB170817A/grb170817a_V1_cat24veto.txt
;segment-list = /home/jordan.palamos/svn/scripts/input/O2/grb_offline/GRB170817A/grb170817a_V1_science_cat1.txt


;
[condor]
; ---- Condor parameters.
; ---- These are cluster (scratch disk) and user specific
universe = vanilla
dagman_log_on_source   = /local/psutton/grb_jobs_on_source.logs
dagman_log_off_source  = /local/psutton/grb_jobs_off_source.logs
dagman_log_simulations = /local/psutton/grb_jobs_simulations.logs
dagman_log_mdcs        = /local/psutton/grb_jobs_simulations.logs
ProdDevSim = prod
Era = o2
Group = burst
SearchType = grb.xoffline
UserName = patrick.sutton
retryNumber = 5


[injection]
; ---- Amplitude scales applied to all injections (MDC and on-the-fly).
injectionScales = 0.0100,0.0147,0.0215,0.0316,0.0464,0.0681,0.1000,0.1468,0.2154,0.3162,0.4642,0.6813,1.0000,1.4678,2.1544,3.1623,4.6416,6.8129,10.0000
; ---- Spacing of on-the-fly injections (ignored for MDCs). 
injectionInterval = -600
; ---- Apply estimated calibration uncertainties to injections (ignored for MDCs).
miscalibrateInjections = 1


[waveforms]
; ---- Sets of on-the-fly MVA injections to be performed
; ---- bns at 10 Mpc, nsbh at 20 Mpc, adi-a/c/d/e at 10Mpc, adi-b at 20Mpc - also, jitter in amplitude applied to MVA sets
;mva = chirplet!0.8254e-22;1.2115e-22;log~0.001;0.1;log~20;500;log~0~0;6.283;linear~0.996;1;linear,chirplet!0.8254e-22;1.2115e-22;log~0.001;0.1;log~20;500;log~0~0;6.283;linear~-1;-0.996;linear,inspiral!1.4;0.2;1;3;1.4;0.2;1;3;2;6;mass~0.866;1;linear~8.254;12.115;log,inspiral!1.4;0.2;1;3;10;6;2;25;3;25;mass~-1;-0.866;linear~16.5081;24.2306;log,adi-a!8.254;12.115;log~0.996;1;linear,adi-b!16.5081;24.2306;log~-1;-0.996;linear,adi-c!8.254;12.115;log~0.996;1;linear,adi-d!8.254;12.115;log~-1;-0.996;linear,adi-e!8.254;12.115;log~0.996;1;linear
;mediummixed = inspiral!1.4;0.2;1;3;1.4;0.2;1;3;2;6;mass~0.866;1;linear~8.254;12.115;log,inspiral!1.4;0.2;1;3;10;6;2;25;3;25;mass~-1;-0.866;linear~16.5081;24.2306;log
;longmixed = adi-a!8.254;12.115;log~0.996;1;linear,adi-b!16.5081;24.2306;log~-1;-0.996;linear,adi-c!8.254;12.115;log~0.996;1;linear,adi-d!8.254;12.115;log~-1;-0.996;linear,adi-e!8.254;12.115;log~0.996;1;linear
; ---- Sets of on-the-fly injections to be performed. Fixed amplitudes
;sgc70Q9  = chirplet!1.0e-22~0.0143~70~0~0~-1;-0.996;linear
;sgc100Q9 = chirplet!1.0e-22~0.01~100~0~0~0.996;1;linear
;sgc150Q9 = chirplet!1.0e-22~0.006667~150~0~0~-1;-0.996;linear
;sgc300Q9 = chirplet!1.0e-22~0.00333~300~0~0~0.996;1;linear
;bns = inspiral!1.4;0.2;1;3;1.4;0.2;1;3;2;6;mass~0.866;1;linear~10
;nsbh = inspiral!1.4;0.2;1;3;10;6;2;25;3;25;mass~-1;-0.866;linear~20
;adi-a  = adi-a!10~0.996;1;linear
;adi-b  = adi-b!20~-1;-0.996;linear
;adi-c  = adi-c!10~0.996;1;linear
;adi-d  = adi-d!10~-1;-0.996;linear
;adi-e  = adi-e!10~0.996;1;linear


[mdc]
; ---- Sets of MDC injections to be performed.

[xtmva]
; ---- The XTMVA analysis requires the following three variable to be specified.
; ;      WFtrain must be one of the waveform sets specified in [waveforms].
; ;      The tmvadir is the directory that olds the xtmva scripts. 
; ---- Also remember to source ~cadonati/root/bin/thisroot.sh prior to running grb.param
;      This is insofar the only root version that works with the XTMVA
; ---- Also remember to use different names for mixed WF collections for different runs:
;      see shortmixed, mediummixed, longmixed
;WFtrain    = mva
;classifier = BDT
; ---- This is the LOCAL user specific directory
;tmvadir    = /home/valeriu/O1XGRB/xtmvapy


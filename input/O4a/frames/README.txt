
O4a/b: see https://wiki.ligo.org/LSC/JRPComm/ObsRun4
           https://wiki.ligo.org/Bursts/O4
           https://git.ligo.org/detchar/veto-definitions/-/tree/master/burst/O4

  frames: [HL]1_HOFT_C00_AR
channels: [HL]1:GDS-CALIB_STRAIN_CLEAN_AR 

O4a start and end times are taken from https://wiki.ligo.org/LSC/JRPComm/ObsRun4 :

  start: 24 May 2023 15:00 UTC = 1368975618
    end: 16 Jan 2024 16:00 UTC = 1389456018

gw_data_find -o H --type H1_HOFT_C00_AR -s 1368975618 -e 1389456018 > O4a_H1_frames.txt
gw_data_find -o L --type L1_HOFT_C00_AR -s 1368975618 -e 1389456018 > O4a_L1_frames.txt



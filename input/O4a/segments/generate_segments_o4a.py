#!/usr/bin/env python

# -------------------------------------------------------------------------
#      Script to generate segment lists for a specified period.
# -------------------------------------------------------------------------


# -------------------------------------------------------------------------
#      Imports.
# -------------------------------------------------------------------------

import sys, os, shutil, math, random, copy, getopt, re, string, time
import configparser, glob, operator
from glue import pipeline
from glue.lal import CacheEntry
from numpy import loadtxt
import utils as setuputils
from gwpy.segments import SegmentList, Segment


# -------------------------------------------------------------------------
#      User inputs.
# -------------------------------------------------------------------------

# ---- O4a start and end times, from https://wiki.ligo.org/LSC/JRPComm/ObsRun4
start_time   = 1368975618 
end_time     = 1389456018
# ---- Use shortest possible blockTime value for x-pipeline for most inclusive segment lists.
blockTime = 64 
# ---- Generate segments for HL network (ignoring V).
detector = []
detector.append('H1')
detector.append('L1')
# ---- Specify an .ini file that points to the veto definer file and supplies the ANALYSIS_READY flags.
params_file = 'stub.ini'


# -------------------------------------------------------------------------
#      Generate segment lists.
# -------------------------------------------------------------------------

# ---- Read .ini file.
cp = configparser.ConfigParser()
cp.read(params_file)

# ---- Write our analysis duration as a gwpy segment list.
analysis_segment = SegmentList([Segment(start_time, end_time)])

# ---- Prepare storage for full segment lists.
full_segment_list = []

# ---- Prepare storage for lists of analysis and veto segment filenames.
analysis_seg_files, veto_seg_files = setuputils.generate_segments_and_vetos(detector, start_time, end_time, cp)

print(analysis_seg_files)
print(veto_seg_files)

# ---- Read in segment_lists to ScienceData object.
for (iFile, ifo) in zip(analysis_seg_files, detector):

    # ---- Read full segment list into gwpy
    #      Throw away science segment shorter than the
    #      blockTime value read from the configuration file.
    #logger.info('Reading segment list from file {0}'.format(iFile))
    print('Reading segment list from file {0}'.format(iFile))
    full_segment = SegmentList.read(iFile)
    #logger.info('... finished reading segment list.')
    #logger.info('Checking for segments less than {0}.'.format(blockTime))
    print('... finished reading segment list.')
    print('Checking for segments less than {0}.'.format(blockTime))
    full_segment = SegmentList([seg for seg in full_segment if abs(seg) > blockTime])
    #logger.info('Done')
    print('Done')

    # ---- Finally, append segment for this detector to the full list.
    full_segment_list.append(full_segment)



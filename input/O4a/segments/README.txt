This directory contains a script to generate the H1/L1_science_cat1.txt and 
H1/L1_cat24veto.txt files used by x-pipeline for O4a. It runs successfully 
under the igwn-py37 conda environment, though it is slow (about 2.5 hours 
in total)!

Usage:

  conda activate igwn-py37
  mkdir input
  python generate_segments_o4a.py

The O4a start/end times are specified in generate_segments_o4a.py. The veto 
definer file is specified in stub.ini. The generated files will appear in input/.

See https://git.ligo.org/detchar/veto-definitions/-/tree/master/burst/O4?ref_type=heads 




#!/bin/sh
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Run.sh - script to launch the xtmva analysis in a single command
# customize as needed
# @author Laura Cadonati, 03/2014 (update 06/2014)
# @version $Id: Run.sh 4731 2015-01-21 15:21:55Z patrick.sutton@LIGO.ORG $
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# select the run and how many detectors
GRBNAME=GRB160830; nifo=2
#GRBNAME=GRB100725B ; nifo=2
#GRBNAME=bert ; nifo=1

# toggle xtmva run and plots
doxtmva=0
doplots=1

# set the working areas: where grb.py is stored and where the grb output goes
GRBHOME=${HOME}/GRB/${GRBNAME}
GRBDIR=${GRBHOME}/xtmva_BDT/output

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
if [ $doxtmva == 1 ]; then
    echo "--------------------------------------------------------------------"
    echo "Run xtmva.py for GRB $GRBNAME ($nifo interferometers)"
    echo "For runtime options, type: xtmva.py -h"
    echo "--------------------------------------------------------------------"
    #./xtmva.py --grbname $GRBNAME -n $nifo --cname xtmva.ini --fdir ${HOME}/GRB
    ./xtmva.py --grbname $GRBNAME -n $nifo >& ${GRBNAME}.log
fi
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
if [ $doplots == 1 ]; then
    echo "--------------------------------------------------------------------"
    echo "Run xtmvaPlot.py to make plots in $GRBDIR"
    echo "For runtime options, type: xtmvaPlots.py -h"
    echo "--------------------------------------------------------------------"
    for run in 1 2 3; do
        ./xtmvaPlots.py -b -q --png --fdir ${GRBDIR} --fname TMVA_GRB_Output.root --wf ${run} -n $nifo --tmvaplots
        ./xtmvaPlots.py -b -q --png --fdir ${GRBDIR} --wf ${run} -n $nifo --BDTth -0.1
        ./xtmvaPlots.py -b -q --png --fdir ${GRBDIR} --wf ${run} -n $nifo --BDTth 0
    done
fi
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

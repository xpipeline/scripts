import ROOT

# Load the output of our test
untested = ROOT.TFile.Open("TMVA_GRB_Input.root")
# Load the expected results
expected = ROOT.TFile.Open("expectedResults/TMVA_GRB_Input.root")

# Count failed tests
counter = 0

# Check a selection of the results
print "\nTesting STEST "
for i in xrange(untested.STEST.GetEntries()):
    untested.STEST.GetEntry(i)
    expected.STEST.GetEntry(i)
    if untested.STEST.significance != expected.STEST.significance \
    or untested.STEST.nPixels != expected.STEST.nPixels \
    or untested.STEST.triggerIdx != expected.STEST.triggerIdx:
        counter += 1 
        print "\nSTEST tree in TMVA_GRB_Input.root made incorrectly.\n"
        break

print "\nTesting STEST16 "
try:
    untested.STEST16.GetEntries()
except AttributeError:
    counter += 1 
    print "\nSTEST16 tree in TMVA_GRB_Input.root does not exist.\n"
else:
    for i in xrange(untested.STEST16.GetEntries()):
        untested.STEST16.GetEntry(i)
        expected.STEST16.GetEntry(i)
        if untested.STEST16.significance != expected.STEST16.significance \
        or untested.STEST16.nPixels != expected.STEST16.nPixels \
        or untested.STEST16.triggerIdx != expected.STEST16.triggerIdx:
            counter += 1 
            print "\nSTEST16 tree in TMVA_GRB_Input.root made incorrectly.\n"
            break

print "\nTesting BTRAIN "
for i in xrange(untested.BTRAIN.GetEntries()):
    untested.BTRAIN.GetEntry(i)
    expected.BTRAIN.GetEntry(i)
    if untested.BTRAIN.significance != expected.BTRAIN.significance \
    or untested.BTRAIN.nPixels != expected.BTRAIN.nPixels \
    or untested.BTRAIN.triggerIdx != expected.BTRAIN.triggerIdx:
        counter += 1 
        print "\nBTRAIN tree in TMVA_GRB_Input.root made incorrectly.\n"
        break

print "\nTesting STRAIN "
for i in xrange(untested.STRAIN.GetEntries()):
    untested.STRAIN.GetEntry(i)
    expected.STRAIN.GetEntry(i)
    if untested.STRAIN.significance != expected.STRAIN.significance \
    or untested.STRAIN.nPixels != expected.STRAIN.nPixels \
    or untested.STRAIN.triggerIdx != expected.STRAIN.triggerIdx:
        counter += 1 
        print "\nSTRAIN tree in TMVA_GRB_Input.root made incorrectly.\n"
        break

print "\nTesting BTEST "
for i in xrange(untested.BTEST.GetEntries()):
    untested.BTEST.GetEntry(i)
    expected.BTEST.GetEntry(i)
    if untested.BTEST.significance != expected.BTEST.significance \
    or untested.BTEST.nPixels != expected.BTEST.nPixels \
    or untested.BTEST.triggerIdx != expected.BTEST.triggerIdx:
        counter += 1 
        print "\nBTEST tree in TMVA_GRB_Input.root made incorrectly.\n"
        break

print "\nTesting ONSOURCE "
for i in xrange(untested.ONSOURCE.GetEntries()):
    untested.ONSOURCE.GetEntry(i)
    expected.ONSOURCE.GetEntry(i)
    if untested.ONSOURCE.significance != expected.ONSOURCE.significance \
    or untested.ONSOURCE.nPixels != expected.ONSOURCE.nPixels \
    or untested.ONSOURCE.triggerIdx != expected.ONSOURCE.triggerIdx:
        counter += 1 
        print "\nONSOURCE tree in TMVA_GRB_Input.root made incorrectly.\n"
        break

# Print results of tests.
if counter==0:
    print "\nRootLoader.C passed all tests.\n"
else:
    print "\nRootLoader.C failed " + str(counter) + " tests.\n"


from xtmvaClassification import main
import ROOT
import time
import numpy

# Time the code
startTime = time.time()
# Run xtmvaCLassification
main()

timeTaken = time.time() - startTime 
print "xtmvaClassification.py ran in " + str(timeTaken) + "seconds."

# Test
#Load the Output root file and the expected output root file
untested = ROOT.TFile.Open("TMVA_GRB_Output.root")
expected = ROOT.TFile.Open("expectedResults/TMVA_GRB_Output.root")
#Count number of files that fail tests
counter = 0
# Loop through tree events and check against expected results
print "Checking TrainTreeEntries."
for ievt in xrange(untested.TrainTree.GetEntries()):
    if ievt%1000==0:
        print "--- ... Processing event: ", ievt
    #Select corresponding events and compare a few of their attributes
    #Ideally would test all of them, but that would be slow
    untested.TrainTree.GetEntry(ievt)
    check1=[untested.TrainTree.BDT, untested.TrainTree.nPixels, untested.TrainTree.uniqIdx]
    expected.TrainTree.GetEntry(ievt)
    check2=[expected.TrainTree.BDT, expected.TrainTree.nPixels, expected.TrainTree.uniqIdx]
    if check1 != check2:
        counter += 1
        print "\nTMVA_GRB_Output.root contains incorrect values in TrainTree"
        break

print "Checking TestTree entries."
for ievt in xrange(untested.TestTree.GetEntries()):
    if ievt%1000==0:
        print "--- ... Processing event: ", ievt
    untested.TestTree.GetEntry(ievt)
    check1 = [untested.TestTree.BDT, untested.TestTree.nPixels, untested.TestTree.uniqIdx]
    expected.TestTree.GetEntry(ievt)
    check2 = [expected.TestTree.BDT, expected.TestTree.nPixels, expected.TestTree.uniqIdx]
    if check1 != check2:
        counter += 1
        print "\nTMVA_GRB_Output.root contains incorrect values in TestTree"
        break

if counter != 0:
    print "\n xtmvaClassification.py failed " + str(counter) + " test(s)."
else:
    print "\n xtmvaClassification.py passed all tests." 

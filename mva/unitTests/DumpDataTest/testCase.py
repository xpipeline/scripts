import numpy as np
import time

def testDumpData():
    from DumpData import DumpData

    startTime = time.time()
    DumpData('BDT', 2, "likelihoods.dat", "output")
    endTime = time.time()
    print "\nDumpData.py ran in " + str(endTime-startTime) + "seconds\n"
    print "\n\n-----TESTING DumpData.py-----\n\n"

    testFailCounter = 0

    txtFileList = ["Output_background_testing.txt", "Output_background_training.txt", "temp_signal_testing.txt", "temp_signal_training.txt", "Output_on_source.txt"]
    for txtFile in txtFileList:
        if (np.loadtxt(txtFile) == np.loadtxt("expectedResults/"+txtFile)).all() == False:
            print "ERROR IN DumpData.py: " + txtFile + " TEST FAILED"
            testFailCounter += 1
        else:
            print txtFile + " .....passed test"

    print "\n\n Number of failed tests: " + str(testFailCounter)


if __name__ == "__main__":
    testDumpData()

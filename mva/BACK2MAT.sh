#  BACK2MAT.sh

#  Shell script that converts ROOT files from xtmva.py analysis back to matlab
#  files that can be read by x-pipeline. This script should be converted to 
#  python and added as the final section of xtmva.py.
#
#  GRBDIR        The full path to the directory where the x-pipeline analysis
#                was performed (e.g., containing grb_alljpb.dag).
#  TMVADIR       The full path to the directory containing this script and xtmva.py.
#  FOLDEROUT     The full path to the directory containing the TMVA_GRB_Output.root 
#                file and weight directory (folder_out in xtmva.py).
#  CLASSIFIER    The name of the classifier to be used.
#  NUMIFOS       Number of interferometers used. This can only process runs with
#                either 1, 2 or 3 detectors. CURRENTLY ONLY FUNCTIONAL FOR 1 DETECTOR.
#  FOLDERIN      The full path to the directory containing the ASCII trigger files 
#                used as input to TMVA (folder_in in xtmva.py).
#  DATFILE       The name (with full path) of the DATFILE produced by xtmva.py.
#
# example:
# /BACK2MAT.sh /home/psutton/GRB/bert_clean/ /home/psutton/xtmvapy_test /home/psutton/GRB/bert_clean/xtmva_BDT/output/ BDT 1 /home/psutton/GRB/bert_clean/xtmva_BDT/input /home/psutton/GRB/bert_clean/bert_clean_files.dat
#
#  Adapted from a script by Thomas Adams.
#  $Id: BACK2MAT.sh 5391 2016-11-07 10:27:35Z iain.dorrington@LIGO.ORG $

# --- Give the input command-line arguments intelligible names.
export GRBDIR=$1      # eg: /home/psutton/GRB/bert_clean/
export TMVADIR=$2     #     /home/psutton/xtmvapy_test 
export FOLDEROUT=$3   #     /home/psutton/GRB/bert_clean/xtmva_BDT/output/
export CLASSIFIER=$4  #     BDT, BDTG, BDTD, KNN, MLP, Likelihood, Fisher, LD
export NUMIFOS=$5     #     1 or 2 or 3
export FOLDERIN=$6    #     /home/psutton/GRB/bert_clean/xtmva_BDT/input
export DATFILE=$7     #     /home/psutton/GRB/bert_clean/bert_clean_files.dat

# --- Write command to file
echo "$0 $GRBDIR $TMVADIR $FOLDEROUT $CLASSIFIER $NUMIFOS $FOLDERIN $DATFILE" > RUN-command.txt

# --- Checks to make sure that there are the correct number of input commands
if [ $# -ne 7 ]
then
  echo "Error in $0 - Invalid Argument Count"
  echo "Syntax: $0 GRBDIR TMVADIR FOLDEROUT CLASSIFIER NIFOs FOLDERIN DATFILE"
  exit 1
fi

# --- Checks to see if the classifier method that is selected is able to be
#     processed
if [[ $CLASSIFIER != BDT && $CLASSIFIER != BDTG && $CLASSIFIER != BDTD && $CLASSIFIER != BDTB && $CLASSIFIER != MLP && $CLASSIFIER != Likelihood && $CLASSIFIER != Fisher && $CLASSIFIER != LD ]]
then
  echo "Error in fourth argument (you specified: $CLASSIFIER) - Invalid Input"
  echo "Should be either: BDT, BDTG, BDTD, BDTB, MLP, Likelihod, Fisher, LD"
  exit 2
fi

# --- Checks to see if the output.root file has been created
if [ ! -f $FOLDEROUT/TMVA_GRB_Output.root ]; then
  echo "ERROR: .root file ($FOLDEROUT/TMVA_GRB_Output.root) was not produced"
  exit 3
fi

# --- Calls Dumpdata which takes the output.root file and extracts the training/
#     testing signal/background data and produces output.txt files. For the
#     signal data, temp files are created as the original file location paths
#     need to be added onto them.
# ---- Delete any prexisting output files.
rm -f $TMVADIR/temp_signal_*.txt $TMVADIR/Output_*.txt temp_signal_*.txt Output_*.txt
# ---- Call DUMPDATA.
DUMPDATA=DumpData.py
echo "Calling $DUMPDATA."
echo "python $TMVADIR/$DUMPDATA $CLASSIFIER $NUMIFOS $TMVADIR/likelihoods.dat $FOLDEROUT" 
python $TMVADIR/$DUMPDATA $CLASSIFIER $NUMIFOS $TMVADIR/likelihoods.dat $FOLDEROUT 

# ---- Split injection testing triggers into separate files for each waveform set.
#      First parse *_files.dat file to see how many waveform sets there are 
#      and how they're numbered.
echo "Splitting injection testing triggers into separate files for each waveform set."
# ---- Get SIG as well as testing-only waveforms sets, e.g.:
#        /home/psutton/GRB/GRB160830/xtmva_BDT/input/A_SIG_test.txt       1
#        /home/psutton/GRB/GRB160830/xtmva_BDT/input/A_nsns_test.txt   2
#        /home/psutton/GRB/GRB160830/xtmva_BDT/input/A_csgfixed_test.txt   3
#      Note: it may be possible to simplify this code greatly now that the injection scale is also provided 
#      in the A_* ASCII files. We would just eed to add that as output in DumpData.C. 
grep STEST $DATFILE | sed 's/STEST//g' | awk '{if (NF==1) print $0, "1" ; else print $0;}' > .injmapall.txt
while read filename wfnumber
do 
  echo "Retrieving the original file names for the triggers from injection set ${wfnumber}."
  # ---- Determine name of original mat2ascii file -- the one without the "A_" 
  #      prefix. This has the matlab file name.
  dirname=$(dirname $filename)
  Aname=$(basename $filename)
  name=${dirname}/${Aname:2}
  # ---- Pull out just the last two columns: uniqIdx, matFile (without the header line).
  awk '{if (NF>1) print $(NF-1), $NF}' ${name} > names.txt
  # ---- From the MVA output file, pull out injections for this waveform set, 
  #      discarding the last column (matFile number), which we no longer need.
  echo "\$NF==${wfnumber} {print \$1, \$2, \$3, \$4}" > awkfile
  awk -f awkfile temp_signal_testing.txt > tmp1.txt
  # ---- Use join to merge the matFile names from names.txt into the signal_testing file.
  #      For some reason the following line has stopped working recently. Try splitting into smaller steps...
  #join -1 4 -2 1 <(sort -k 4 -n tmp1.txt) <(sort -k 1 -n names.txt) > tmp2.txt 
  sort -k 4 -n tmp1.txt  > fu
  sort -k 1 -n names.txt > bar 
  join -1 4 -2 1 fu bar > tmp2.txt 
  #
  awk '{print $2, $3, $4, $5}' tmp2.txt > $TMVADIR/Output_signal_testing_${wfnumber}.txt
  #join -1 4 -2 1 <(sort -k 4 -n temp_signal_testing_${wfnumber}.txt) <(sort -k 1 -n names.txt) > tmp_${wfnumber}.txt 
done < .injmapall.txt
cp $TMVADIR/Output_signal_testing_1.txt $TMVADIR/Output_signal_testing.txt
# ---- Don't forget the signal_training set!
awk '{if (NF>1) print $(NF-1), $NF}' ${FOLDERIN}/SIG_train.txt > names.txt
awk '{print $1, $2, $3, $4}' temp_signal_training.txt > tmp1.txt
# ---- Use join to merge the matFile names from names.txt into the signal_testing file.
#join -1 4 -2 1 <(sort -k 4 -n tmp1.txt) <(sort -k 1 -n names.txt) > tmp2.txt 
sort -k 4 -n tmp1.txt  > fu
sort -k 1 -n names.txt > bar 
join -1 4 -2 1 fu bar > tmp2.txt 
awk '{print $2, $3, $4, $5}' tmp2.txt > $TMVADIR/Output_signal_training.txt

# ---- Clean up.
mv Output_*.txt $TMVADIR
rm -f .injmap*.txt awkfile tmp1.txt tmp2.txt names.txt temp_signal_t*ing.txt fu bar 

# --- Check that Output_signal_*.txt files have been created
if [ ! -f $TMVADIR/Output_signal_training.txt ]; then
  echo "ERROR: .root file ($TMVADIR/Output_signal_training.txt) was not produced"
  exit 4
fi
if [ ! -f $TMVADIR/Output_signal_testing.txt ]; then
  echo "ERROR: .root file ($TMVADIR/Output_signal_testing.txt) was not produced"
  exit 5
fi
echo "WARNING: Should add checks for the other signal output files/"

# --- Calls xwriteallmat which converts the output.txt files back into 
#     merged.mat files in the original output directory ($FOLDEROUT) with the
#     MVA significance value replacing the X-Pipeline significance value.
echo " "
echo "Calling xwriteallmat"
echo "xwriteallmat $GRBDIR/output $TMVADIR $FOLDEROUT"
xwriteallmat $GRBDIR/output $TMVADIR $FOLDEROUT
echo "... finished call to xwriteallmat"
# ---- Repeat for all the other (testing-only) waveform sets.
grep STEST $DATFILE | sed 's/STEST//g' | awk '$NF>1' > .injmap.txt
awk -F/ '{print $NF}' .injmap.txt  | sed 's/A_//' > .injmapstripped.txt
while read filename wfnumber
do 
  echo " "
  echo "Calling xwriteinjmatfilefromMVAascii"
  echo "xwriteinjmatfilefromMVAascii $GRBDIR/output [] $TMVADIR/Output_signal_testing_${wfnumber}.txt $FOLDEROUT"
  xwriteinjmatfilefromMVAascii $GRBDIR/output [] $TMVADIR/Output_signal_testing_${wfnumber}.txt $FOLDEROUT
  echo "... finished call to xwriteinjmatfilefromMVAascii"
done < .injmapstripped.txt
rm -f .injmap*.txt 
# ---- Move Output*.txt files to FOLDEROUT. These files are no longer needed, 
#      but are useful for debugging.
rm -f $FOLDEROUT/Output_*.txt
mv $TMVADIR/Output_*.txt $FOLDEROUT


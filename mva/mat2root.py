#!/usr/bin/env python
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# xtmva.py : replaces xtmva.sh
# reads xtmva.ini configuration file
#
# L. Cadonati April 2014
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# initialize
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import re, os, sys, glob
import datetime,time
from optparse import OptionParser
import ConfigParser

from numpy import *
import scipy.io as sio

from ROOT import *

tstart=time.time()

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def maketree(struct, jobs, treename, isinj):
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    """
    header='significance/F:'
    for iLike in range(len(likelihoodtypes)):
        header=header+str(likelihoodtypes[iLike][0])+'/F:'
    header=header+'jobNumber/I:nPixels/I:peakTime/F:peakFrequency/F:'
    header=header+'minTime/F:minFreq/F:deltaTime/F:deltaFreq/F:'
    header=header+'injIdx/I:triggerIdx/I:uniqIdx/I:filename/S'
    #print header
    """

    tree = TTree( treename, 'Converted from matlab file' )

    # create 1 dimensional float arrays as fill variables
    # (python's float datatype corresponds to c++ doubles)

    fSignificance=zeros(1, dtype=float)
    nLike=len(likelihoodtypes)
    ltypes=[]
    for iLike in range(nLike): ltypes.append(likelihoodtypes[iLike][0])
    fL0=zeros(1, dtype=float)
    fL1=zeros(1, dtype=float)
    fL2=zeros(1, dtype=float)
    fL3=zeros(1, dtype=float)
    fL4=zeros(1, dtype=float)
    fL5=zeros(1, dtype=float)
    fL6=zeros(1, dtype=float)
    fL7=zeros(1, dtype=float)
    fL8=zeros(1, dtype=float)
    fL9=zeros(1, dtype=float)
    fL10=zeros(1, dtype=float)
    fL11=zeros(1, dtype=float)
    fL12=zeros(1, dtype=float)
    fL13=zeros(1, dtype=float)
    jobNumber=zeros(1, dtype=int)
    nPixels=zeros(1, dtype=int)
    fPeakTime=zeros(1, dtype=float)
    fPeakFrequency=zeros(1, dtype=float)
    fMinTime=zeros(1, dtype=float)
    fMinFreq=zeros(1, dtype=float)
    fDeltaTime=zeros(1, dtype=float)
    fDeltaFreq=zeros(1, dtype=float)
    injIdx=zeros(1, dtype=int)
    triggerIdx=zeros(1, dtype=int)
    uniqIdx=zeros(1, dtype=int)
    matfile=zeros(1, dtype=int)
    
    var='significance' ; data=struct[var][0,0]
    tree.Branch( 'significance', fSignificance , 'significance/D')
    tree.Branch( ltypes[0],   fL0,  ltypes[0]+'/D')
    tree.Branch( ltypes[1],   fL1,  ltypes[1]+'/D')
    tree.Branch( ltypes[2],   fL2,  ltypes[2]+'/D')
    tree.Branch( ltypes[3],   fL3,  ltypes[3]+'/D')
    tree.Branch( ltypes[4],   fL4,  ltypes[4]+'/D')
    tree.Branch( ltypes[5],   fL5,  ltypes[5]+'/D')
    tree.Branch( ltypes[6],   fL6,  ltypes[6]+'/D')
    tree.Branch( ltypes[7],   fL7,  ltypes[7]+'/D')
    tree.Branch( ltypes[8],   fL8,  ltypes[8]+'/D')
    tree.Branch( ltypes[9],   fL9,  ltypes[9]+'/D')
    tree.Branch( ltypes[10],  fL10, ltypes[10]+'/D')
    tree.Branch( ltypes[11],  fL11, ltypes[11]+'/D')
    tree.Branch( ltypes[12],  fL12, ltypes[12]+'/D')
    tree.Branch( ltypes[13],  fL13, ltypes[13]+'/D')
    tree.Branch( 'jobNumber', jobNumber, 'jobNumber/I')
    tree.Branch( 'nPixels',   nPixels, 'nPixels/I')
    tree.Branch( 'peakTime',      fPeakTime ,      'peakTime/D')
    tree.Branch( 'peakFrequency', fPeakFrequency , 'peakFrequency/D')
    tree.Branch( 'minTime',       fMinTime ,       'minTime/D')
    tree.Branch( 'minFreq',       fMinFreq ,       'minFreq/D')
    tree.Branch( 'deltaTime',     fDeltaTime ,     'deltaTime/D')
    tree.Branch( 'deltaFreq',     fDeltaFreq ,     'deltaFreq/D')
    tree.Branch( 'injIdx',     injIdx,     'jobNumber/I')
    tree.Branch( 'triggerIdx', triggerIdx, 'triggerIdx/I')
    tree.Branch( 'uniqIdx',    uniqIdx,    'uniqIdx/I')
    tree.Branch( 'matfile',    matfile,    'matfile/I')

    Nev=len(struct['jobNumber'][0,0])
    
    if isinj:
        for iFile in range(len(injFiles)):
            #for iFile in range(1):
            # --- parse filename to get injection scale
            injparse=str.split(injFiles[iFile],'_')
            inj_scale=injparse[-4]
            if options.verbose:
                print injFiles[iFile],inj_scale
            inj_data = sio.loadmat(injFiles[iFile],struct_as_record=True,squeeze_me=False)
            inj_struct=inj_data['clusterInj']
            
            # number of injections (600 in test runs)
            Nev=inj_struct['jobNumber'].shape[0]

            # loop through injections
            # for i in range(Nev):
            for i in range(Nev):
                if i not in jobs: continue
                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                # there are multiple triggers for each injection
                # only keep the one with loudest significance!
                # NOTE: matlab script also checks bounding box within a
                # certain window of injection time. That should be done
                # at trigger preparation time though. Check with Patrick.
                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                ntrig=len(inj_struct['peakTime'][i,0])
                if ntrig==0: continue
                asig=inj_struct['significance'][i,0]
                ikeep=asig.argmax()
                #print i, ntrig,inj_struct['significance'][i,0],asig[ikeep]
                #print i, ikeep,(inj_struct['boundingBox'][i,0][0])-inj_struct['peakTime'][i,0],asig[ikeep]

                fSignificance[0] = inj_struct['significance'][i,0][ikeep]
                fL0[0] = inj_struct['likelihood'][i,0][ikeep][0]
                fL1[0] = inj_struct['likelihood'][i,0][ikeep][1]
                fL2[0] = inj_struct['likelihood'][i,0][ikeep][2]
                fL3[0] = inj_struct['likelihood'][i,0][ikeep][3]
                fL4[0] = inj_struct['likelihood'][i,0][ikeep][4]
                fL5[0] = inj_struct['likelihood'][i,0][ikeep][5]
                fL6[0] = inj_struct['likelihood'][i,0][ikeep][6]
                fL7[0] = inj_struct['likelihood'][i,0][ikeep][7]
                fL8[0] = inj_struct['likelihood'][i,0][ikeep][8]
                fL9[0] = inj_struct['likelihood'][i,0][ikeep][9]
                fL10[0] = inj_struct['likelihood'][i,0][ikeep][10]
                fL11[0] = inj_struct['likelihood'][i,0][ikeep][11]
                fL12[0] = inj_struct['likelihood'][i,0][ikeep][12]
                fL13[0] = inj_struct['likelihood'][i,0][ikeep][13]
                jobNumber[0]  = inj_struct['jobNumber'][i,0][ikeep]
                nPixels[0]    = inj_struct['nPixels'][i,0][ikeep]
                fPeakTime[0]  = inj_struct['peakTime'][i,0][ikeep]
                fPeakFrequency[0] = inj_struct['peakFrequency'][i,0][ikeep]
                fMinTime[0]   = inj_struct['boundingBox'][i,0][ikeep][0]
                fMinFreq[0]   = inj_struct['boundingBox'][i,0][ikeep][1]
                fDeltaTime[0] = inj_struct['boundingBox'][i,0][ikeep][2]
                fDeltaFreq[0] = inj_struct['boundingBox'][i,0][ikeep][3]
                injIdx[0]     = inj_scale
                triggerIdx[0] = i
                uniqIdx[0]    = i
                matfile[0]     = inj_scale

                tree.Fill()
        print "next file"

    else:
        previousjob=-1
        for i in range(Nev):
            
            triggerIdx[0] = i
            jn = struct['jobNumber'][0,0][i]
            if(not i % 50000): print i,jn
        
            if jn != previousjob:
                #print jn, previousjob, i
                previousjob=jn
                mask=False
                if jn in jobs:
                    mask=True
            
            if mask:

                jobNumber[0]=jn
                fSignificance[0] = struct['significance'][0,0][i]
                fL0[0] = struct['likelihood'][0,0][i][0]
                fL1[0] = struct['likelihood'][0,0][i][1]
                fL2[0] = struct['likelihood'][0,0][i][2]
                fL3[0] = struct['likelihood'][0,0][i][3]
                fL4[0] = struct['likelihood'][0,0][i][4]
                fL5[0] = struct['likelihood'][0,0][i][5]
                fL6[0] = struct['likelihood'][0,0][i][6]
                fL7[0] = struct['likelihood'][0,0][i][7]
                fL8[0] = struct['likelihood'][0,0][i][8]
                fL9[0] = struct['likelihood'][0,0][i][9]
                fL10[0] = struct['likelihood'][0,0][i][10]
                fL11[0] = struct['likelihood'][0,0][i][11]
                fL12[0] = struct['likelihood'][0,0][i][12]
                fL13[0] = struct['likelihood'][0,0][i][13]
                nPixels[0]    = struct['nPixels'][0,0][i]
                fPeakTime[0]  = struct['peakTime'][0,0][i]
                fPeakFrequency[0] = struct['peakFrequency'][0,0][i]
                fMinTime[0]   = struct['boundingBox'][0,0][i][0]
                fMinFreq[0]   = struct['boundingBox'][0,0][i][1]
                fDeltaTime[0] = struct['boundingBox'][0,0][i][2]
                fDeltaFreq[0] = struct['boundingBox'][0,0][i][3]
                uniqIdx[0]+=1

                tree.Fill()

    tree.Write()


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# START HERE: PARSE INPUT ARGUMENTS
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
parser = OptionParser()
# run options
parser.add_option("-q", "--quiet", action="store_false", dest="verbose",
                  default=True,
                  help="don't print status messages to stdout")
# name options
parser.add_option("--grbname",type="string",action="store",dest="grbname",
                  default="GRB060223-cleaned", help="The GRB to be analyzed")
parser.add_option("--fdir",type="string",action="store",dest="fdir",
                  default=os.environ['HOME']+"/GRB",
                  help="Working directory [default: %default]")
parser.add_option("--cname",type="string",action="store",dest="cname",
                  default="xtmva.ini",
                  help="xtmva configuration file [default: %default]")

parser.add_option("-n","--nifo",type="int",action="store",dest="nifo",
                  default=3, help="number of detectors [default: %default]")

(options, args) = parser.parse_args()
 
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# ---- Record the current working directory in a string.
cwdstr = "."
print >> sys.stdout, "####################################################"
print >> sys.stdout, "#              Matlab 2 ROOT                       #"
print >> sys.stdout, "####################################################"
divider='----------------------------------------------------------------------'

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Parse configuration file.
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
fdir=options.fdir+"/"+options.grbname
cname=fdir+"/"+options.cname
print >> sys.stdout, "Parsing parameters (ini) file:", cname

if not os.path.isfile(cname):
    print >> sys.stderr,"Error: non existent parameter file: ", cname
    sys.exit(1)

cp = ConfigParser.ConfigParser()
cp.read(cname)

# ---- Create configuration-file-parser object and read parameters file.
if cp.has_option('tags','version'): ini_version = cp.get('tags','version')
if cp.has_option('tags','user-tag'):
    usertag = cp.get('tags','user-tag')
    print >> sys.stdout, "Parameter file tag:", ini_version,usertag

tmvadir=cp.get('Folders','tmvadir')
xout=fdir+'/'+cp.get('Folders','xout')
folder_in=xout+"/input"
folder_out=xout+"/output"
signal=cp.get('Settings','WFtrain')
injGlob=signal

if cp.has_section('WFtest'):
    wsets = cp.options('WFtest')
else:
    wsets=[]

print >> sys.stdout, divider
print >> sys.stdout, "Input folder:", folder_in
print >> sys.stdout, "Output folder:", folder_out
print >> sys.stdout, "injGlob:", injGlob

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# matlab to ROOT conversion
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
tick=time.time()
print divider
cmdstr="xwriteallascii "+fdir+" "+signal
print "***",cmdstr
print divider

# --- Input mat files
on_matFile = fdir+"/output/on_source_0_0_merged.mat"
off_matFile = fdir+"/output/off_source_0_0_merged.mat"
injDir = fdir+"/output/"

# --- Output ascii files
BG_training_file  = fdir+"/output/BG_train.txt"
BG_testing_file   = fdir+"/output/BG_test.txt"
ON_file           = fdir+"/output/ON_test.txt"
SIG_training_file = fdir+"/output/SIG_train.txt"
SIG_testing_file  = fdir+"/output/SIG_test.txt"

# --- Load mat files into python dictionaries with scipy.io
# http://docs.scipy.org/doc/scipy-0.13.0/reference/generated/scipy.io.loadmat.html
print >> sys.stdout, "Loading on_source_file:", on_matFile
on_data  = sio.loadmat(on_matFile,struct_as_record=True,squeeze_me=False)
on_struct=on_data['cluster']

print >> sys.stdout, "Loading off_source_file:", off_matFile
off_data = sio.loadmat(off_matFile,struct_as_record=True,squeeze_me=False)
off_struct=off_data['cluster']

fieldnames=off_struct.dtype.names
likelihoodtypes=off_data['likelihoodType'][:,0]

# --- Print information on the structure
if options.verbose:
    print >> sys.stdout, divider
    print >> sys.stdout, 'THIS IS THE MATFILE STRUCTURE - Output of sio.whosmat(on_matFile):'
    print >> sys.stdout, divider
    print >> sys.stdout, sio.whosmat(on_matFile)
    print >> sys.stdout, " "
    print >> sys.stdout, "on_struct.dtype.names:"
    print >> sys.stdout, on_struct.dtype.names
    print >> sys.stdout, " "
    print >> sys.stdout, "on_struct.dtype.fields:"
    print >> sys.stdout, on_struct.dtype.fields
    print >> sys.stdout, " "
    print >> sys.stdout, "on_struct.shape:",on_struct.shape
    print >> sys.stdout, "likelihoodtypes:"
    print >> sys.stdout, likelihoodtypes
    print >> sys.stdout, divider
    print >> sys.stdout, "NOW PRINT SAMPLE EVENTS:"
    print >> sys.stdout, divider
    #for f in fieldnames: print >> sys.stdout, f,off_struct[f][0,0][0]
    #print >> sys.stdout, divider
    ## Uncomment these if you want to see more sample events
    #for f in fieldnames: print >> sys.stdout, f,off_struct[f][0,0][15791]
    #print >> sys.stdout, divider
    #for f in fieldnames: print >> sys.stdout, f,off_struct[f][0,0][202]
    #print >> sys.stdout, divider

# --------------------------------------------------------------------------
#  Select BACKGROUND Jobs For TMVA Training & Testing
#  This is where we split the total events into sets using a random number
#  generator. Result will be a list of jobs to be used for either Training or Testing
# --------------------------------------------------------------------------
print >> sys.stdout, divider
print >> sys.stdout, 'Make a list of BACKGROUND Jobs For TMVA Training & Testing'
print >> sys.stdout, divider

# --- Get total number of off-source JOBS (note jobs is not events)
a=off_struct['jobNumber'][0,0]
if options.verbose: 
    print >> sys.stdout, "Number of off-source jobs (not unique):"
    print >> sys.stdout, "off_struct['jobNumber'][0,0].shape =",a.shape

uniqueOffJobNumber = unique(off_struct['jobNumber'][0,0])
nOffJobs    = len(uniqueOffJobNumber)
nOffJobsBy2 = int(ceil(nOffJobs/2))
if options.verbose:
    print >> sys.stdout, "Number of offline jobs =",nOffJobs
    print >> sys.stdout, "Divided by 2 =",nOffJobsBy2

# --- Get center time for analysis to generate random seed
analysis_centerTime =  unique(on_struct['centerTime'][0,0])[0]
if options.verbose:
    print >> sys.stdout, "Analysis center time =",analysis_centerTime

# --- Set random seed for job selection
initialSeed=931316785+analysis_centerTime
print >> sys.stdout, 'initial random seed: ',initialSeed
#random.seed(initialSeed)
#print 'random.rand() = ',random.rand()

# ---- Get random order of indices between 1 and nOffJobs
# ---- Use the first half of randomly ordered injections for tuning.
# ---- Use the second half of randomly ordered injections for testing.
offJobSeed = initialSeed+11
random.seed(offJobSeed)
randOffIdx=random.permutation(nOffJobs)

BG_training_jobs=randOffIdx[0:nOffJobsBy2]
BG_testing_jobs=randOffIdx[nOffJobsBy2:nOffJobs]
if options.verbose:
    print >> sys.stdout, 'train jobs:',len(BG_training_jobs),BG_training_jobs
    print >> sys.stdout, 'test jobs:',len(BG_testing_jobs),BG_testing_jobs
    print >> sys.stdout, divider

# --------------------------------------------------------------------------
## Select SIGNAL (injection) jobs for TMVA Training & Testing
# --------------------------------------------------------------------------
# ---- Use the first merged injection file to find number of injections
# ---- We then assume this is the same for all files
# --------------------------------------------------------------------------
print >> sys.stdout, divider
print >> sys.stdout, 'Make a list of SIGNAL Jobs For TMVA Training & Testing'
print >> sys.stdout, divider

injFiles=glob.glob(injDir+'*'+signal+'*merged.mat')
print >> sys.stdout, "Loading on_source_file:", injFiles[0]
first_inj_file=sio.loadmat(injFiles[0],struct_as_record=True,squeeze_me=False)
inj_struct=first_inj_file['clusterInj']
fieldnames2=inj_struct.dtype.names
#likelihoodtypes2=inj_data['likelihoodType'][:,0]

if options.verbose:
    print sio.whosmat(injFiles[0]); print divider
    print "inj_struct.dtype.names:"
    print inj_struct.dtype.names
    print " "
    print "inj_struct.dtype.fields:"
    print inj_struct.dtype.fields
    print " "
    print "inj_struct.shape:",inj_struct.shape
    print "NOW PRINT A FEW SAMPLE EVENTS:"
    print divider
    #for f in fieldnames2: print f,inj_struct[f][0,0][0]
    print divider
    #for f in fieldnames2: print f,inj_struct[f][299,0][0]
    print divider
    #for f in fieldnames2: print f,inj_struct[f][400,0][0]
    print divider

nInjections=first_inj_file['injectionProcessedMask'].shape[0]
nInjectionsBy2 = int(ceil(nInjections/2))
if options.verbose:
    print divider
    print "Number of injections per file = ",nInjections

# ---- Get random order of indices between 1 and nInjections.
# ---- Use the first half of randomly ordered injections for tuning.
# ---- Use the second half for testing.

random.seed(initialSeed+22)
randInjIdx = random.permutation(nInjections)
SIG_training_jobs = sort(randInjIdx[0:nInjectionsBy2])
SIG_testing_jobs = sort(randInjIdx[nInjectionsBy2+1:nInjections])
if options.verbose:
    print 'train jobs:',len(SIG_training_jobs),SIG_training_jobs
    print 'test jobs:',len(SIG_testing_jobs),SIG_testing_jobs

# --------------------------------------------------------------------------
# Produce root files for TMVA input
# --------------------------------------------------------------------------

f=TFile('TMVA_GRB_Input.root','RECREATE')

###def maketree(struct, jobs, treename, isinj):

# --- On-source data
maketree(on_struct, [0], 'ONSOURCE',0)

# --- Background Training Data
maketree(off_struct, BG_training_jobs, 'BTRAIN',0)

# --- Background Testing Data
maketree(off_struct, BG_testing_jobs, 'BTEST',0)

# --- Signal Training Data
maketree(inj_struct, SIG_training_jobs, 'STRAIN',1)

# --- Signal Testing data
maketree(inj_struct, SIG_testing_jobs, 'STEST',1)

# --------------------------------------------------------------------------
# Now the additional testing sets
# --------------------------------------------------------------------------
id=1
for wf in wsets:
    injFiles=glob.glob(injDir+'*'+wf+'*merged.mat')
    print >> sys.stdout, "Loading on_source_file:", injFiles[0]
    first_inj_file=sio.loadmat(injFiles[0],struct_as_record=True,squeeze_me=False)
    inj_struct=first_inj_file['clusterInj']
    nInjections=first_inj_file['injectionProcessedMask'].shape[0]
    SIG_jobs=arange(0,nInjections,1)

    id=id+1
    label='STEST'+str(id)
    maketree(inj_struct, SIG_jobs,label,1)

# --------------------------------------------------------------------------

f.Close()
print divider
T=time.time()-tick
print "mat2root: ", T/60, "minutes"


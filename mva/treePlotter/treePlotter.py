# -*- coding: utf-8 -*-
"""
Created on Tue Apr 19 16:29:32 2016

@author: iain
"""


#==============================================================================
# This script reads in data about the trees from multiple X-pipeline MVA runs
# and creates a plot to illustrate what the forrest of trees was doing for this
# particular run. The plots show what statistics were used to make cuts
# and at what depth the cuts were made for each forrest, with the thickness/
#colour of each line corresponding to the number of trees in that forrest
# that made that cut at that depth.
#==============================================================================

import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import os
import argparse
import sys



#==============================================================================
# The line function plots a straight line between two points
#==============================================================================

def line(x1,y1,x2,y2,plot=False,thickness=1.0):
    x1=float(x1)
    x2=float(x2)
    y1=float(y1)
    y2=float(y2)
    if x2==x1:  #Can't have infinite gradient
        x2=x2+1e-8    
    m=(y2-y1)/(x2-x1)
    x=np.linspace(min(x1,x2),max(x1,x2),100)
    c=y2-m*x2
    y=m*x+c  
    if plot==True:
            plt.plot(x,y,lw=thickness)   
    return x,y,m


#==============================================================================
# These are the input arguments, help, and errors. 
#==============================================================================

parser = argparse.ArgumentParser(description='Create a plot showing what each tree in your forest is doing.')
#Add the working directory so we can access the information about the trees.
parser.add_argument('working_directory', metavar='w', type=str, nargs='+',
                   help='The directory where you ran the multivariate analysis')
parser.add_argument('-second_working_directory', metavar='-s', type=str, nargs='+', default=["none"],
                   help='If you want to plot the difference between two MVA runs, set this to the directory where you ran the second multivariate analysis')
parser.add_argument('-count_type', metavar='-c', type=str, nargs='+', default=["weights"],
                   help='Default is to weight each tree by its MVA score. To simply count the number of trees use the option -c trees')
parser.add_argument('-tree', metavar='-t', type=str, nargs='+', default=["all"],
                   help='Plot just one tree using -t #treeNumber, where #treeNumber is between zero and NTrees-1.')
parser.add_argument('-keep', metavar='-k', type=str, nargs='+', default=["delete"],
                   help='To keep temporary files, set -k keep, by default all temporary files are deleted.')
args = parser.parse_args()
wd=args.working_directory[0]
swd=args.second_working_directory[0]

if wd[-1]=="/": #File path names often end with a "/", so this line adjusts the names of the files appropriately.    
    fwd2=wd.split("/")[-2]
else:
    fwd2=wd.split("/")[-1]
 #swd is used later to name the the plots and files
if swd[-1]=="/": #File path names often end with a "/", so this line adjusts the names of the files appropriately.    
    swd2=swd.split("/")[-2]
else:
    swd2=swd.split("/")[-1]
print "Working Directory: " + wd

if swd!="none":
    print "Second Working Directoy: "+ swd
    print "Plotting the difference between two MVA runs"
    # If we are making a difference plot, we need to loop over the code and make 
    # two count arrays and then subtract one from the other. Directories is the
    # range of that for loop.
    directories=[wd,swd]
else: 
    directories=[wd]




for wd in directories:
    print "Parsing "+wd
    # The default is to count each tree weighted by its MVA score. The user can also 
    # use the -c flag to change the count type to a simple sum of trees.
    count_type=args.count_type[0]
    print "Summing over: " + count_type
    
    plot_trees=args.tree[0]
    print "Plotting tree number: " + plot_trees
    
    #==============================================================================
    # Titles for plots and files
    #==============================================================================
    #What do you want to call the plot and files? (title3 is a variable later on) 
    if wd[-1]=="/": #File path names often end with a "/", so this line adjusts the names of the files appropriately.    
        wd2=wd.split("/")[-2]
    else:
        wd2=wd.split("/")[-1]
    if count_type=="weights" and plot_trees=="all":
        title="Weighted Tree Count for each Cut Type"
        title2="WeightedCount_"+wd2
    elif count_type=="trees" and plot_trees=="all":
        title="Tree Count for each Cut Type"
        title2="TreeCount_"+wd2
    elif plot_trees!="all":
        title="Tree Number " + plot_trees
        title2="Tree"+plot_trees+"_"+wd2
    
    #==============================================================================
    # Here we make temporary files containing the depth, ivar, ntype, etc. to be
    # loaded later, and also a directory to store our plots.
    #==============================================================================
    cwd=os.getcwd()
    print "Current Working Directory: " + cwd
    #os.chdir(wd)
    os.system(cwd+"/make_temp_files.sh" +" " +title2+" " +wd)
    #os.chdir(cwd)
    #If the tree_plots directory doesn't exist then we should make it
    if os.path.isdir(cwd + "/tree_plots")==False: 
        os.mkdir(cwd + "/tree_plots")
        print "Making tree_plots directory in " + cwd
    else: 
        print "tree_plots directory already exists in directory " +cwd
    
    
    
    
    #==============================================================================
    # Error messages
    #==============================================================================
    
    #Error if invalid count_type is chosen
    if count_type!="weights" and count_type!="trees":
        print "Please use the option -c trees to count the number of trees, or do not use the -c option if you want a weighted count." 
        #os.chdir(wd)
        os.system("/remove_temp_files.sh")
        #os.chdir(cwd)
        sys.exit()
        
    # We load the weights file here so we can get the number of trees. This will
    # also be used to weight our sums if we are use the default -c option.
    weights=np.loadtxt("weights_"+title2)
        
    if plot_trees!="all" and int(plot_trees) not in range(len(weights)-1):
        print "Please either use the default value for -t, or choose -t #treeNumber where #treeNumber is in the range 0 to " + str(len(weights)-1) + "."
    
    
    
    figure_index=0 # This will index the figures 
    
    #==============================================================================
    # First we need to load the relevant data (weights were already loaded)
    #==============================================================================
    
    #These are the names of each of the statistics used to make cuts
    cuts=np.loadtxt("cuts_"+title2,dtype="string") 
    cuts=cuts.tolist()
    cuts.insert(0,"Background")
    cuts.insert(len(cuts),"Signal")
    
    depth=np.loadtxt("depth_"+title2) 
    ivar=np.loadtxt("IVar_"+title2) 
    ntype=np.loadtxt("ntype_"+title2) 
    
    
     
       
    #==============================================================================
    # Now we will create a new arrays such that each tree is on a different row
    # and each entry is the corresponding depth/ivar        
    #==============================================================================
    
    #We will need to know the number of trees, this is the number of depth=0 entries
    treeNumber=sum(depth==0)
    
    #This array keeps track of the elements in "depth" where the new trees start
    newTrees=np.zeros(treeNumber) 
    j=0 #index the new trees array
    for i in range(len(depth)):
        if depth[i]==0:
            newTrees[j]=i 
            j+=1
    
    #This gives the number of nodes in each tree
    treeLength=np.zeros(len(newTrees)) 
    for i in range(len(newTrees)):
        #The last tree has to be treated differently
        if i<len(newTrees)-1:
            treeLength[i]=newTrees[i+1]-newTrees[i]
        else:
            treeLength[i]=len(ivar)-newTrees[i]
    
    #These seperate the depth/ivar/ntype data so that different trees are on different rows
    ivarTrees=np.zeros((treeNumber,max(treeLength))) 
    depthTrees=np.zeros((treeNumber,max(treeLength)))  
    ntypeTrees=np.zeros((treeNumber,max(treeLength)))        
    index=0 #This keeps track of the element in depth.ivar that we are up to
    for i in range(treeNumber):
        for j in range(int(treeLength[i])): 
            ivarTrees[i,j]=ivar[index]
            depthTrees[i,j]=depth[index]
            ntypeTrees[i,j]=ntype[index]
            index+=1
            
    # Test this has worked by comparing entries from the original data and the new arrays
    #        tree=-1 #The tree to be compared in each
    #        print fn
    #        print depth[newTrees[tree]:newTrees[tree]+treeLength[tree]]
    #        print depthTrees[tree,:]
    #        print filename
    #        print ivar[newTrees[tree]:newTrees[tree]+treeLength[tree]]
    #        print ivarTrees[tree,:]
    #        print filename2
    #        print ntype[newTrees[tree]:newTrees[tree]+treeLength[tree]]
    #        print ntypeTrees[tree,:]
    #==============================================================================
    # Before we can plot the data, we need to know how many trees in our forrest 
    # follow what paths. To do this we first create a list of coordinates for each 
    # tree and then count the number of trees that follow each possible path.
    #==============================================================================
    
    #The coordinates of each point is given by depth on the y axis and ivar 
    #on the x. We use -depth so the trees grow downwards. The array elements
    #are coords[depth/ivar,tree,node]
    coords=np.array([ivarTrees,-depthTrees]) 
    
    #This will count the number of branches going between each possible point,
    # e.g. if 10 of the trees in the forest use cut c at depth d followed by
    # cut c' and depth d+1, the the corresponding element in count will be 10.
    # The index is based on the start and end nodes of the path.
    # We use max(depth)+1 to index the depths as the root node has depth 0.
    #We use max(ivar)+3 as ivar can be zero, and we will also introduce two
    # new possible coordinates for when we have a leaf node that is signal or 
    # backgound, these will be max(ivar)+1 and -1 respectively. 
    count=np.zeros((max(ivar)+3,max(depth)+1,max(ivar)+3,max(depth)+1))
    
    
    #The general method for counting the branches will be to go through each
    #node of each tree, find what nodes it is connected to, and then add one 
    #to the corresponding element in count.
    
    # Are we plotting all the trees, or just one of them?
    if plot_trees=="all":
        first_tree=0
        last_tree=treeNumber
    else: 
        first_tree=int(plot_trees)
        last_tree=int(plot_trees)+1
    
    
    #For each tree or for the particular tree we want to plot    
    for tree in range(first_tree,last_tree):
        coordsTree=coords[:,tree,:]
        for i in range(int(treeLength[tree])-1): #For each node of the tree
            #These are the two nodes we are considering
            point1=coordsTree[:,i] 
            point2=coordsTree[:,i+1]
           #This is explained below 
            if point1[1]==0:
                    pointDepth0=point1
            #First, determine if the end end node of the branch is a
            #descision node, background leaf, or signal leaf. i.e. is
            #ntype 0,-1, or 1.
            if ntypeTrees[tree,i+1]==0: #Is point2 a descision node?
                #Descision nodes have two paths following them, and left
            # and a right path. By defualt, the trees first follow the left
            #path of each node until it reaches a leaf, then it goes up one
            #level, follows the path right, and then again goes left as far 
            #as possible. It continues like this until the whole tree is 
            #described. We thus need to remember the nodes that have other 
            #paths from them so that we can return later and plot the other
            #path. As the maximum depth is 3, we only need to remember three
            #nodes, the one at depth=0, and the last descision node we passed
            #that had depth =-1 or -2. The next time we have point2=-1 (say)
            #we know it must be the path going right from the node with depth=0.
            #Hence, when we go past an element with depth=-1 for the second 
            #time, we must set point1 to be the node with depth=0. This is 
            #why we define the following variables and if statements.
                if point2[1]==-1:
                    pointDepth1=point2
                if point2[1]==-2:
                    pointDepth2=point2
                #If the node is going left, we are done and can simply add
                #one to the approprate element in count. If it branches to
                #the right we need to change point1 as to the appropriate 
                #pointDepth variable.
                    
                #If the depth of point2 is greater than the depth of point1
                #then our node is branching to the right
                if point2[1]-point1[1]>=0: 
                #the depth of the second point determines whicj pointDepth we
                #go back to.
                    if point2[1]==-3:
                        point1=pointDepth2
                    elif point2[1]==-2:
                        point1=pointDepth1
                    elif point2[1]==-1:
                        point1=pointDepth0
            
            elif ntypeTrees[tree,i+1]==-1: #Is point2 a background leaf?
            #If point2 is a background leaf, the ivar variable is meaningless.
            #We instead set the x value of point2 to be -1.
                point2[0]=-1.
                #And as before we must determine if it is going left or right
                if point2[1]-point1[1]>=0: 
                    if point2[1]==-3:
                        point1=pointDepth2
                    elif point2[1]==-2:
                        point1=pointDepth1
                    elif point2[1]==-1:
                        point1=pointDepth0
            
            elif ntypeTrees[tree,i+1]==1: #Is point2 a signal leaf?
            #Similarly here we set the x value of point2 to be max(ivar)+1
                point2[0]=max(ivar)+1 #CHECK THIS
                #And as before we must determine if it is going left or right
                if point2[1]-point1[1]>=0: 
                    if point2[1]==-3:
                        point1=pointDepth2
                    elif point2[1]==-2:
                        point1=pointDepth1
                    elif point2[1]==-1:
                        point1=pointDepth0
            
            #Now the start and end nodes of our branch are properly defined
            #we add 1 to the corresponding element in count. To do this we
            #transform point1 and point2 s.t. their x and y values are >=0
            #and can thus be used as array indicies in count. We call these
            #transformed values index1 and index2.
            index1=np.zeros_like(point1)
            index2=np.zeros_like(point2)
            index1[1]=-point1[1] #goes from 0-3
            index2[1]=-point2[1]
            index1[0]=1+point1[0] #goes from 0-14
            index2[0]=1+point2[0]
            
            if count_type=="weights":
                # If using -c weights then add the weight of the tree to the count
                count[index1[0],index1[1],index2[0],index2[1]]+=weights[tree]
            elif count_type==str("trees"):
                #Add one to the count if using -c trees
                count[index1[0],index1[1],index2[0],index2[1]]+=1
    
    #For difference plots, we need to keep track of the two different count arrays.
    #The second condition here means the second count array is not renamed, but
    #the first is renmed count1.
    if swd!="none" and wd!=swd:
        count1=count
        title4=title2 #Store the title before we loop through again        
    np.save("count_"+ title2 +".npy",count)
    

#For difference plots, we need to subract the second count array from the first
if swd!="none":
    count=count1-count


    
    

#==============================================================================
# Now we plot them
#==============================================================================
# Before we can plot them, we need to use a lot of the same code again in order 
# to get to the corrct directories and plot the correct lines
for wd in directories:
    print "Plotting "+wd
    
    if swd!="none": #If you are making a regular tree plot, then these arrays are
                    #already correctly defined. For difference plots, they were
                    #renamed, so we need to redefine them.
        #Titles
        if wd[-1]=="/": #File path names often end with a "/", so this line adjusts the names of the files appropriately.    
            wd2=wd.split("/")[-2]
        else:
            wd2=wd.split("/")[-1]
        if count_type=="weights" and plot_trees=="all":
            title="Weighted Tree Count for each Cut Type"
            title2="WeightedCount_"+wd2
        elif count_type=="trees" and plot_trees=="all":
            title="Tree Count for each Cut Type"
            title2="TreeCount_"+wd2
        elif plot_trees!="all":
            title="Tree Number " + plot_trees
            title2="Tree"+plot_trees+"_"+wd2    
        
        #Open the files for the MVA run
        cuts=np.loadtxt("cuts_"+title2,dtype="string") 
        cuts=cuts.tolist()
        cuts.insert(0,"Background")
        cuts.insert(len(cuts),"Signal")
        depth=np.loadtxt("depth_"+title2) 
        ivar=np.loadtxt("IVar_"+title2)
        ntype=np.loadtxt("ntype_"+title2)
        
        #The number of trees
        treeNumber=sum(depth==0)
        
        #Where the new trees start
        newTrees=np.zeros(treeNumber) 
        j=0 #index the new trees array
        for i in range(len(depth)):
            if depth[i]==0:
                newTrees[j]=i 
                j+=1
        
        #The number of nodes in each tree
        treeLength=np.zeros(len(newTrees)) 
        for i in range(len(newTrees)):
            #The last tree has to be treated differently
            if i<len(newTrees)-1:
                treeLength[i]=newTrees[i+1]-newTrees[i]
            else:
                treeLength[i]=len(ivar)-newTrees[i]
        
        #These seperate the depth/ivar/ntype data so that different trees are on different rows
        ivarTrees=np.zeros((treeNumber,max(treeLength))) 
        depthTrees=np.zeros((treeNumber,max(treeLength)))  
        ntypeTrees=np.zeros((treeNumber,max(treeLength)))        
        index=0 #This keeps track of the element in depth.ivar that we are up to
        for i in range(treeNumber):
            for j in range(int(treeLength[i])): 
                ivarTrees[i,j]=ivar[index]
                depthTrees[i,j]=depth[index]
                ntypeTrees[i,j]=ntype[index]
                index+=1
        coords=np.array([ivarTrees,-depthTrees]) 
      
        # Are we plotting all the trees, or just one of them?
        if plot_trees=="all":
            first_tree=0
            last_tree=treeNumber
        else: 
            first_tree=int(plot_trees)
            last_tree=int(plot_trees)+1
    
    #Now we start actually plotting them
    
    
    #To plot them, we determine point1 and point2 as above, and then plot
    #them using the line function with thickness and colour determined by count
    
    for tree in range(first_tree,last_tree):
        coordsTree=coords[:,tree,:]
        for i in range(int(treeLength[tree])-1): #For each node of the tree
        #for i in range(2): #For testing a few nodes
            #These are the two nodes we are considering
            point1=coordsTree[:,i] 
            point2=coordsTree[:,i+1]
            if point1[1]==0:
                    pointDepth0=point1 
            if ntypeTrees[tree,i+1]==0: #Is point2 a descision node?
                if point2[1]==-1:
                    pointDepth1=point2
                if point2[1]==-2:
                    pointDepth2=point2
    
                if point2[1]-point1[1]>=0: #Is it a rightward branch?
                #the depth of the second point determines whicj pointDepth we
                #go back to.
                    if point2[1]==-3:
                        point1=pointDepth2
                    elif point2[1]==-2:
                        point1=pointDepth1
                    elif point2[1]==-1:
                        point1=pointDepth0
    
            
            elif ntypeTrees[tree,i+1]==-1: #Is point2 a background leaf?
                point2[0]=-1.
                #And as before we must determine if it is going left or right
                if point2[1]-point1[1]>=0: 
                    if point2[1]==-3:
                        point1=pointDepth2
                    elif point2[1]==-2:
                        point1=pointDepth1
                    elif point2[1]==-1:
                        point1=pointDepth0
            
            elif ntypeTrees[tree,i+1]==1: #Is point2 a signal leaf?
                point2[0]=max(ivar)+1
                #And as before we must determine if it is going left or right
                if point2[1]-point1[1]>=0: 
                    if point2[1]==-3:
                        point1=pointDepth2
                    elif point2[1]==-2:
                        point1=pointDepth1
                    elif point2[1]==-1:
                        point1=pointDepth0   
                    
            
            #Find an expression describing the branch
            l=line(point1[0],point1[1],point2[0],point2[1])
            
            if l[2]==0:
                print title,"tree number=",tree,"point1 and point 2=",point1,point2
            
            #Find the correct element of count for the thickness/colour of the line                
            index1=np.zeros_like(point1)
            index2=np.zeros_like(point2)
            index1[1]=-point1[1] #goes from 0-3
            index2[1]=-point2[1]
            index1[0]=1+point1[0] #goes from 0-max(ivar)+2
            index2[0]=1+point2[0]
            #Normalise the counts
            ncount=count[index1[0],index1[1],index2[0],index2[1]]/abs(count).max()
            #plot the line
            fig=plt.figure(figure_index)

            if swd=="none": #If we are plotting a regular tree plot 
                #Plot leaves in green rather than red/blue
                if ntypeTrees[tree,i+1]==0:
                    plt.plot(l[0],l[1],lw=ncount*5,color=[ncount,0,1-ncount])
                else:
                    plt.plot(l[0],l[1],lw=ncount*5,color=[0,ncount,0])
            else: #For difference plot
                if ncount<0: #Plot negative counts in red
                    plt.plot(l[0],l[1],lw=abs(ncount)*5,color=[abs(ncount),0,0])
                else: #And positive counts in blue
                    plt.plot(l[0],l[1],lw=ncount*5,color=[0,0,ncount])

plt.xlabel("ivar")
plt.ylabel("depth")
plt.axis([-1,max(ivar)+1,-3,0])
z=range(-1,len(cuts)-1)
plt.xticks(z, cuts)
locs, labels = plt.xticks()
plt.setp(labels, rotation=90)      
ax = fig.gca()
ax.set_xticks(np.arange(-1,len(cuts)-1,1.0))
ax.set_yticks(np.arange(-max(depth),1,1.0))
plt.grid()

#Save the plot 
if swd!="none":
    title3="DifferencePlot_"+fwd2+"_"+swd2  
    plt.title(title3)
    imagePath=str(cwd+"/tree_plots/"+title3+".png")
    plt.savefig(imagePath,bbox_inches='tight')
    
else:
    plt.title(title+" "+wd2)
    imagePath=str(cwd+"/tree_plots/"+title2+".png")
    plt.savefig(imagePath,bbox_inches='tight')
    
            
#==============================================================================
# Remove the temporary files made at the beginning        
#==============================================================================

if args.keep[0]=="delete":
    #os.chdir(wd)
    os.system(cwd+"/remove_temp_files.sh"+" "+title2)
    if swd!="none":
        os.system(cwd+"/remove_temp_files.sh"+" "+title4)
    #os.chdir(cwd)
    print "Temporary files deleted"
else:
    print "Temporary files saved"




#==============================================================================
# Display plot in webpage
#==============================================================================
html_file_name=imagePath.split(".")[-2].split("/")[-1] #This is the name of the html file
path=os.path.expanduser('~')+"/public_html/tree_plots/" #The path where the html is saved
if not os.path.exists(path): #Check if the html directory already exists, and make it if you need to
    print "Making directory: " + path
    os.makedirs(path)

path+=html_file_name+"/"
if not os.path.exists(path): #Check if the html directory already exists, and make it if you need to
    print "Making directory: " + path
    os.makedirs(path)

webPageImage=path+html_file_name+".png" #Save the image under public_html
imageAddress="https://ldas-jobs.ligo.caltech.edu/"+webPageImage.replace("/home/","~").replace("/public_html/","/")

html_string="""
<!DOCTYPE html>
<html>
<head>
<title>TreePlotter Output</title> <!--This is the tab title of the webpage-->
<meta charset="UTF-8">
<style>
body {background-color: white;}
h1   {color: black;}
p    {font-family:arial;}
</style>
</head>

<h1>TreePlotter Output</h1>
"""

html_string+="""
<a title="""+ webPageImage +""">
<img src="""+imageAddress+""" alt="treePlotter" style="border:0">
</a>
"""

html_string+="""
</body>
</html>
"""
Html_file= open(path+html_file_name+".html","w") #Save the html file
Html_file.write(html_string)
Html_file.close

plt.savefig(webPageImage,bbox_inches='tight')

print "The HTML file is saved under" + path + html_file_name



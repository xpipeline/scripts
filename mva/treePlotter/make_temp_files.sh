
#!/bin/bash

# This program creates the temporary files needed by the tree_plotter.py script.

grep depth $2/xtmva_BDT/output/weights/xtmvaClassification_BDT.weights.xml |awk {'print $5}' | awk -F\" '{print $2}'  > IVar_$1

grep depth $2/xtmva_BDT/output/weights/xtmvaClassification_BDT.weights.xml |awk {'print $3}' | awk -F\" '{print $2}'  > depth_$1

grep depth $2/xtmva_BDT/output/weights/xtmvaClassification_BDT.weights.xml |awk {'print $11}' | awk -F\" '{print $2}' > ntype_$1

grep VarIndex $2/xtmva_BDT/output/weights/xtmvaClassification_BDT.weights.xml | awk {'print $4}' | awk -F\" {'print $2'} > cuts_$1

grep boostWeight $2/xtmva_BDT/output/weights/xtmvaClassification_BDT.weights.xml | awk {'print $3}' | awk -F\" '{print $2}' > weights_$1




#!/bin/bash
# Automated installer for the spherical radiometer version of xpipeline.

echo "===================================================================== "
echo "   Automatic installation - Spherical Radiometer in Xpipeline"
echo "===================================================================== "

MEXOPTSHPATH=/home/$USER/.matlab/R2007a
MEXOPTSH=/home/$USER/.matlab/R2007a/mexopts.sh

if [ ! -f $MEXOPTSH ]; then
    echo " File $MEXOPTSH not found"
    mkdir -p $MEXOPTSHPATH
    echo " Copying working copy from the svn..."
    cp dependencies/mexopts/mexopts.sh $MEXOPTSHPATH/.
    echo "done"
else
    echo " File $MEXOPTSH found"
    MEXREPO=`md5sum dependencies/mexopts/mexopts.sh | awk '{print $1}'`
    MEXUSER=`md5sum $MEXOPTSH | awk '{print $1}'`
    echo "$MEXREPO"
    echo "$MEXUSER"
    if [ "$MEXUSER" == "$MEXREPO" ];
       then
          echo "Your current mexopts.sh file should work for compilation"
       else
          echo " Backing up current and copying working copy from the svn..."
          timestamp=$(date +%s)
          mv $MEXOPTSH $MEXOPTSHPATH/mexopts.$timestamp.backup
          cp dependencies/mexopts/mexopts.sh $MEXOPTSHPATH/.
          echo "done"
    fi
fi

# Clean out files that may have been left by a previous installation.
rm -f envsetup.sh
rm -f install.log

# Make a backup copy of Makefile.in, since this is edited with user-specific 
# info by buildmex.sh. Once the build is finished we'll replace the edited 
# copy with the backup copy so the user won't accidentally commit the edited 
# copy and break the build for other users.
cp Makefile.in dependencies/Makefile.in

# Compile sphrad portion of the pipeline.
cd dependencies/
./buildmex.sh
cd ..

# Compile xpipeline. Note that install-allsky, install-mva, and install-sn all 
# include install-grb.
make clean
./configure
#make install-allsky
#make install-grb
#make install-mva
# Select type of search to install.
INSTALL_DEFAULT=install-grb
echo Installation type:
read -e -p "[$INSTALL_DEFAULT]: " INSTALL
INSTALL=${INSTALL:-$INSTALL_DEFAULT}
make $INSTALL

# Clean up: retrieve original version of Makefile.in.
mv dependencies/Makefile.in Makefile.in
#rm Makefile
echo "===================================================================== "
echo "The spherical radiometer version of Xpipeline was installed "
echo "successfuly, you will find it ready to use at the location"
echo "Provided by you during the installation. Don't forget to add"
echo "it to your PATH !!!"
cat install.log
echo "===================================================================== "

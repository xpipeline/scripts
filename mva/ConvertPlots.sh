#!/bin/sh
#######################################################
# need ImageMagick, to convert eps in gif
# could be replaced by different convert function
# if convert is not available (eps2png?)
# L. Cadonati - February 2014
#######################################################
convertfigure() {
    name=$(basename $1 .eps)
    convert -density 150 $1 $name.png
    echo ``$1 converted from eps to png''
}

folder=plots
test -d plots/eps || mkdir plots/eps
for file in $(ls $folder/*.eps); do
    #echo $file
    convertfigure $file;
    name=$(basename $file .eps)
    mv $name.png $folder
    mv $file $folder/eps
done

  

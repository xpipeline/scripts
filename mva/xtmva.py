#!/usr/bin/env python
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# xtmva.py : replaces xtmva.sh
# reads xtmva.ini configuration file
#
# WARNING: This script should work run from any directory, except that 
# the BACK2MAT.sh script run with the root2mat option may only work 
# when you are running in the tmvadir directory.
#
# first version: L. Cadonati March 2014


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# initialize
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import re, os, sys
import datetime,time
from optparse import OptionParser
import ConfigParser
import getpass


os.system("export TMVASYS=$ROOTSYS/tmva") # this needed for ROOT v34.14
tstart=time.time()


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# START HERE: PARSE INPUT ARGUMENTS
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
parser = OptionParser()
# run options
parser.add_option("-q", "--quiet", action="store_false", dest="verbose",
                  default=True,
                  help="don't print status messages to stdout")
parser.add_option("-b", "--batch", action="store_true", dest="isbatch",
                  default=False,
                  help="run in batch mode")
# name options
parser.add_option("--grbname",type="string",action="store",dest="grbname",
                  default="GRB060223", help="The GRB to be analyzed")
parser.add_option("--fdir",type="string",action="store",dest="fdir",
                  default=os.environ['HOME']+"/GRB",
                  help="Working directory [default: %default]")
parser.add_option("--cname",type="string",action="store",dest="cname",
                  default="xtmva.ini",
                  help="xtmva configuration file [default: %default]")

parser.add_option("-n","--nifo",type="int",action="store",dest="nifo",
                  default=3, help="number of detectors [default: %default]")
parser.add_option("-t","--threshold",type="int",action="store",dest="threshold",
                  default=20, help="Minimum significance for injection training set [default: %default]")
(options, args) = parser.parse_args()

# --------------------------------------------------------------------------
# flags to switch on/off different parts of the script (for testing purposes)
newrun=1          # will delete pre-existing ${XTMVAOUT} folder
mat2root=0        # toggle on/off for matlab 2 root conversion (no ascii)
                  # do this OR do mat2ascii, ascii2root
DoClassifier=1    # all the root MVA processing
# --------------------------------------------------------------------------
mat2ascii=1       # toggle on/off for matlab 2 ascii conversion
ascii2root=1      # toggle on/off for ascii 2 root conversion
                  # do these OR do mat2root
# --------------------------------------------------------------------------
root2mat=1        # toggle on/off for running BACK2MAT.sh, which runs the 
                  # root 2 ascii and ascii 2 mat conversions
# --------------------------------------------------------------------------

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
fdir=options.fdir+"/"+options.grbname
cname=fdir+"/"+options.cname

# ---- Record the current working directory in a string.
cwdstr = "."
print >> sys.stdout, "####################################################"
print >> sys.stdout, "#              XTMVA                               #"
print >> sys.stdout, "####################################################"
divider='----------------------------------------------------------------------'


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Read configuration file.
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
print >> sys.stdout, "Parsing parameters (ini) file:", cname

if not os.path.isfile(cname):
    print >> sys.stderr,"Error: non existent parameter file: ", cname
    sys.exit(1)

cp = ConfigParser.ConfigParser()
cp.read(cname)

# ---- Create configuration-file-parser object and read parameters file.

if cp.has_option('tags','version'):
    ini_version = cp.get('tags','version')
if cp.has_option('tags','user-tag'):
    usertag = cp.get('tags','user-tag')
    print >> sys.stdout, "Parameter file tag:", ini_version,usertag

tmvadir=cp.get('Folders','tmvadir')
xout=fdir+'/'+cp.get('Folders','xout')
folder_in=xout+"/input"
folder_out=xout+"/output"

nifo=options.nifo
classifier=cp.get('Settings','classifier')
signal=cp.get('Settings','WFtrain')
if cp.has_section('WFtest'):
    wsets = cp.options('WFtest')
    injectioncount  = []
    for set in wsets:
        injectioncount.append(cp.get('WFtest',set))
else:
    wsets=[]
print wsets
print injectioncount

# ---- Optionally write likelihoods to a file for on-the-fly setting of 
#      likelihoods in the MVA analysis. The file will be placed in the 
#      tmvadir directory and will be deleted at the end of the script.
if cp.has_option('parameters','likelihoods'):
    likelihoodList = cp.get('parameters','likelihoods')
    print >> sys.stdout, "Reading likelihoods on the fly:", likelihoodList
    likelihoods = likelihoodList.split(',') 
    os.system("rm -f "+tmvadir+"/likelihoods.dat")
    lfile = open(tmvadir+"/likelihoods.dat",'w')
    for likelihood in likelihoods:
        lfile.write(likelihood + '\n')
    lfile.close() 
else:
    print "Error: ini file must specify the list of likelihoods to be processed."
    sys.exit(2)
    
if classifier not in ['BDT','BDTG','BDTD','BDTB','MLP','Likelihod','Fisher','LD']:
    print "Error in classifier:",classifier," - Invalid Input"
    print "Supported: BDT, BDTG, BDTD, BDTB, MLP, Likelihod, Fisher, LD"
    sys.exit(3)

# ---- Name of file containing list of files to be processed.
datfile=options.grbname+'_files.dat'


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Set up folders for a new run
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
if newrun:
    print divider
    print "Creating",xout,"directory (delete pre-existing)"
    cmdstr="test -d "+xout+" && rm -rf "+xout ; os.system(cmdstr)
    cmdstr="mkdir -pv "+folder_in ; os.system(cmdstr)
    cmdstr="mkdir -pv "+folder_out ; os.system(cmdstr)


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# matlab to ROOT conversion
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
if mat2root:
    tick=time.time()
    print divider
    cmdstr="mat2root.py -q --grbname "+options.grbname
    cmdstr=cmdstr+" --nifo "+str(options.nifo)
    cmdstr=cmdstr+" --cname "+str(options.cname)
    cmdstr=cmdstr+" --fdir "+str(options.fdir)
    print "***",cmdstr ; os.system(cmdstr)
    cmdstr="mv TMVA_GRB_Input.root "+folder_in ; os.system(cmdstr)
    print cmdstr


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# matlab to ascii conversion
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
if mat2ascii:
    tick=time.time()
    print divider
    print "*** Writing basic ASCII files: background and signal training and testing, and on-source:"
    threshold = options.threshold
    cmdstr="xwriteallascii "+fdir+" "+signal+" "+str(threshold)
    print "***",cmdstr ; os.system(cmdstr)
    cmdstr="mv "+fdir+"/output/*_test.txt "+folder_in ; os.system(cmdstr)
    cmdstr="mv "+fdir+"/output/*_train.txt "+folder_in ; os.system(cmdstr)

    for wf in wsets:
        cmdstr="xwriteinjasciifiles "+fdir+"/output "+wf+" 1:"+cp.get('WFtest',wf)+" "
        cmdstr=cmdstr+folder_in+"/"+wf+"_test.txt "+str(threshold)+" 0"
        print "*** Writing test injection file WITHOUT cleaning:"
        print "***",cmdstr ; os.system(cmdstr)

    if not os.path.isfile(folder_in+"/BG_train.txt"):
        print "ERROR:",folder_in+"/BG_train.txt was not produced"
    if not os.path.isfile(folder_in+"/BG_test.txt"):
        print "ERROR:",folder_in+"/BG_test.txt was not produced"
    if not os.path.isfile(folder_in+"/SIG_train.txt"):
        print "ERROR:",folder_in+"/SIG_train.txt was not produced"
    if not os.path.isfile(folder_in+"/SIG_test.txt"):
        print "ERROR:",folder_in+"/SIG_test.txt was not produced"
    if not os.path.isfile(folder_in+"/ON_test.txt"):
        print "ERROR:",folder_in+"/ON_test.txt was not produced"
    for wf in wsets:
        if not os.path.isfile(folder_in+"/"+wf+"_test.txt"):
            print "ERROR:",folder_in+"/"+wf+"_test.txt was not produced"
    print divider
    T=time.time()-tick
    print "mat2ascii: ", T/60, "minutes"


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# ascii to root conversion
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
if ascii2root:
    tick=time.time()
    print divider

    # ---- Use awk to change last column in files from a string (matlab file name) to
    #      two integers:
    #           ON, BG files: 0 0
    #        injection files: M N
    #      where M is the STEST number for this injection set in <grbname>_files.dat
    #      and N is the injection scale.
    #      We do this because TMVA will not carrying chars as spectator variables.
    #      Name new files A_<original_name> to distinguish from original files.
    # ---- ON/BG files.
    #      Note that we must also add an entry to the file header for the injection scale.
    awkstr0=' \'NR==1{print $0"injscale/I:"} NR>1 {for(i=1;i<NF;++i){printf "%s ", $i }; print a, "0"}\' '
    for fname in ['BG_train', 'BG_test', 'ON_test']:
        f1=folder_in+"/"+fname+".txt"
        f2=folder_in+"/A_"+fname+".txt"
        cmdstr='awk -v a=0'+awkstr0+f1+" > "+f2
        print cmdstr ; os.system(cmdstr)
    # ---- SIG files.
    for fname in ['SIG_train', 'SIG_test']:
        # ---- Use sed to strip out string content around injection scale in file name.
        #      Note that we must also add an entry to the file header for the injection scale.
        sedstr1 = "s/simulation_"+signal+"_//g" 
        sedstr2 = "s/_0_0_merged.mat//g"
        awkstr1=' \'NR==1{print $0"injscale/I:"} NR>1 {for(i=1;i<NF;++i){printf "%s ", $i }; print a, $NF}\' '
        f1=folder_in+"/"+fname+".txt"
        f2=folder_in+"/A_"+fname+".txt"
        cmdstr='awk -v a=1'+awkstr1+f1+" | sed -e "+sedstr1+" -e "+sedstr2+" > "+f2
        print cmdstr ; os.system(cmdstr)
    # ---- -> 2+ for <wf>_test.txt files.
    counter=1
    for wf in wsets:
        counter=counter+1
        # ---- Use sed to strip out string content around injection scale in file name.
        #      Note that we must also add an entry to the file header for the injection scale.
        sedstr1 = "s/simulation_"+wf+"_//g" 
        sedstr2 = "s/_0_0_merged.mat//g"
        awkstr1=' \'NR==1{print $0"injscale/I:"} NR>1 {for(i=1;i<NF;++i){printf "%s ", $i }; print a, $NF}\' '
        f1=folder_in+"/"+wf+"_test.txt"
        f2=folder_in+"/A_"+wf+"_test.txt"
        cmdstr='awk -v a='+str(counter)+awkstr1+f1+" | sed -e "+sedstr1+" -e "+sedstr2+" > "+f2
        print cmdstr ; os.system(cmdstr)

    # ---- Write the "dat" file, that lists to various ASCII files that have been made.
    df=open(datfile,'w+')
    df.write(folder_in+"/A_BG_train.txt      BTRAIN\n \n")
    df.write(folder_in+"/A_BG_test.txt       BTEST\n \n")
    df.write(folder_in+"/A_SIG_train.txt     STRAIN\n \n")
    df.write(folder_in+"/A_SIG_test.txt      STEST\n \n")
    for i in range(len(wsets)):
        df.write(folder_in+"/A_"+wsets[i]+"_test.txt   STEST"+str(i+2)+"\n \n")
    df.write(folder_in+"/A_ON_test.txt       ONSOURCE\n \n")
    df.close()

    # Move the dat file to the TMVA folder, where RootLoader is run from.
    # (This will generate a harmless error message if we are already running in tmvadir.)
    cmdstr="mv "+datfile+" "+tmvadir ; os.system(cmdstr)
    # Note: cd commands do not seem to work when running script under condor. 
    # Replace all cd commands by use of absolute paths.
    # cmdstr="cd "+tmvadir ; os.system(cmdstr)

    # RootLoader.C converts the ascii files listed in the dat file
    # into a .root file which is then passed to xtmvaClassification.py
    print divider
    cmdstr="root -l -q -b "+tmvadir+"/RootLoader.C\("+str(nifo)+',\\"'+tmvadir+"/"+datfile+'\\",\\"'+tmvadir+'\\",\\"likelihoods.dat\\"\)'+" >& rootloader.log" 
    print "***",cmdstr ; os.system(cmdstr)
    # Concatentate rootloader.log to standard output.
    with open('rootloader.log', 'r') as fin:
        print fin.read()
    cmdstr="rm  rootloader.log" ; os.system(cmdstr)
    # Move root file to final destination.
    cmdstr="mv -f "+tmvadir+"/TMVA_GRB_Input.root "+folder_in
    print cmdstr; os.system(cmdstr)

    print divider
    T=time.time()-tick
    print "ascii2root: ", T/60, "minutes"


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Now run the classifier
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
if DoClassifier:
    tick=time.time()
    print divider

    if not os.path.isfile(folder_in+"/TMVA_GRB_Input.root"):
        print "ERROR:",folder_in+"/TMVA_GRB_Input.root"
    cmdstr="python "+tmvadir+"/xtmvaClassification.py --methods "+classifier+" --nifo "+str(nifo)+" --inputfile "+folder_in+"/TMVA_GRB_Input.root "+"--likelihoodfile "+tmvadir+"/likelihoods.dat "+"--outputfile "+tmvadir+"/TMVA_GRB_Output.root"
    print "***",cmdstr ; os.system(cmdstr)

    print divider
    T=time.time()-tick
    print "xtmvaClassification: ", T/60, "minutes"

    if not os.path.isfile(tmvadir+"/TMVA_GRB_Output.root"):
        print "ERROR:",tmvadir+"/TMVA_GRB_Output.root"
    cmdstr="mv -f "+tmvadir+"/TMVA_GRB_Output.root "+folder_out
    print cmdstr; os.system(cmdstr)
    cmdstr="rm -rf "+folder_out+"/weights "
    print cmdstr; os.system(cmdstr)
    #cmdstr="mv "+tmvadir+"/weights "+folder_out
    cmdstr="mv weights "+folder_out
    print cmdstr; os.system(cmdstr)


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Convert back to matlab and reset output directory to point to new files.
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
if root2mat:
    tick=time.time()

    # ---- Run the BACK2MAT.sh shell script.
    print divider
    cmdstr=tmvadir+"/BACK2MAT.sh "+fdir+" "+tmvadir+" "+folder_out+" "+classifier+" "+str(nifo)+" "+folder_in+" "+tmvadir+"/"+datfile
    print "***",cmdstr ; os.system(cmdstr)

    # ---- Assuming all went well, move the new MVA files in place of the original ones.
    # ---- First check to see if original output directory has already been moved 
    #      in a previous run.
    print divider
    if os.path.isdir(fdir+"/output"):
        # ---- Verify output/ is a real directory and not a symbolic link, then rename it.
        #      If it is a symlink and output_xpipeline already exists from a previous run,
        #      then just delete the old symlink.
        if os.path.islink(fdir+"/output") and os.path.isdir(fdir+"/output_xpipeline"):
            print "Warning: output_xpipeline/ already exists. Deleting existing symlinks."
            cmdstr="rm "+fdir+"/output "
            print "***",cmdstr ; os.system(cmdstr)
        elif not os.path.islink(fdir+"/output") and not os.path.isdir(fdir+"/output_xpipeline"):
            cmdstr="mv "+fdir+"/output "+fdir+"/output_xpipeline"
            print "Renaming original output/ directory output_xpipeline/ :"
            print "***",cmdstr ; os.system(cmdstr)
        else:
            print "Error: something is wrong with the directories"+fdir+"/output and "+fdir+"/output_xpipeline. Exiting."
            sys.exit(366);
    else:
        print "Error: directory "+fdir+"/output does not exist. Exiting."
        sys.exit(366);
    print "Making symbolic link of MVA output directory to output/ :"
    cmdstr="ln -s "+xout+"/output "+fdir+"/output"
    print "***",cmdstr ; os.system(cmdstr)

    # ---- Done.
    print divider
    T=time.time()-tick
    print "root2mat: ", T/60, "minutes"


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Final cleanup.
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
cmdstr="cp "+tmvadir+"/likelihoods.dat "+folder_in
cmdstr="cp "+tmvadir+"/"+datfile+" "+folder_in
print divider
os.system(cmdstr)
 

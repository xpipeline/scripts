#!/bin/bash
# Automated installer for the spherical radiometer version of xpipeline.

echo "===================================================================== "
echo "   Automatic installation - Spherical Radiometer in Xpipeline"
echo "===================================================================== "

#set -e
MEXOPTSHPATH=/home/$USER/.matlab/R2007a
MEXOPTSH=/home/$USER/.matlab/R2007a/mexopts.sh

if [ ! -f $MEXOPTSH ]; then
    echo " File $MEXOPTSH not found"
    mkdir -p $MEXOPTSHPATH
    echo " Copying working copy from the svn..."
    cp dependencies/mexopts/mexopts.sh $MEXOPTSHPATH/.
    echo "done"
else
    echo " File $MEXOPTSH found"
    MEXREPO=`md5sum dependencies/mexopts/mexopts.sh | awk '{print $1}'`
    MEXUSER=`md5sum $MEXOPTSH | awk '{print $1}'`
    echo "$MEXREPO"
    echo "$MEXUSER"
    if [ "$MEXUSER" == "$MEXREPO" ];
       then
          echo "Your current mexopts.sh file should work for compilation"
       else
          echo " Backing up current and copying working copy from the svn..."
          timestamp=$(date +%s)
          mv $MEXOPTSH $MEXOPTSHPATH/mexopts.$timestamp.backup
          cp dependencies/mexopts/mexopts.sh $MEXOPTSHPATH/.
          echo "done"
    fi
fi

rm envsetup.sh
rm install.log
rm dependencies/Makefile.in
cp Makefile.in dependencies/Makefile.in
cd dependencies/
./buildmex.sh
cd ..
make clean
./configure
make install-allsky
rm Makefile.in
rm Makefile
cp dependencies/Makefile.in Makefile.in
echo "===================================================================== "
echo "The spherical radiometer version of Xpipeline was installed "
echo "successfuly, you will find it ready to use at the location"
echo "Provided by you during the installation. Don't forget to add"
echo "it to your PATH !!!"
cat install.log
echo "===================================================================== "

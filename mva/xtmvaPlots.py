#!/usr/bin/env python
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# xtmvaPlots.py: makes plots from the XTMVA output root file
# Needs XTMVAplots.C (to call TMVA standard routines)
# Needs pyrootStyle.py (Root Style settings)
# Needs ConvertPlots.sh and convert (from ImageMagick) to convert eps to png
#
# L. Cadonati February 2014
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# initialize
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import re
import datetime
import os

import sys
tmpargv = sys.argv[:]   # [:] for a copy, not reference
sys.argv = []
from ROOT import *
sys.argv = tmpargv      # to prevent pyROOT from taking over the command line

from optparse import OptionParser
from array import array
from math import *
#import pylab as plt
import numpy as np


# ROOT style options
import pyrootStyle
pyrootStyle
TGaxis.SetMaxDigits(3)
#include "tmvaglob.C"

# Suppress annoying warnings
import warnings
warnings.filterwarnings( action='ignore', category=RuntimeWarning,
                        message='Replacing existing*' )
warnings.filterwarnings( action='ignore', category=RuntimeWarning,
                        message='Deleting canvas*' )
gROOT.ProcessLine("gErrorIgnoreLevel = 1001;")

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function to normalize a histogram
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def NormalizeHist(h):
    if h==0: return
    if h.GetSumw2N()==0: h.Sumw2()
    if h.GetSumOfWeights()!=0:
        dx = (h.GetXaxis().GetXmax()-h.GetXaxis().GetXmin())/h.GetNbinsX()
        h.Scale(1.0/h.GetSumOfWeights()/dx)

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function to make 2D scatter plots of triggers; color is significance
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def Plot2D( V1, V2, V3, cut, logaxis ):
    global tt, g, ff1
    s=V1+":"+V2+":"+V3+">> h"
    if options.verbose: print s,cut,gtitle
    
    tt.Draw(s,cut,"goff")
    ## See "how to obtain more info from TTree::Draw" at:
    ## http://root.cern.ch/root/htmldoc///TTree.html#TTree:Draw
    g=TGraph2D(tt.GetSelectedRows(),tt.GetV1(),tt.GetV2(),tt.GetV3())

    g.SetMarkerStyle(20)
    g.SetMarkerSize(0.3)
    if Bthr>thrmin:
        g.SetMarkerSize(0.5)
    g.SetNpx(100)
    g.SetNpy(100)
    g.GetXaxis().SetTitle(V1)
    g.GetYaxis().SetTitle(V2)
    g.GetXaxis().SetTitleOffset(1.4)
    g.GetYaxis().SetTitleOffset(1.8)
    g.SetTitle(gtitle)
    g.Draw("Pcolz")
    gPad.Update()

    x1=min(g.GetXaxis().GetXmin(),g.GetYaxis().GetXmin())+1e-4
    x2=max(g.GetXaxis().GetXmax(),g.GetYaxis().GetXmax())
    l1=floor(log(x1, 10))
    l2=ceil(log(x2, 10))
    print x1, x2, log(x1,10),log(x2,10), l1, l2
    x1=10 ** l1
    x2=10 ** l2

    if logaxis==1:
        g.GetXaxis().Set(10,x1,x2)
        g.GetYaxis().Set(10,x1,x2)
    else:
        g.GetXaxis().Set(10,smin,smax)
        g.GetYaxis().Set(10,s2min,s2max)
    #fix color scale to match in signal and background plots
    g.GetZaxis().SetRangeUser(smin,smax)

    gPad.SetGrid(1,1)
    gPad.SetLeftMargin(0.15)
    gPad.SetRightMargin(0.15)
    gPad.SetLogx(logaxis)
    gPad.SetLogy(logaxis)
    gPad.Update()
    # set TopView for a colored scatter plot look
    gPad.GetView().TopView()
    gPad.Modified()
    gPad.Update()

    # Draw diagonal line as a function
    # (having issue with TLine, but this also sometimes gives trouble)
    if logaxis==1:
        ff1=TF1("ff1","x",0,x2)
        ff1.SetLineStyle(3)
        ff1.Draw("same")
        gPad.Update()

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Call Plot2D for signal/background events and save
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def MakePlot2D(logaxis,title):
    global gtitle
    
    # Plot signal
    c1=TCanvas("c1","c1",100,100,500,400)
    gtitle="Off Source - Signal"
    Plot2D(v1,v2,sig,cutS+cutC,logaxis)
    gPad.Print("plots/"+title+"-signal.eps")
    if options.gif: gPad.Print("plots/"+v1+"-"+v2+"-signal.gif")
    
    # Plot background
    c2=TCanvas("c2","c2",600,100,500,400)
    gtitle="Off Source - Background"
    Plot2D(v1,v2,sig,cutB+cutC,logaxis)
    gPad.Print("plots/"+title+"-bckg.eps")
    if options.gif: gPad.Print("plots/"+v1+"-"+v2+"-bckg.gif")

    # Write html
    hname=[title+"-bckg",title+"-signal"]
    htmlfile.write('<h2>'+title+'</h2>')
    for hh in hname:
       htmlfile.write('<a href='+hh+'.png><img src='+hh+'.png width=450></a>\n')
    htmlfile.write('<p>\n')

    if options.isbatch==0:raw_input("Press Enter To Continue: ")

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Customized palette (looks better than the default)
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def set_palette(name, ncontours=999):
    """Set a color palette from a given RGB list
        stops, red, green and blue should all be lists of the same length"""
    
    if name == "gray" or name == "grayscale":
        stops = [0.00, 0.34, 0.61, 0.84, 1.00]
        red   = [1.00, 0.84, 0.61, 0.34, 0.00]
        green = [1.00, 0.84, 0.61, 0.34, 0.00]
        blue  = [1.00, 0.84, 0.61, 0.34, 0.00]
    else:
        # default palette
        stops = [0.00, 0.34, 0.61, 0.84, 1.00]
        red   = [0.00, 0.00, 0.87, 1.00, 0.51]
        green = [0.00, 0.81, 1.00, 0.20, 0.00]
        blue  = [0.51, 1.00, 0.12, 0.00, 0.00]
    
    s = array('d', stops)
    r = array('d', red)
    g = array('d', green)
    b = array('d', blue)
    
    npoints = len(s)
    TColor.CreateGradientColorTable(npoints, s, r, g, b, ncontours)
    gStyle.SetNumberContours(ncontours)

set_palette("mypalette")

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# START HERE: PARSE INPUT ARGUMENTS
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
parser = OptionParser()
# run options
parser.add_option("-q", "--quiet", action="store_false", dest="verbose",
                  default=True,
                  help="don't print status messages to stdout")
parser.add_option("-b", "--batch", action="store_true", dest="isbatch",
                  default=False,
                  help="run in batch mode")
# name options
parser.add_option("--fdir",type="string",action="store",dest="fdir",
                  default="GRB060223",
                  help="Working directory where root file is stored")
parser.add_option("--fname",type="string",action="store",dest="fname",
                  default="TMVA_GRB_Output.root",
                  help="TMVA output filename [default: %default]")
# selective run/save options
parser.add_option("--gif", action="store_true",dest="gif",
                  default=False,
                  help="save gif (slow for large datasets) ")
parser.add_option("--png", action="store_true",dest="png",
                  default=False,
                  help="save png (needs convert from ImageMagick) ")
parser.add_option("--tmvaplots", action="store_true",dest="tmvaplots",
                  default=False,
                  help="produce tmva GUI plots [default: %default]")
# significance option
parser.add_option("--sig",type="string",action="store",dest="sig",
                  default="BDT",
                  help="What significance to use: BDT or X [default: %default]")
parser.add_option("--BDTth",type="float",action="store",dest="sigthr",
                  default=-1,help="threshold on BDT [default: %default]")
parser.add_option("--wf",type="float",action="store",dest="wf",
                  default=1,help="waveform set ID [default: %default]")
parser.add_option("-n","--nifo",type="int",action="store",dest="nifo",
                  default=3, help="number of detectors [default: %default]")
parser.add_option("--FAP",type="float",action="store",dest="FAP",
                  default=0.1, help="False Alarm Probability [default: 0.1]")


(options, args) = parser.parse_args()
if options.isbatch: ROOT.gROOT.SetBatch(True)

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
fname=options.fdir+"/"+options.fname
if options.verbose: print fname

# The standard TMVA plots (separate root call)
useTMVAStyle=1
os.system('test -d plots && rm -rf plots')
os.system('mkdir plots')
cmdstring='root -b -l -q XTMVAplots.C\(\\"'+fname+'\\",'+str(useTMVAStyle)+'\)'
if options.verbose: print cmdstring
if options.tmvaplots: os.system(cmdstring)

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Prepare an html file to look at results - lives in same folder as plots
htmlfile= open("AAA.html","w")
htmlfile.write('<html>\n')
htmlfile.write('<h1>'+options.fdir+'<h1>\n')
htmlfile.write('<h2> TMVA standard plots <h2>\n')
hname=['mvaeffs_BDT','overtrain_BDT','rejBvsS']
for hh in hname:
    htmlfile.write('<a href='+hh+'.png><img src='+hh+'.png width=300></a>\n')
htmlfile.write('<p>\n')
hname=['CorrelationMatrixB','CorrelationMatrixS']
for hh in hname:
    htmlfile.write('<a href='+hh+'.png><img src='+hh+'.png width=300></a>\n')
htmlfile.write('<p>\n')

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Open the root file, retrieve train/test trees
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
f=TFile(fname)
tr=f.Get("TrainTree")
tt=f.Get("TestTree")
sSig=f.Get("significance_BDT")

# next lines are for memory allocation (needed for large trees)
nev=tr.GetEntries(); print nev,"total training events (S+B)"
tr.SetEstimate(nev)
nev=tt.GetEntries(); print nev,"total testing events (S+B)"
tt.SetEstimate(nev)

# Define which significance is to be used as color scale in the plots
sig1="(BDT+1)*50"
sig2="log_significance_"
if options.sig == "BDT":
   sig=sig1
elif options.sig == "X":
   sig=sig2
else:
   print options.sig,"Significance option not recognized; using default BDT"

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# apply a BDT threshold to all plots
# define cuts for signal and noise
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Bthr=options.sigthr
bt=str(int((Bthr+1)*50))
cut0=TCut("BDT>"+str(Bthr))
cutB=TCut("classID==1 && BDT>"+str(Bthr))
cutS=TCut("classID==0 && BDT>"+str(Bthr)+" && matfile=="+str(int(options.wf)))
cutStrain=TCut("classID==0 && BDT>"+str(Bthr))

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Identify the range of significances
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
v1=sig   # set by BDT or X in calling command
v2=sig2  # _log_significance_

tt.Draw(v1+":"+v2,cut0,"goff")
var1 = tt.GetV1()
smin=min(var1[n] for n in range(tt.GetSelectedRows()))
smax=max(var1[n] for n in range(tt.GetSelectedRows()))
var2 = tt.GetV2()
s2min=min(var2[n] for n in range(tt.GetSelectedRows()))
s2max=max(var2[n] for n in range(tt.GetSelectedRows()))
if options.verbose:
    print "****************************************"
    print options.sig+" significance limits = %.1f - %.1f" % (smin,smax)
    print v2+" significance limits = %.1f - %.1f" % (s2min,s2max)
    print "****************************************"

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Analysis of background triggers in X-BDT significance plane
# use contours of the background distribution  to set a line cut
# Remaining events are fit to a line that can be used to identify candidates
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
cbg=TCanvas("cbg","Background Events",10,10,500,400)

tt.Draw(v2+":"+v1+">>hbg",cutB)
gB=TGraph(tt.GetSelectedRows(),tt.GetV2(),tt.GetV1())

# Contours in the background distribution
hbg.SetContour(3,array('d',[3,100,1000]))
hbg.Draw("contsame,list")
gPad.Update()

conts=gROOT.GetListOfSpecials().FindObject("contours")
# retrieve the largest of the lowest-order contours
Ncont=conts[0].GetSize()
dy=0
nc=0
for i in range(Ncont):
    cg=conts[0].At(i)
    y1=min(cg.GetY()[n] for n in range(cg.GetN()))
    y2=max(cg.GetY()[n] for n in range(cg.GetN()))
    if y2-y1> dy:
        nc=i
        dy=y2-y1
cg=conts[0].At(nc)
# make it into a standard python list for ease of manipulation
xlist=[]; ylist=[]
for n in range(cg.GetN()):
    xlist.append(cg.GetX()[n])
    ylist.append(cg.GetY()[n])
x2=max(xlist)
y1=max(ylist)
x1=xlist[ylist.index(y1)]
y2=ylist[xlist.index(x2)]
if options.verbose:
    print "Contour cut below (x1,x2,y1,y2): %.1f %.1f %.1f %.1f" % (x1,x2,y1,y2)
slope=(y2-y1)/(x2-x1)
fContourCut=TF1("fContourCut","[0]+[1]*(x-[2])",0,100)
fContourCut.SetParameter(0,y1)
fContourCut.SetParameter(1,slope)
fContourCut.SetParameter(2,x1)
fContourCut.SetLineStyle(2)
fContourCut.SetLineWidth(2)
fContourCut.SetLineColor(3)
fContourCut.Draw("same")
gPad.Update()

# now only keep background triggers above the dashed line fContourCut
# and fit the residual events to a line which will be used for event selection
thrmin=-0.2
if Bthr>thrmin:
    cutC=TCut(v2+"> "+str(y1)+"+"+str(slope)+"*("+v1+"-"+str(x1)+")")
else:
    cutC=TCut("")
cbg2=TCanvas("cbg2","Background Events",50,50,500,400)
tt.Draw(v2+":"+v1+">>hbg2",cutB+cutC)
gB=TGraph(tt.GetSelectedRows(),tt.GetV2(),tt.GetV1())

if Bthr>thrmin:
    # clone gB to fit it to a line
    gBfit=gB.Clone("gBfit")
    gBfit.Fit("pol1")
    # Create a 1d histogram to hold the fit value and confidence intervals
    hint=TH1D("hint","Fitted gaussian with .95 conf.band", 1000, 0,100)
    hint.SetFillColor(3)
    TVirtualFitter.GetFitter().GetConfidenceIntervals(hint,0.99)
    hint.Draw("e3same")

    # take the fit parameters
    bgfit=gBfit.GetFunction("pol1")
    p0=bgfit.GetParameter(0)
    p1=bgfit.GetParameter(1)
    erp0=bgfit.GetParError(0)
    erp1=bgfit.GetParError(1)
    p2=1/sqrt(1+p1**2)

    fSigRef=TF1("fSigRef","[0]+[1]*x",0,100)
    fSigRef.SetParameter(0,p0+erp0)
    fSigRef.SetParameter(1,p1)
    fSigRef.SetLineStyle(1)
    fSigRef.SetLineWidth(2)
    fSigRef.SetLineColor(3)
    fSigRef.Draw("same")
    fContourCut.Draw("same")
    gPad.Update()

    cbg.cd()
    hint.Draw("e3same")
    fSigRef.Draw("same")
gPad.Update()

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#------- Plot with signal and background events
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
c1=TCanvas("c1","c1",100,100,500,400)

#tt.Draw(v2+":"+v1,cutB+cutC,"goff")
tt.Draw(v2+":"+v1,cutS,"goff")
gS=TGraph(tt.GetSelectedRows(),tt.GetV2(),tt.GetV1())
gS.SetMarkerStyle(20); gB.SetMarkerStyle(20)
gS.SetMarkerSize(0.3); gB.SetMarkerSize(0.3)
gS.SetMarkerColor(2);  gB.SetMarkerColor(4)
if Bthr>thrmin:
    gS.SetMarkerSize(0.5)
    gB.SetMarkerSize(0.5)
gS.GetXaxis().SetTitle(v1)
gS.GetYaxis().SetTitle(v2)
gtitle="Off Source"
gS.SetTitle(gtitle)

gS.Draw("AP")
gB.Draw("Psame")
hbg.Draw("cont3same,list")
fContourCut.Draw("same")
if Bthr>thrmin:
    bgfit.Draw("same")
    fSigRef.Draw("same")
gPad.Update()

gS.GetXaxis().Set(10,smin,smax)
gS.GetYaxis().Set(10,s2min,s2max)
gPad.SetGrid(1,1)
gPad.SetLeftMargin(0.1)
gPad.Update()
gPad.Print("plots/BDTvsX-"+bt+".eps")
if options.gif: gPad.Print("plots/BDTvsX-"+bt+".gif")

htmlfile.write('<a href=BDTvsX-'+bt+'.png><img src=BDTvsX-'+bt+'.png width=300></a>\n')
#if options.isbatch==0:raw_input("Press Enter To Continue: ")

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#------- significance statistics (distance from the fit line)
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
if Bthr>thrmin:
    nb=gB.GetN()
    xb=gB.GetX()
    yb=gB.GetY()
    sb=[]; sbplus=[]
    hSigB=TH1D("hSigB","Compound Signficance",40,0,20)
    hSigB.GetXaxis().SetTitle("N sigma from background fit")
    hSigS=hSigB.Clone("hSigS")
    hSigB.SetLineColor(4)
    hSigS.SetLineColor(2)

    for i in range(nb):
        d=-(p0+p1*xb[i]-yb[i])*p2
        sb.append(d)
        if d>0: sbplus.append(d)

    # calculate the 1-sigma as standard deviation of points above the line
    sigma=np.std(np.array(sbplus))
    print "sigma =",sigma
    for i in range(len(sb)):
        hSigB.Fill(sb[i]/sigma)
        #if sb[i]/sigma>1:print i,xb[i],yb[i],bgfit(xb[i]),sb[i]/sigma

    ns=gS.GetN(); xs=gS.GetX(); ys=gS.GetY(); ss=[]
    for i in range(ns):
        d=-(p0+p1*xs[i]-ys[i])*p2
        ss.append(d)
        hSigS.Fill(d/sigma)

    csig=TCanvas("csig","csig",100,500,500,400)
    #gPad.SetLogy()
    hSigS.Draw()
    hSigB.Draw("same")
    gPad.Update()

    gPad.Print("plots/significance-"+bt+".eps")
    if options.gif: gPad.Print("plots/significance-"+bt+".gif")
    htmlfile.write('<a href=significance-'+bt+'.png><img src=significance-'+bt+'.png width=300></a>\n')

if options.isbatch==0:raw_input("Press Enter To Continue: ")

# -----------------------------------------------------------------------------
# BDT histogram of B and S from training and test sets (replaces call to mvas.C)
# -----------------------------------------------------------------------------
c=TCanvas("c","c",200,200,500,400)
gPad.SetLogy()
tr.Draw(sig1+">>hstr(100,1,100)",cutStrain,"goff")
tr.Draw(sig1+">>hbtr(100,1,100)",cutB,"goff")
tt.Draw(sig1+">>hstt(100,1,100)",cutS,"goff")
tt.Draw(sig1+">>hbtt(100,1,100)",cutB,"goff")

#NormalizeHist(hbtr); NormalizeHist(hbtt)
#hbtr.Scale(hstr.GetEntries())
#hbtt.Scale(hstr.GetEntries())
#NormalizeHist(hstr); NormalizeHist(hstt)

hstt.SetTitle("TMVA overtraining check - not normalized")
hstt.GetXaxis().SetRangeUser(smin-2,smax+2)
hstt.GetXaxis().SetTitle(sig1)
hstt.SetMaximum(max(hbtt.GetMaximum(),hstt.GetMaximum())*100)
hstt.SetLineColor( 924 )
hstt.SetLineWidth( 2 )
hstt.SetFillStyle( 1001 )
hstt.SetFillColor( 38 )
hstt.Draw("hist")

hbtt.SetLineColor( 2 );
hbtt.SetLineWidth( 2 )
hbtt.SetFillStyle( 3554 )
hbtt.SetFillColor( 2 );
hbtt.Draw("samehist")

hstr.SetMarkerColor(hstt.GetLineColor())
hstr.SetMarkerSize(0.7)
hstr.SetMarkerStyle(20)
hstr.SetLineWidth(1)
hstr.SetLineColor(hstt.GetLineColor())
hstr.Draw("e1same")

hbtr.SetMarkerColor(hbtt.GetLineColor())
hbtr.SetMarkerSize(0.7)
hbtr.SetMarkerStyle(20)
hbtr.SetLineWidth(1)
hbtr.SetLineColor(hbtt.GetLineColor())
hbtr.Draw("e1same");

legend1=TLegend(0.1,0.75,0.5,0.9);
legend1.SetFillStyle( 1 );
legend1.AddEntry(hstt,"Signal (test)","F");
legend1.AddEntry(hbtt,"Background (test)","F");
legend1.SetBorderSize(1);
legend1.Draw("same");
legend2=TLegend(0.5,0.75,0.9,0.9);
legend2.SetFillStyle( 1 );
legend2.AddEntry(hstr,"Signal (train)","P");
legend2.AddEntry(hbtr,"Background (train)","P");
legend2.SetBorderSize(1);
legend2.Draw("same");

gPad.Update()
gPad.Print("plots/BDThisto-"+bt+".eps")
if options.gif: gPad.Print("plots/BDThisto-"+bt+".gif")

htmlfile.write('<a href=BDThisto-'+bt+'.png><img src=BDThisto-'+bt+'.png width=300></a>\n')

# -----------------------------------------------------------------------------
# Scan residual background events
# -----------------------------------------------------------------------------
if Bthr>thrmin:
    outname='output'+options.sig+'-'+bt+'.txt'
    tt.GetPlayer().SetScanRedirect(kTRUE)
    tt.GetPlayer().SetScanFileName(outname)

    if Bthr>thrmin:
        cutstring=v2+"> "+str(p0+erp0)+"+("+str(p1)+")*"+v1
        nsigmastr="("+v2+"-("+str(p0)+")-("+str(p1)+")*"+v1+")*"+str(p2)+"/"+str(sigma)
        cutstring=nsigmastr+">2"
        print cutstring
        print nsigmastr
    else:
        cutstring=""
    cutF=TCut(cutstring)

    tt.SetAlias("BDTsig","(BDT+1)*50")
    tt.SetAlias("Xsig","log_significance_")
    tt.SetAlias("Nsigma",nsigmastr)
    tt.SetAlias("skytheta","skypositiontheta")
    tt.SetAlias("skyphi","skypositionphi")

    scanstr="uniqIdx:Xsig:BDTsig:Nsigma:skytheta"+ \
        ":skyphi:peakTime:peakFrequency:minTime:minFreq"
    tt.Scan(scanstr,str(cutB+cutF),
            "colsize=10 precision=3 col=i:.3f:.3f:.3f:.2f:.2f:15.4f:.3f:15.4f:.3f")

    ftable=open(outname,'r')
    ftable.readline()
    a=ftable.readline() #.split("*")
    print a
    for line in ftable:
        if '*****' in line: continue
        a=line.strip("\n") #a=line.split("*")
        print a
    plotdir=options.fdir+'/plots-'+str(int(options.wf))
    os.system('mkdir -p '+plotdir)
    os.system('mv '+outname+' '+plotdir)
    htmlfile.write('<a href='+outname+'>loudest events</a>\n')

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Make 2D scatter plots (using Plot2D) and save to .eps
MakePlot2D(0,"BDTvsX-"+bt)

v="null"
v1=v+"energy"
v2=v+"inc"
if options.nifo==3: MakePlot2D(1,options.sig+"-"+bt+"-"+v)

v="circ"
v1=v+"energy"
v2=v+"inc"
MakePlot2D(1,options.sig+"-"+bt+"-"+v)

v="circnull"
v1=v+"energy"
v2=v+"inc"
MakePlot2D(1,options.sig+"-"+bt+"-"+v)
print "plots done"


#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
### Loudest events per jobNumber

print "~~~~~~~~~~~~~~~~ Loudest Events per jobNumber  ~~~~~~~~~~~~~~~~~~~"
Njobs=int(tt.GetMaximum('jobNumber'))
print 'Number of Jobs',Njobs

### Make new canvas
cJob=TCanvas("cJob","Histograms by Job Number",100,100,500,400)

###Get jobNumber, Xsig, BDTsig and Nsigma background data from training tree
tt.Draw('jobNumber:Xsig:BDT:Nsigma',cutB)
var1=tt.GetV1()
var2=tt.GetV2()
var3=tt.GetV3()
var4=tt.GetV4()

nrows=tt.GetSelectedRows()
v1=np.zeros(nrows+1)
v2=np.zeros(nrows+1)
v3=np.zeros(nrows+1)
v4=np.zeros(nrows+1)

for i in range(nrows):
  v1[i]=var1[i]
  v2[i]=var2[i]
  v3[i]=var3[i]
  v4[i]=var4[i]

### index by jobNumber and create indexed table 
ind=np.lexsort((v4,v1))
table=[(var1[i],var2[i],var3[i],var4[i]) for i in ind]

### index by Nsigma and create indexed table
ind2=np.lexsort((v1,v4))
tableNsigma=[(var1[i],var2[i],var3[i],var4[i]) for i in ind2]

### Dummy Onsource based on false alarm prob, p, in Nsigma
p = options.FAP
FAP = str(int(p*100))
Nsigma_OnS = np.rint((nrows+1)*(1-p)).astype(int)-1

xOnS=np.zeros(1,dtype=float)
bOnS=np.zeros(1,dtype=float)
nOnS=np.zeros(1,dtype=float)

xOnS[0] = np.float_(tableNsigma[Nsigma_OnS][1])
bOnS[0] = (np.float_(tableNsigma[Nsigma_OnS][2])+1)*50
nOnS[0] = np.float_(tableNsigma[Nsigma_OnS][3])

### "Prob" of 1
OnSprobX=np.zeros(1,dtype=float)
OnSprobB=np.zeros(1,dtype=float)
OnSprobN=np.zeros(1,dtype=float)
OnSprobXAll=np.zeros(1,dtype=float)
OnSprobBAll=np.zeros(1,dtype=float)
OnSprobNAll=np.zeros(1,dtype=float)

OnSprobX[0]=1.0
OnSprobB[0]=1.0
OnSprobN[0]=1.0
OnSprobXAll[0]=1.0
OnSprobBAll[0]=1.0
OnSprobNAll[0]=1.0

### Creat histograms
hXsig=TH1D("Xsig","Xsig",100,0,10)
hBDTsig=TH1D("BDTsig","BDTsig",200,0,100)
hNsigma=TH1D("Nsigma","Nsigma",100,-15,15)

hXsigAll=TH1D("XsigAll","XsigAll",100,0,10)
hBDTsigAll=TH1D("BDTsigAll","BDTsigAll",200,0,100)
hNsigmaAll=TH1D("NsigmaAll","NsigmaAll",100,-15,15)

j=0
k=0

### Fill histograms
for i in range(nrows):
    jn=table[i][0]
    hXsigAll.Fill(table[i][1])
    hBDTsigAll.Fill((table[i][2]+1)*50)
    hNsigmaAll.Fill(table[i][3])

    if jn==table[i+1][0]:
       k=k+1
    if jn!=table[i+1][0]:
       smax1=max(table[n][1] for n in range(j,k+1))
       smax2=max(table[n][2] for n in range(j,k+1))
       smax3=max(table[n][3] for n in range(j,k+1))
       hXsig.Fill(smax1)
       hBDTsig.Fill((smax2+1)*50)
       hNsigma.Fill(smax3)
       k=k+1
       j=k


### Draw and Format Histograms

gPad.SetLogy(1)
norm = 1.0/Njobs
normAll = 1.0/(Njobs*256)

OnSprobX[0]=hXsig.GetEntries()/Njobs
OnSprobB[0]=hBDTsig.GetEntries()/Njobs
OnSprobN[0]=hNsigma.GetEntries()/Njobs
OnSprobXAll[0]=hXsigAll.GetEntries()/(Njobs*256)
OnSprobBAll[0]=hBDTsigAll.GetEntries()/(Njobs*256)
OnSprobNAll[0]=hNsigmaAll.GetEntries()/(Njobs*256)

###Xsig
hXsig.Scale(norm)
# work around for the GetCumulative function.
nBins=hXsig.GetNbinsX()
sumBins = 0
for ii in range(nBins):
  sumBins = sumBins + hXsig.GetBinContent(nBins-ii)
  hXsig.SetBinContent(nBins-ii, sumBins)
#hXsig = hXsig.GetCumulative(kFALSE)
hXsig.SetTitle("Loudest Off-Source Events in Xpipeline per JobNumber ")
hXsig.GetXaxis().SetTitle("Xpipeline Significance")
hXsig.GetYaxis().SetTitle("# Events")
hXsig.GetYaxis().SetTitleOffset(1.2)
hXsig.SetLineColor( 2 )
hXsig.SetLineWidth( 2 )
hXsig.SetFillStyle( 3554 )
hXsig.SetFillColor( 2 )
grx=TGraph(1,xOnS,OnSprobX)
hXsig.Draw()
grx.Draw("*")

legendXsig=TLegend(0.6,0.7,0.9,0.9)
legendXsig.SetFillStyle( 1 )
legendXsig.AddEntry(hXsig,"Background (test)","F")
legendXsig.AddEntry(grx,"Dummy On-Source","P")
legendXsig.AddEntry(0,"False Alarm Prob "+FAP+"%","")
legendXsig.AddEntry(0,"BDT Threshold "+bt+" ","")
legendXsig.SetBorderSize( 1 )
legendXsig.Draw()
gPad.Print("plots/Xsig-JobNumber-"+bt+".eps")

###XsigAll
hXsigAll.Scale(normAll)
# work around for the GetCumulative function.
nBins=hXsigAll.GetNbinsX()
sumBins = 0
for ii in range(nBins):
  sumBins = sumBins + hXsigAll.GetBinContent(nBins-ii)
  hXsigAll.SetBinContent(nBins-ii, sumBins)
#hXsigAll = hXsigAll.GetCumulative(kFALSE)
hXsigAll.SetTitle("Rate vs. Xpipeline Significance")
hXsigAll.GetXaxis().SetTitle("Xpipeline Significance")
hXsigAll.GetYaxis().SetTitle("rate(Hz)")
hXsigAll.GetYaxis().SetTitleOffset(1.2)
hXsigAll.SetLineColor( 2 )
hXsigAll.SetLineWidth( 2 )
hXsigAll.SetFillStyle( 3554 )
hXsigAll.SetFillColor( 2 )
grxAll=TGraph(1,xOnS,OnSprobXAll)
hXsigAll.Draw()
grxAll.Draw("*")

legendXsigAll=TLegend(0.6,0.7,0.9,0.9)
legendXsigAll.SetFillStyle( 1 )
legendXsigAll.AddEntry(hXsigAll,"Background (test)","F")
legendXsigAll.AddEntry(grxAll,"Dummy On-Source","P")
legendXsigAll.AddEntry(0,"False Alarm Prob "+FAP+"%","")
legendXsigAll.AddEntry(0,"BDT Threshold "+bt+" ","")
legendXsigAll.SetBorderSize( 1 )
legendXsigAll.Draw()
gPad.Print("plots/XsigAll-JobNumber-"+bt+".eps")

###BDTsig
hBDTsig.Scale(norm)
# work around for the GetCumulative function.
nBins=hBDTsig.GetNbinsX()
sumBins = 0
for ii in range(nBins):
  sumBins = sumBins + hBDTsig.GetBinContent(nBins-ii)
  hBDTsig.SetBinContent(nBins-ii, sumBins)
#hBDTsig = hBDTsig.GetCumulative(kFALSE)
hBDTsig.SetTitle("Loudest Off-Source Events in BDT per JobNumber")
hBDTsig.GetXaxis().SetTitle("BDT Significance")
hBDTsig.GetYaxis().SetTitle("# Events")
hBDTsig.GetYaxis().SetTitleOffset(1.2)
hBDTsig.SetLineColor( 2 )
hBDTsig.SetLineWidth( 2 )
hBDTsig.SetFillStyle( 3554 )
hBDTsig.SetFillColor( 2 )
grb=TGraph(1,bOnS,OnSprobB)
hBDTsig.Draw()
grb.Draw("*")

legendBDTsig=TLegend(0.6,0.7,0.9,0.9)
legendBDTsig.SetFillStyle( 1 )
legendBDTsig.AddEntry(hBDTsig,"Background (test)","F")
legendBDTsig.AddEntry(grb,"Dummy On-Source","P")
legendBDTsig.AddEntry(0,"False Alarm Prob "+FAP+"%","")
legendBDTsig.AddEntry(0,"BDT Threshold "+bt+" ","")
legendBDTsig.SetBorderSize(1)
legendBDTsig.Draw()
gPad.Print("plots/BDTsig-JobNumber-"+bt+".eps")

###BDTsigAll
hBDTsigAll.Scale(normAll)
# work around for the GetCumulative function.
nBins=hBDTsigAll.GetNbinsX()
sumBins = 0
for ii in range(nBins):
  sumBins = sumBins + hBDTsigAll.GetBinContent(nBins-ii)
  hBDTsigAll.SetBinContent(nBins-ii, sumBins)
#hBDTsigAll = hBDTsigAll.GetCumulative(kFALSE)
hBDTsigAll.SetTitle("Rate vs. BDT Significance")
hBDTsigAll.GetXaxis().SetTitle("BDT Significance")
hBDTsigAll.GetYaxis().SetTitle("rate(Hz)")
hBDTsigAll.GetYaxis().SetTitleOffset(1.2)
hBDTsigAll.SetLineColor( 2 )
hBDTsigAll.SetLineWidth( 2 )
hBDTsigAll.SetFillStyle( 3554 )
hBDTsigAll.SetFillColor( 2 )
grbAll=TGraph(1,bOnS,OnSprobBAll)
hBDTsigAll.Draw()
grbAll.Draw("*")

legendBDTsigAll=TLegend(0.6,0.7,0.9,0.9)
legendBDTsigAll.SetFillStyle( 1 )
legendBDTsigAll.AddEntry(hBDTsigAll,"Background (test)","F")
legendBDTsigAll.AddEntry(grbAll,"Dummy On-Source","P")
legendBDTsigAll.AddEntry(0,"False Alarm Prob "+FAP+"%","")
legendBDTsigAll.AddEntry(0,"BDT Threshold "+bt+" ","")
legendBDTsigAll.SetBorderSize(1)
legendBDTsigAll.Draw()
gPad.Print("plots/BDTsigAll-JobNumber-"+bt+".eps")

###Nsigma
hNsigma.Scale(norm)
# work around for the GetCumulative function.
nBins=hNsigma.GetNbinsX()
sumBins = 0
for ii in range(nBins):
  sumBins = sumBins + hNsigma.GetBinContent(nBins-ii)
  hNsigma.SetBinContent(nBins-ii, sumBins)
#hNsigma = hNsigma.GetCumulative(kFALSE)
hNsigma.SetTitle("Largest N sigma from Background Fit per JobNumber")
hNsigma.GetXaxis().SetTitle("N sigma from Fit")
hNsigma.GetYaxis().SetTitle("# Events")
hNsigma.GetYaxis().SetTitleOffset(1.2)
hNsigma.SetLineColor( 2 )
hNsigma.SetLineWidth( 2 )
hNsigma.SetFillStyle( 3554 )
hNsigma.SetFillColor( 2 )
grn=TGraph(1,nOnS,OnSprobN)
hNsigma.Draw()
grn.Draw("*")

legendNsigma=TLegend(0.6,0.7,0.9,0.9)
legendNsigma.SetFillStyle( 1 )
legendNsigma.AddEntry(hNsigma,"Background (test)","F")
legendNsigma.AddEntry(grn,"Dummy On-Source","P")
legendNsigma.AddEntry(0,"False Alarm Prob "+FAP+"%","")
legendNsigma.AddEntry(0,"BDT Threshold "+bt+" ","")
legendNsigma.SetBorderSize(1)
legendNsigma.Draw()
gPad.Print("plots/Nsigma-JobNumber-"+bt+".eps")

###NsigmaAll
hNsigmaAll.Scale(normAll)
# work around for the GetCumulative function.
nBins=hNsigmaAll.GetNbinsX()
sumBins = 0
for ii in range(nBins):
  sumBins = sumBins + hNsigmaAll.GetBinContent(nBins-ii)
  hNsigmaAll.SetBinContent(nBins-ii, sumBins)
#hNsigmaAll = hNsigmaAll.GetCumulative(kFALSE)
hNsigmaAll.SetTitle("Rate vs. Number of Sigma from Background Fit")
hNsigmaAll.GetXaxis().SetTitle("Nsigma from fit")
hNsigmaAll.GetYaxis().SetTitle("rate(Hz)")
hNsigmaAll.GetYaxis().SetTitleOffset(1.2)
hNsigmaAll.SetLineColor( 2 )
hNsigmaAll.SetLineWidth( 2 )
hNsigmaAll.SetFillStyle( 3554 )
hNsigmaAll.SetFillColor( 2 )
grnAll=TGraph(1,nOnS,OnSprobNAll)
hNsigmaAll.Draw()
grnAll.Draw("*")

legendNsigmaAll=TLegend(0.6,0.7,0.9,0.9)
legendNsigmaAll.SetFillStyle( 1 )
legendNsigmaAll.AddEntry(hNsigmaAll,"Background (test)","F")
legendNsigmaAll.AddEntry(grnAll,"Dummy On-Source","P")
legendNsigmaAll.AddEntry(0,"False Alarm Prob "+FAP+"%","")
legendNsigmaAll.AddEntry(0,"BDT Threshold "+bt+" ","")
legendNsigmaAll.SetBorderSize(1)
legendNsigmaAll.Draw()
gPad.Print("plots/NsigmaAll-JobNumber-"+bt+".eps")

### Save to htmlfile
htmlfile.write('<br><a href=Xsig-JobNumber-'+bt+'.png><img src=Xsig-JobNumber-'+bt+'.png width=300></a>\n')
htmlfile.write('<a href=BDTsig-JobNumber-'+bt+'.png><img src=BDTsig-JobNumber-'+bt+'.png width=300></a>\n')
htmlfile.write('<a href=Nsigma-JobNumber-'+bt+'.png><img src=Nsigma-JobNumber-'+bt+'.png width=300></a>\n')

htmlfile.write('<br><a href=XsigAll-JobNumber-'+bt+'.png><img src=XsigAll-JobNumber-'+bt+'.png width=300></a>\n')
htmlfile.write('<a href=BDTsigAll-JobNumber-'+bt+'.png><img src=BDTsigAll-JobNumber-'+bt+'.png width=300></a>\n')
htmlfile.write('<a href=NsigmaAll-JobNumber-'+bt+'.png><img src=NsigmaAll-JobNumber-'+bt+'.png width=300></a>\n')

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
### Efficiency Curves 

print "~~~~~~~~~~~~~~~~ Efficiency Curves ~~~~~~~~~~~~~~~~~~~"

#Injection Scale for 3 detectors (the injscale var holds the number representing the  position of the actual injscale on this list)
injScale = np.array([0.01,0.0316,0.0442,0.0619,0.0866,0.121,0.169,0.237,0.332,0.464,0.649,0.909,1.271,1.778,2.488,3.481,4.870,6.813,9.532,13.335,18.657,26.102,36.517,51.090,71.477,100.00])

#Number of Injections, the number of injections scales, times the number of waveforms, times the number of jobs run per each divided by 2, half for the training and half for the testing tree.
#Ninj = (injScale.size)*(3)*(600)/(2)
#This will eventually need to be parsed in or read from ini file
Ninj = (600.0)/(2.0)
print 'Ninj',Ninj

#########

### Make new canvas
EffC=TCanvas("EffC","Efficiency Curves",100,100,500,400)

###Get injscale #, Nsigma matfile and BDT from training tree
tt.Draw('injscale:Nsigma:matfile:BDT')
var5=tt.GetV1()
var6=tt.GetV2()
var7=tt.GetV3()
var8=tt.GetV4()

### Make python arrays
nrows=tt.GetSelectedRows()
v5=np.zeros(nrows+1)
v6=np.zeros(nrows+1)
v7=np.zeros(nrows+1)
v8=np.zeros(nrows+1)

for i in range(nrows):
  v5[i]=var5[i]
  v6[i]=var6[i]
  v7[i]=var7[i]
  v8[i]=var8[i]


### index by Injscale and create indexed table of Nsigma, BDT, matfile and InjScale
ind3=np.lexsort((v6,v5))
tableEff=[[v5[i],v6[i],v7[i],v8[i]] for i in ind3]

#Nsigma Eff histogram
hNsigmaEffw1=TH1D("NsigmaEff","NsigmaEff",25,0,26)
hNsigmaEffw2=TH1D("NsigmaEff","NsigmaEff",25,0,26)
hNsigmaEffw3=TH1D("NsigmaEff","NsigmaEff",25,0,26)
hBDTEffw1=TH1D("BDTEff","BDTEff",25,0,26)
hBDTEffw2=TH1D("BDTEff","BDTEff",25,0,26)
hBDTEffw3=TH1D("BDTEff","BDTEff",25,0,26)


for i in range(nrows):
    if tableEff[i][1] >= nOnS:
        if tableEff[i][2] == 1:
              hNsigmaEffw1.Fill(tableEff[i][0])
              hBDTEffw1.Fill(tableEff[i][0])
        if tableEff[i][2] == 2:
              hNsigmaEffw2.Fill(tableEff[i][0])
              hBDTEffw2.Fill(tableEff[i][0])
        if tableEff[i][2] == 3:
              hNsigmaEffw3.Fill(tableEff[i][0])
              hBDTEffw3.Fill(tableEff[i][0])


### Create Plots
hNsigmaEffw1.Scale((1.0/Ninj))
hNsigmaEffw1.SetTitle("Waveform_1 Nsigma Detection Efficiency")
hNsigmaEffw1.GetXaxis().SetTitle("InjScale")
hNsigmaEffw1.GetYaxis().SetTitle("fraction above loudest event")
hNsigmaEffw1.GetYaxis().SetTitleOffset(1.2)
hNsigmaEffw1.SetLineColor( 2 )
hNsigmaEffw1.SetLineWidth( 2 )
hNsigmaEffw1.SetFillStyle( 3554 )
hNsigmaEffw1.SetFillColor( 2 )
grnAll=TGraph(1,nOnS,OnSprobNAll)
hNsigmaEffw1.Draw()
grnAll.Draw("*")
gPad.Print("plots/Nsigma_w1-Efficiency-"+bt+".eps")

hNsigmaEffw2.Scale((1.0/Ninj))
hNsigmaEffw2.SetTitle("Waveform_2 Nsigma Detection Efficiency")
hNsigmaEffw2.GetXaxis().SetTitle("InjScale")
hNsigmaEffw2.GetYaxis().SetTitle("fraction above loudest event")
hNsigmaEffw2.GetYaxis().SetTitleOffset(1.2)
hNsigmaEffw2.SetLineColor( 2 )
hNsigmaEffw2.SetLineWidth( 2 )
hNsigmaEffw2.SetFillStyle( 3554 )
hNsigmaEffw2.SetFillColor( 2 )
grnAll=TGraph(1,nOnS,OnSprobNAll)
hNsigmaEffw2.Draw()
grnAll.Draw("*")
gPad.Print("plots/Nsigma_w2-Efficiency-"+bt+".eps")

hNsigmaEffw3.Scale((1.0/Ninj))
hNsigmaEffw3.SetTitle("Waveform_3 Nsigma Detection Efficiency")
hNsigmaEffw3.GetXaxis().SetTitle("InjScale")
hNsigmaEffw3.GetYaxis().SetTitle("fraction above loudest event")
hNsigmaEffw3.GetYaxis().SetTitleOffset(1.2)
hNsigmaEffw3.SetLineColor( 2 )
hNsigmaEffw3.SetLineWidth( 2 )
hNsigmaEffw3.SetFillStyle( 3554 )
hNsigmaEffw3.SetFillColor( 2 )
grnAll=TGraph(1,nOnS,OnSprobNAll)
hNsigmaEffw3.Draw()
grnAll.Draw("*")
gPad.Print("plots/Nsigma_w3-Efficiency-"+bt+".eps")

hBDTEffw1.Scale((1.0/Ninj))
hBDTEffw1.SetTitle("Waveform_1 BDT Detection Efficiency")
hBDTEffw1.GetXaxis().SetTitle("InjScale")
hBDTEffw1.GetYaxis().SetTitle("fraction above loudest event")
hBDTEffw1.GetYaxis().SetTitleOffset(1.2)
hBDTEffw1.SetLineColor( 2 )
hBDTEffw1.SetLineWidth( 2 )
hBDTEffw1.SetFillStyle( 3554 )
hBDTEffw1.SetFillColor( 2 )
grnAll=TGraph(1,nOnS,OnSprobNAll)
hBDTEffw1.Draw()
grnAll.Draw("*")
gPad.Print("plots/BDT_w1-Efficiency-"+bt+".eps")

hBDTEffw2.Scale((1.0/Ninj))
hBDTEffw2.SetTitle("Waveform_2 BDT Detection Efficiency")
hBDTEffw2.GetXaxis().SetTitle("InjScale")
hBDTEffw2.GetYaxis().SetTitle("fraction above loudest event")
hBDTEffw2.GetYaxis().SetTitleOffset(1.2)
hBDTEffw2.SetLineColor( 2 )
hBDTEffw2.SetLineWidth( 2 )
hBDTEffw2.SetFillStyle( 3554 )
hBDTEffw2.SetFillColor( 2 )
grnAll=TGraph(1,nOnS,OnSprobNAll)
hBDTEffw2.Draw()
grnAll.Draw("*")
gPad.Print("plots/BDT_w2-Efficiency-"+bt+".eps")

hBDTEffw3.Scale((1.0/Ninj))
hBDTEffw3.SetTitle("Waveform_3 BDT Detection Efficiency")
hBDTEffw3.GetXaxis().SetTitle("InjScale")
hBDTEffw3.GetYaxis().SetTitle("fraction above loudest event")
hBDTEffw3.GetYaxis().SetTitleOffset(1.2)
hBDTEffw3.SetLineColor( 2 )
hBDTEffw3.SetLineWidth( 2 )
hBDTEffw3.SetFillStyle( 3554 )
hBDTEffw3.SetFillColor( 2 )
grnAll=TGraph(1,nOnS,OnSprobNAll)
hBDTEffw3.Draw()
grnAll.Draw("*")
gPad.Print("plots/BDT_w3-Efficiency-"+bt+".eps")

### Save to htmlfile
htmlfile.write('<br><a href=Nsigma_w1-Efficiency-'+bt+'.png><img src=Nsigma_w1-Efficiency-'+bt+'.png width=300></a>\n')
htmlfile.write('<br><a href=Nsigma_w2-Efficiency-'+bt+'.png><img src=Nsigma_w2-Efficiency-'+bt+'.png width=300></a>\n')
htmlfile.write('<br><a href=Nsigma_w3-Efficiency-'+bt+'.png><img src=Nsigma_w3-Efficiency-'+bt+'.png width=300></a>\n')
htmlfile.write('<br><a href=BDT_w1-Efficiency-'+bt+'.png><img src=BDT_w1-Efficiency-'+bt+'.png width=300></a>\n')
htmlfile.write('<br><a href=BDT_w2-Efficiency-'+bt+'.png><img src=BDT_w2-Efficiency-'+bt+'.png width=300></a>\n')
htmlfile.write('<br><a href=BDT_w3-Efficiency-'+bt+'.png><img src=BDT_w3-Efficiency-'+bt+'.png width=300></a>\n')

print "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"



#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Housekeeping
if options.png: os.system('./ConvertPlots.sh')
plotdir=options.fdir+'/plots-'+str(int(options.wf))
print "moving plots to "+plotdir
os.system('test -d '+plotdir+'/eps || mkdir -p '+plotdir+'/eps')
os.system('mv plots/*.png '+plotdir)

htmlfile.write('</html>\n')
htmlfile.close()
os.system('mv AAA.html '+plotdir+'/AAA-'+options.sig+'-'+bt+'.html')
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++







# -*- coding: utf-8 -*-
"""
Created on Mon Sep 12 12:43:06 2016

@author: iain

After running the MVA, DumpData.py converts the root data to txt.

This is based on the DumpData.C code.

To test the code, enter the command line code
         python -i DumpData.py BDT 2 likelihoods.dat output

This will test with MVA method myMethodlist=BDT and with nifo=3 interferometers. It will use the likelihoods in likelihoods.dat and load the MVA data from the xml file in output.
"""



import ROOT
import sys
import array
import numpy




#@profile
def DumpData(myMethodList, nifo, sLikelihoodsFile, outputDir):
    print "Running DumpData.py"

    #Open the likelihoods file and add them as variables
    likelihoods = numpy.loadtxt(sLikelihoodsFile, dtype="string")
    print "Using likelihoods:", likelihoods[:]

    #Add variables and spectators to a list
    variables=[]
    spectators=[]

    #This is an array that stores the C++ references of the likelihood variables.
    varRefs=[]
    specRefs=[]

    #We use arrays to store the C++ references, then add the reader variable.
    varRefs.append(array.array("f",[0]))
    variables.append("log(significance)")


    for i in xrange(len(likelihoods)):
        #log likelihoods that are not skypositions, add skypositions as spectators
        if likelihoods[i][0:3] != "sky":
            if likelihoods[i][0:3] != "log":
                variables.append("log(" + likelihoods[i] + ")")
                varRefs.append(array.array("f",[0]))
            else:
                variables.append(likelihoods[i])
                varRefs.append(array.array("f",[0]))


    varRefs.append(array.array("f",[0]))
    variables.append("nPixels")
    varRefs.append(array.array("f",[0]))
    variables.append("deltaTime")
    varRefs.append(array.array("f",[0]))
    variables.append("deltaFreq")

    #Add spectators
    specRefs.append(array.array("i",[0]))
    spectators.append("jobNumber")


    specRefs.append(array.array("f",[0]))
    spectators.append("significance")


    #We probably don't need to add the likelihoods as spectators,
    #but this is the way DumpData.C was so we are sticking with it for now.
    for i in xrange(len(likelihoods)):
        if likelihoods[i][0:3] != "log":
            spectators.append(likelihoods[i])
            specRefs.append(array.array("f",[0]))

    specRefs.append(array.array("i",[0]))
    spectators.append("injIdx")
    specRefs.append(array.array("i",[0]))
    spectators.append("triggerIdx")
    specRefs.append(array.array("f",[0]))
    spectators.append("peakTime")
    specRefs.append(array.array("f",[0]))
    spectators.append("peakFrequency")
    specRefs.append(array.array("f",[0]))
    spectators.append("minTime")
    specRefs.append(array.array("f",[0]))
    spectators.append("minFreq")
    specRefs.append(array.array("i",[0]))
    spectators.append("uniqIdx")
    specRefs.append(array.array("i",[0]))
    spectators.append("matfile")
    specRefs.append(array.array("f",[0]))
    spectators.append("injscale")


    #What MVA method are we using? This is found from the method list.
    #We only use BDT and MLP at the moment.

    if myMethodList == "BDT":
       print "Dumping BDT files"
    elif myMethodList == "MLP":
       print "Dumping MLP files"
    else:
       sys.exit("Unknown MVA method") #Will later add other MVA methods.

    if int(nifo) == 2:
       print "Number of interferometers = 2"
    elif int(nifo) == 3:
       print "Number of interferometers = 3"
    else:
       sys.exit("Invalid number of interferometers. Please choose nifo=2 or 3")



    #Define the reader
    reader=ROOT.TMVA.Reader()

    #Add variables and spectators to the reader
    for i in xrange(len(variables)):
        reader.AddVariable(variables[i], varRefs[i])
        print "Adding variable ", variables[i]

    for i in xrange(len(spectators)):
        reader.AddSpectator(spectators[i], specRefs[i])
        print "Adding spectator ", spectators[i]

    #Book MVA
    weightfile = outputDir + "/weights/xtmvaClassification_" + myMethodList + ".weights.xml"
    methodName = myMethodList + " method"
    reader.BookMVA( methodName, weightfile)



    #Load the ROOT file with the tree data
    rootFile = ROOT.TFile.Open(outputDir + "/TMVA_GRB_Output.root")
    print "Using file " + outputDir + "/TMVA_GRB_Output.root"

    trainTree = rootFile.Get("TrainTree") #Select the training data

    #Open files to write training data into
    trainSig = open("temp_signal_training.txt","w")
    trainBack = open("Output_background_training.txt","w")

    """
    addresses=[]
    #Set branch addresses
    for i in range(len(variables)):
        addresses.append(array.array("f",[0]))
        variables[i] = variables[i].replace("(","_").replace(")","_")
        trainTree.SetBranchAddress( variables[i],   addresses[i] );
        print "Setting address for ", variables[i]

    for i in range(len(spectators)):
        if isinstance(specRefs[i][0], int):
            addresses.append(array.array("i",[0]))
        else:
            addresses.append(array.array("f",[0]))
        trainTree.SetBranchAddress( spectators[i],   addresses[len(variables)+i] );
        print "Setting address for ", spectators[i]


    """
    print "--- Processing ", trainTree.GetEntries(), " events ---"


    for ievt in xrange(trainTree.GetEntries()):
        if ievt%1000==0:
            print "--- ... Processing event: ", ievt
        trainTree.GetEntry(ievt)
        if "BDT" in myMethodList:
            #Set addresses to be value of event ievt. Then we can use EvaluateMVA tofind the MVA score at that point in parameter space
            #for i in range(len(varRefs)):
                #varRefs[i][0] = addresses[i][0]

            #j=0 #Index for specRefs in following loop
            #for i in range(len(varRefs),len(addresses)):
                #specRefs[j][0] = addresses[i][0]
                #j+=1
            #Transform the BDT score to go from 0-100
            score = ((trainTree.BDT +0.8)/1.6)*100.
            if trainTree.injIdx != 0:
                #If signal event, write data in this file
                trainSig.write("%.8g" % score) #Set precision to 8s.f.
                trainSig.write("  "+ str(int(trainTree.injIdx)) + "  " +  str(int(trainTree.triggerIdx)) + "  " + str(int(trainTree.uniqIdx)) + "\n")
            else:
                #if bkg event write data in this file
                trainBack.write("%.8g" % score) #Set precision to 8s.f.
                trainBack.write("  " + str(int(trainTree.triggerIdx)) + "  " + str(int(trainTree.uniqIdx)) + "\n")
        if "MLP" in myMethodList:
            #Set addresses to be value of event ievt. Then we can use EvaluateMVA tofind the MVA score at that point in parameter space
            #for i in range(len(varRefs)):
                #varRefs[i][0] = addresses[i][0]

            #j=0 #Index for specRefs in following loop
            #for i in range(len(varRefs),len(addresses)):
                #specRefs[j][0] = addresses[i][0]
                #j+=1
            #Transform the MLP score to go from 0-100
            score = ((trainTree.MLP +0.8)/1.6)*100.
            if trainTree.injIdx != 0:
                #If signal event, write data in this file
                trainSig.write("%.8g" % score) #Set precision to 8s.f.
                trainSig.write("  "+ str(int(trainTree.injIdx)) + "  " +  str(int(trainTree.triggerIdx)) + "  " + str(int(trainTree.uniqIdx)) + "\n")
            else:
                #if bkg event write data in this file
                trainBack.write("%.8g" % score) #Set precision to 8s.f.
                trainBack.write("  " + str(int(trainTree.triggerIdx)) + "  " + str(int(trainTree.uniqIdx)) + "\n")

                                                                                                           
    #Close the txt files
    trainSig.close()
    trainBack.close()



    #Now we repeat the above process for the testing data and onsource

    testTree = rootFile.Get("TestTree") #Select the testing data

    #Open files to write testing data into
    testSig = open("temp_signal_testing.txt","w")
    testBack = open("Output_background_testing.txt","w")
    onSource = open("Output_on_source.txt","w")
    """
    addresses=[]
    #Set branch addresses
    for i in range(len(variables)):
        addresses.append(array.array("f",[0]))
        variables[i] = variables[i].replace("(","_").replace(")","_")
        testTree.SetBranchAddress( variables[i],   addresses[i] );
        print "Setting address for ", variables[i]

    for i in range(len(spectators)):
        if isinstance(specRefs[i][0], int):
            addresses.append(array.array("i",[0]))
        else:
            addresses.append(array.array("f",[0]))
        testTree.SetBranchAddress( spectators[i],   addresses[len(variables)+i] );
        print "Setting address for ", spectators[i]
    """

    for ievt in xrange(testTree.GetEntries()):
        if ievt%1000==0:
            print "--- ... Processing event: ", ievt
        testTree.GetEntry(ievt)
        if "BDT" in myMethodList:
            #for i in range(len(varRefs)):
                #varRefs[i][0] = addresses[i][0]

            #j=0 #Index for specRefs in following loop
            #for i in range(len(varRefs),len(addresses)):
                #specRefs[j][0] = addresses[i][0]
                #j+=1
            #Transform the BDT score to go from 0-100
            score = ((testTree.BDT + 0.8)/1.6)*100.
            if testTree.injIdx != 0:
                #If signal event, write data in this file
                testSig.write("%.8g" % score) #Set precision to 8s.f.
                testSig.write("  "+ str(int(testTree.injIdx)) + "  " +  str(int(testTree.triggerIdx)) + "  " + str(int(testTree.uniqIdx)) + "  " + str(int(testTree.matfile))  + "\n")
            elif testTree.jobNumber != 0:
                #if bkg event write data in this file
                testBack.write("%.8g" % score) #Set precision to 8s.f.
                testBack.write("  " + str(int(testTree.triggerIdx)) + "  " + str(int(testTree.uniqIdx)) + "\n")
            else:
                onSource.write("%.8g" % score) #Set precision to 8s.f.
                onSource.write("  " + str(int(testTree.triggerIdx)) + "  " + str(int(testTree.uniqIdx)) + "\n")
        if "MLP" in myMethodList:
            #for i in range(len(varRefs)):
                #varRefs[i][0] = addresses[i][0]

            #j=0 #Index for specRefs in following loop
            #for i in range(len(varRefs),len(addresses)):
                #specRefs[j][0] = addresses[i][0]
                #j+=1
            #Transform the MLP score to go from 0-100
            score = ((testTree.MLP + 0.8)/1.6)*100.
            if testTree.injIdx != 0:
                #If signal event, write data in this file
                testSig.write("%.8g" % score) #Set precision to 8s.f.
                testSig.write("  "+ str(int(testTree.injIdx)) + "  " +  str(int(testTree.triggerIdx)) + "  " + str(int(testTree.uniqIdx)) + "  " + str(int(testTree.matfile))  + "\n")
            elif testTree.jobNumber != 0:
                #if bkg event write data in this file
                testBack.write("%.8g" % score) #Set precision to 8s.f.
                testBack.write("  " + str(int(testTree.triggerIdx)) + "  " + str(int(testTree.uniqIdx)) + "\n")
            else:
                onSource.write("%.8g" % score) #Set precision to 8s.f.
                onSource.write("  " + str(int(testTree.triggerIdx)) + "  " + str(int(testTree.uniqIdx)) + "\n")



    #Close the txt files
    testSig.close()
    testBack.close()
    onSource.close()





    """
    TO DO:
    add other MVA methods?
    Check the methodName, and make more rigourous weightFile.

    CHECK WITH PATRICK THE IF STATEMENTS FOR THE ONSOURCE!! Why is jobNum=0 for onsource?

    TO DO LATER:
    -- Are we supposed to add most of the likelihoods as Spectators AND Variables???
            -- Sort out spectators and variables earlier in pipeline
    """







    print "Finished running DumpData.py"


if __name__ == "__main__":
    DumpData(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])
    #DumpData('BDT',2,'likelihoods.dat','output')



###############################
# pyrootstyle.py
# graphics options for pyROOT
# L.Cadonati 2014
###############################
from ROOT import gStyle,gROOT

"""
FontType=42
gridx=1
gridy=1
titlesize=0.06
labelsize=0.06

gStyle.SetOptStat(111110)
gStyle.SetTextSize(0.05)
gStyle.SetTextFont(FontType)

gStyle.SetFillColor(10)
gStyle.SetPalette(1)

# canvas-pad
gStyle.SetCanvasColor(10)
gStyle.SetFrameFillColor(10)
gStyle.SetFrameFillStyle(0)
gStyle.SetPadGridX(gridx)
gStyle.SetPadGridY(gridy)
gStyle.SetPadColor(0)
gStyle.SetPadTopMargin(0.1)
gStyle.SetPadBottomMargin(0.17)
gStyle.SetPadLeftMargin(0.17)
gStyle.SetPadRightMargin(0.23)

gStyle.SetPadBorderMode(0)
gStyle.SetCanvasBorderMode(0)
gStyle.SetFrameBorderMode(0)
gStyle.SetFrameFillStyle(0)

# The title box
gStyle.SetTitleFont(FontType)
gStyle.SetTitleFontSize(0.06)
gStyle.SetTitleStyle(0)
gStyle.SetTitleColor(1)
gStyle.SetTitleFillColor(0)
gStyle.SetTitleTextColor(1)
gStyle.SetTitleBorderSize(0)
gStyle.SetTitleY(0.965)
gStyle.SetTitleX(0.16)

# The stats box
gStyle.SetStatColor(10)
gStyle.SetStatFont(40)
gStyle.SetStatFontSize(0.06)
gStyle.SetStatBorderSize(0)
gStyle.SetStatX(0.98)
gStyle.SetStatY(0.88)

# Axis
gStyle.SetLabelFont(FontType,"xyz")
gStyle.SetTitleFont(FontType,"xyz")
gStyle.SetLabelSize(labelsize,"xyz")
gStyle.SetLabelOffset(0.01,"xz")
gStyle.SetLabelOffset(0.012,"y")
gStyle.SetTitleOffset(1.2,"xz")
gStyle.SetTitleOffset(1.2,"y")
gStyle.SetTitleSize(titlesize,"xyz")

"""

FontType=42
FontType=102

mystyle=gROOT.GetStyle("Plain")
mystyle.SetCanvasColor(0)

mystyle.SetTitleFillColor(0)

mystyle.SetName("mystyle")
mystyle.SetFuncWidth(2)
mystyle.SetHistLineWidth(2)
mystyle.SetLegendBorderSize(0)
mystyle.SetOptFit(1111);
mystyle.SetStatBorderSize(0);
mystyle.SetTitleBorderSize(0);
mystyle.SetDrawBorder(0);
mystyle.SetLabelSize(.04,"xyz");
mystyle.SetTitleSize(.04,"xyz");
mystyle.SetLabelFont(102,"xyz");
mystyle.SetOptStat("");
mystyle.SetStatFont(102);
mystyle.SetTitleFont(102,"xyz");
mystyle.SetTitleFont(102,"pad");
mystyle.SetStatStyle(0);
mystyle.SetStatX(1);
mystyle.SetStatY(1);
mystyle.SetStatW(.2);
mystyle.SetStatH(.15);
mystyle.SetTitleStyle(0);
mystyle.SetTitleX(.2);
mystyle.SetTitleW(.65);
mystyle.SetTitleY(.98);
mystyle.SetTitleH(.07);
mystyle.SetStatColor(0);
mystyle.SetStatBorderSize(0);
mystyle.SetFillColor(10);
mystyle.SetFillStyle(0);
mystyle.SetTextFont(102);
mystyle.SetCanvasBorderMode(0);
mystyle.SetPadBorderMode(0);
mystyle.SetFrameBorderMode(0);
mystyle.SetDrawBorder(0);


mystyle.SetPalette(1);
gROOT.SetStyle("mystyle");
#gROOT.ForceStyle(true);





void XTMVAplots(char* filename, bool useTMVAStyle=1){

    gROOT->SetBatch(1);
    gErrorIgnoreLevel = 1001;
    
    gROOT->LoadMacro("$ROOTSYS/tmva/test/mvaeffs.C");
    mvaeffs(filename,useTMVAStyle);
    
    gROOT->LoadMacro("$ROOTSYS/tmva/test/efficiencies.C");
    efficiencies(filename,2,useTMVAStyle);

    gROOT->LoadMacro("$ROOTSYS/tmva/test/mvas.C");
    mvas(filename,3,useTMVAStyle);

    gROOT->LoadMacro("$ROOTSYS/tmva/test/correlations.C");
    correlations(filename,0,0,useTMVAStyle);

}




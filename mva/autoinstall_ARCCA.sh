############################### README  ###############################
# Before running this script, do a :
# sed -i 's|c1306608|YOURUSERNAME|g' Makefile.in
# Replacing by the variable ${USER} does not work when automatically
# ./configure X-pipeline (it returns empty line and the pipeline can't 
# build sphrad.mexa64)
# default installation path is ~/opt/xpipeline
# To run, source sphrad_envsetup.sh created in your home directory.
###############################	README 	###############################


# All-in-one sphrad installation script for ARCCA (RAVEN - Cardiff University)
# Valeriu Predoi 2 June 2015
# Maxime Fays 1 Oct 2015

# get the lalsuite stuff
PATH=/scratch/LIGO/Apps/Matlab/Matlab_R2014b/bin:$PATH
source /scratch/LIGO/Apps/LALSuite/Master/etc/lalsuiterc

# start building the directory structure
mkdir -p /home/$USER/XSPHRAD
echo "made a directory called XSPHRAD in /home/user..."
# get the sphradkit suite
echo "getting and configuring sphrad kit..."
cp /home/spxph/Projects/LIGO/build_xsphrad/sphradkit/0.3/sphradkit-0.3.tar.gz /home/$USER/XSPHRAD
cd /home/$USER/XSPHRAD
tar xzvf sphradkit-0.3.tar.gz
mkdir -p sphradkitinstall
cd sphradkit-0.3
./configure --prefix=/home/$USER/XSPHRAD/sphradkitinstall --with-matlab=/scratch/LIGO/Apps/Matlab/Matlab_R2014b
make
make install
echo "done configuring sphrad kit..."

# get xpipeline stuff
echo "checking out and installing xpipeline..."
cd /home/$USER
svn checkout https://svn.ligo.caltech.edu/svn/xpipeline/branches/sphrad XSPHRAD
cd /home/$USER/XSPHRAD
sed -i 's|REPLACEMEMEX|/home/c1306608/XSPHRAD/sphradkitinstall/matlab|g' Makefile.in
./configure << EOF
/scratch/LIGO/Apps/Matlab/Matlab_R2014b
/scratch/LIGO/Apps/Matlab/MCR_R2014b
/home/c1306608/opt/xpipeline
no
no
/home/spxph/Projects/LIGO/ligotools
EOF
sed -i 's|usr1|tmp|g' share/xdetection.m
sed -i "s|'mkdir ' tmpdirname|'mkdir -p ' tmpdirname|g" share/xdetection.m
make
make install-allsky
echo "all done...Creating environment file in /home/USER/sphradenvsetup.sh..."

echo "stuff that needs to go in the environment setup file:"
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/software/matlab/R2014b/runtime/glnxa64:/software/matlab/R2014b/sys/os/glnxa64:/software/matlab/R2014b/bin/glnxa64/
echo "export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/software/matlab/R2014b/runtime/glnxa64:/software/matlab/R2014b/sys/os/glnxa64:/software/matlab/R2014b/bin/glnxa64/"
echo "export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/software/matlab/R2014b/runtime/glnxa64:/software/matlab/R2014b/sys/os/glnxa64:/software/matlab/R2014b/bin/glnxa64/" >> /home/$USER/sphradenvsetup.sh
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/home/c1306608/XSPHRAD/sphradkitinstall/lib
echo "LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/home/c1306608/XSPHRAD/sphradkitinstall/lib"
echo "LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/home/c1306608/XSPHRAD/sphradkitinstall/lib" >> /home/$USER/sphradenvsetup.sh
export XPIPE_INSTALL_BIN=/home/c1306608/opt/xpipeline/bin
echo "XPIPE_INSTALL_BIN=/home/c1306608/opt/xpipeline/bin"
echo "XPIPE_INSTALL_BIN=/home/c1306608/opt/xpipeline/bin" >> /home/$USER/sphradenvsetup.sh
export PATH=$XPIPE_INSTALL_BIN:$PATH
echo "PATH=$XPIPE_INSTALL_BIN:$PATH"
echo "PATH=$XPIPE_INSTALL_BIN:$PATH" >> /home/$USER/sphradenvsetup.sh
source /scratch/LIGO/Apps/LALSuite/Master/etc/lalsuiterc
echo "source /scratch/LIGO/Apps/LALSuite/Master/etc/lalsuiterc"
echo "source /scratch/LIGO/Apps/LALSuite/Master/etc/lalsuiterc" >> /home/$USER/sphradenvsetup.sh
chmod +x /home/$USER/sphradenvsetup.sh
echo "------------------------------------------------------------------"
echo "DONE"
